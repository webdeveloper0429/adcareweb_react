import React,{Component} from 'react';
import {Navbar,Nav,NavItem} from 'react-bootstrap';
import {connect} from 'react-redux';
import { logout } from './Actions/AppActions';
import {Link} from 'react-router-dom'
import styles from './sideBarMenu.css';
class AppHeader extends Component {
	constructor(props) {
		super(props);
		this.state = { appTitle: "Login",showModal: false,title:""}
		this.logout = this.logout.bind(this);
	}
	logout(){
		this.props.dispatch(logout());
		console.log(this.props.history)
		this.props.history.push('./');
	}
 
  render() {
	const {loggedIn} = this.props.UserData;
	let Nav='';
	let id='';
	var pathname=this.props.history.location.pathname
	if(pathname==='/profile'){
		id='nav1'
	}else{
		id='nav'
	}
	if(localStorage.token){
		return Nav=(
				<ul id={id} className="nav flex-column">
					<li>
					  <a href={`/ad`}>Playlist</a>
					</li>
					<li >
						<a  href={`/wallet`}>Wallet</a>
					</li>
					<li>
						<a  href={`/ranking`}>Ranking</a>
					</li>
					<li>
						<a  href={`/profile`}>Profile</a>
					</li>
					<li onClick={this.logout}>
						<a  href={`./`}>{'Logout'}</a>
					</li>
				</ul>
			
			);
	}else{
		return Nav=(
				<ul id='nav'className="nav flex-sm-column">
					<li>
					  <a href='#'>About</a>
					</li>
					<li>
					  <a href='#'>How it work</a>
					</li>
					<li>
					  <a href='#'>News</a>
					</li>
					<li>
					  <a href='#'>Download</a>
					</li>
				</ul>
				
				);
	}
	return (
		{Nav}
	);
  }
}
function select(state) {
  return {
	UserData: state.UserData
  };
}
module.exports=connect(select)(AppHeader);
