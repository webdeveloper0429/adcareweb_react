import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {IntlProvider} from 'react-intl';
import en_US from './en_US';
import zh_CN from './zh_CN';


ReactDOM.render(
  <IntlProvider locale="en" messages={en_US}>
  	<App />
  </IntlProvider>,
  document.getElementById('app')
);
