import React,{Component} from 'react';
import './App.css';
import { createStore,applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import  {homeReducer}  from './reducers/reducers';
import { Provider } from 'react-redux';
import HomePage from './components/pages/HomePage';
import GenderPage from './components/pages/GenderPage';
import AdPage from './components/pages/AdPage';
import OldPage from './components/pages/OldPage';
import LovePage from './components/pages/LovePage';
import CharityPage from './components/pages/CharityPage';
import NotFound from './components/pages/NotFound';
import CheckPage from './components/pages/CheckPage';
import VideoPage from './components/pages/VideoPage';
import PlayedPage from './components/pages/PlayedPage';
import VerifyPage from './components/pages/VerifyPage';
import WalletPage from './components/pages/WalletPage';
import RankingPage from './components/pages/RankingPage';
import CountryPage from './components/pages/CountryPage';
import ProfilePage from './components/pages/ProfilePage';
import ProfileEditPage from './components/pages/ProfileEditPage';
import TribePage from './components/pages/TribePage';
import AdminLoginPage from './components/adminpages/AdminLoginPage';
import UsersPage from './components/adminpages/UsersPage';
import InfoPage from './components/adminpages/InfoPage';
import AdListPage from './components/adminpages/AdListPage';
import AdminPage from './components/adminpages/AdminPage';
import AddCharity from './components/adminpages/AddCharity';
import Advertisers from './components/adminpages/Advertisers';
import AddAdPage from './components/adminpages/AddAdPage';
import EditAdPage from './components/adminpages/EditAdPage';
import AdCategory from './components/adminpages/AdCategory';
import AddCountry from './components/adminpages/AddCountry';
import CharityList from './components/adminpages/CharityList';
import EndPage from './components/pages/EndPage';
import CashPage from './components/pages/CashPage';
import store from './Store';
import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

class AppRouter extends Component {
  	render() {
		//const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
		//const store = createStoreWithMiddleware(homeReducer);
    	return (
			  <Router>
				<Switch>
					<Route exact path="/" component={HomePage}/>
					<Route path="/old" component={OldPage} />
					<Route path="/Love" component={LovePage} />
					<Route path="/Charity" component={CharityPage} />
					<Route path="/Gender" component={GenderPage}/>
					<Route path="/ad" component={AdPage} />
					<Route path="/Check/:id" component={CheckPage} />
					<Route path="/Video/:id" component={VideoPage} />
					<Route path="/Played/:id" component={PlayedPage} />
					<Route path="/code" component={VerifyPage} />
					<Route path="/wallet" component={WalletPage} />
					<Route path="/ranking" component={RankingPage} />
					<Route path="/profile" component={ProfilePage} />
					<Route path="/edit" component={ProfileEditPage} />
					<Route path="/tribe" component={TribePage} />
					<Route path="/cash" component={CashPage} />
					<Route path="/end" component={EndPage} />
					<Route path="/country" component={CountryPage} />
					<Route path="/login" component={AdminLoginPage} />
					<Route path="/users" component={UsersPage} />
					<Route path="/AdList" component={AdListPage} />
					<Route path="/info/:id" component={InfoPage} />
					<Route path="/Admin" component={AdminPage} />
					<Route path="/AddCharity" component={AddCharity} />
					<Route path="/addCountry" component={AddCountry} />
					<Route path="/CharityList" component={CharityList} />
					<Route path="/Advertisers" component={Advertisers} />
					<Route path="/AddAd" component={AddAdPage} />
					<Route path="/EditAd" component={EditAdPage} />
					<Route path="/Category" component={AdCategory} />
					<Route path="*" component={NotFound} />
				</Switch>
			  </Router>
    	);
  	}
}
class App extends Component {
  render() {
    return (
        <Provider store={store}>
			<div id='app'>
            <AppRouter/>
			</div>
        </Provider>
    );
  }
}

export default App;
