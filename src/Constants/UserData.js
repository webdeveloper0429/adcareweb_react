const NAME = 'userData';
export const UPDATE = `${NAME}/UPDATE`;
export const DELETE = `${NAME}/DELETE`;
export const LOAD = `${NAME}/LOAD`;
export const SAVE = `${NAME}/SAVE`;
