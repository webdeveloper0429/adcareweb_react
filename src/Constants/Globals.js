export const web = {
  language: {
    defaultValue: 'zh-TW',
    available: ['en', 'zh-TW'],
  },
  fontSize: 16,
  domainName: "https://adcare.skyyer.com",
  downloadVersion: false,
};


