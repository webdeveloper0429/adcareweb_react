import { AD_DATAS,ADD_CHARITY,PRO_DATAS,ADD_DATAS,CLEAR_FORM,CHANGE_FORM, SET_AUTH, SENDING_REQUEST, SET_ERROR_MESSAGE } from '../Constants/AppConstants';
import {web} from '../Constants/Globals';
// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign || require('object.assign');
import auth from '../utils/auth';
import home from '../image/Home.jpg';

// The initial application state
const initialState = {
  formState: {
	nickname:'',
    email: '',
	token:'',
  },
  domainName:web.domainName,
  profileDatas:{
	rows:[],
	edit:false
  },
  loveDatas:[],
  AdDatas:[],
  CharityDatas:[],
  Img:home,
  loggedIn: auth.loggedIn(),
  currentlySending: false,
  errorMessages:''
};

// Takes care of changing the application state
export function homeReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_FORM:
      return assign({}, state, {
        formState: action.newState
      });
      break;
    case PRO_DATAS:
      return assign({}, state, {
        profileDatas: action.newState
      });
      break;
    case AD_DATAS:
      return assign({}, state, {
        AdDatas: action.newState.AdDatas
      });
      break;
    case ADD_DATAS:
      return assign({}, state, {
        loveDatas: action.newState.loveDatas
      });
      break;
    case ADD_CHARITY:
      return assign({}, state, {
    	CharityDatas: action.newState.CharityDatas
      });
      break;
    case SET_AUTH:
      return assign({}, state, {
        loggedIn: action.newState
      });
      break;
    case SET_ERROR_MESSAGE:
      return assign({}, state, {
        errorMessage: action.message
      });
 	case SENDING_REQUEST:
      return assign({}, state, {
        currentlySending: action.sending
      });
      break;
    case CLEAR_FORM:
		return initialState;
    default:
      return state;
  }
}

