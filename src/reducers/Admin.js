import { CHANGE_FORM ,SET_ERROR_MESSAGE} from '../Constants/AppConstants';
import { UPDATE, DELETE } from '../Constants/UserData';
const assign = Object.assign || require('object.assign');
import auth from '../utils/auth';
import home from '../image/Home.jpg';
const initialState = {
  	add: false,
  	AdminloggIn: false
};

// Takes care of changing the application state
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case CHANGE_FORM:
      return {
        ...state,
        ...action.newState
      }
      break;
    case SET_ERROR_MESSAGE:
      return assign({}, state, {
        errorMessage: action.message
      });
    case DELETE:
      return initialState;
    default:
      return state;
  }
}

