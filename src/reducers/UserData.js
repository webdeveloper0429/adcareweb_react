import { CLEAR_FORM,PRO_DATAS,ADD_DATAS,CHANGE_FORM, SET_AUTH, SENDING_REQUEST, SET_ERROR_MESSAGE } from '../Constants/AppConstants';
import { UPDATE, DELETE } from '../Constants/UserData';

// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign || require('object.assign');
import auth from '../utils/auth';
import home from '../image/Home1.jpg';
const initialState = {
	nickname:'',
    email: '',
	token:'',
  	Img:home,
  	loggedIn: auth.loggedIn(),
  	currentlySending: false,
  	loading:false,
  	errorMessages:'',
	src: "",
	obj: {},
};

// Takes care of changing the application state
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case CHANGE_FORM:
      return {
        ...state,
        ...action.newState
      }
      break;
    case SET_AUTH:
      return assign({}, state, {
        loggedIn: action.newState
      });
      break;
    case SET_ERROR_MESSAGE:
      return assign({}, state, {
        errorMessage: action.message
      });
 	case SENDING_REQUEST:
      return assign({}, state, {
        currentlySending: action.sending
      });
      break;
    case CLEAR_FORM:
		return initialState;
    default:
      return state;
  }
}

