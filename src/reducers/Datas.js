import { PRO_DATAS,AD_DATAS,ADD_DATAS,ADD_CHARITY,CHANGE_FORM, SET_AUTH, SENDING_REQUEST, SET_ERROR_MESSAGE } from '../Constants/AppConstants';
import { UPDATE, DELETE } from '../Constants/UserData';
const assign = Object.assign || require('object.assign');
import {web} from '../Constants/Globals';
import auth from '../utils/auth';
import home from '../image/Home.jpg';
const initialState = {
	profileDatas:{
		rows:[],
		edit:false
  	},
  	loveDatas:[],
  	AdDatas:[],
  	CharityDatas:[],
  	Img:home,
  	loggedIn: auth.loggedIn(),
	domainName:web.domainName,
  	currentlySending: false,
  	errorMessages:''
};

// Takes care of changing the application state
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case CHANGE_FORM:
      return {
        ...state,
        ...action.newState
      }
      break;
    case PRO_DATAS:
      return assign({}, state, {
        profileDatas: action.newState
      });
      break;
    case AD_DATAS:
      return assign({}, state, {
        AdDatas: action.newState.AdDatas
      });
      break;
    case ADD_DATAS:
      return assign({}, state, {
        loveDatas: action.newState.loveDatas
      });
      break;
    case ADD_CHARITY:
      return assign({}, state, {
    	CharityDatas: action.newState.CharityDatas
      });
      break;
    case SET_AUTH:
      return assign({}, state, {
        loggedIn: action.newState
      });
      break;
    case SET_ERROR_MESSAGE:
      return assign({}, state, {
        errorMessage: action.message
      });
    case DELETE:
      return initialState;
    default:
      return state;
  }
}

