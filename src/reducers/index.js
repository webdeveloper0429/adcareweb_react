import {combineReducers} from 'redux';
import UserData from './UserData';
import Datas from './Datas';
import Admin from './Admin';
export {
  UserData,Datas,Admin
};
export const CombinedReducer = combineReducers({
	UserData,Datas,Admin
});
