import React, {Component} from 'react';
const LOGO_RESOLUTION = 197.0/163.0;
const LOGO_WIDTH = 30;
const LOGO_HEIGHT = LOGO_WIDTH*LOGO_RESOLUTION;
export default class Logo extends Component {
  render() {
    return (
      <img style={{width:LOGO_WIDTH, height:LOGO_HEIGHT}}
        src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRdFdcyuxWJ56U7BuxFpUA0kh-Oywbuibygf9anjx4FRNwz86e4jHGItJRbJA" />
    );
  }
};
