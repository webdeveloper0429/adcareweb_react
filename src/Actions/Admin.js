import {CHANGE_FORM,SET_ERROR_MESSAGE} from '../Constants/AppConstants';
import * as errorMessages  from '../Constants/MessageConstants';
import auth from '../utils/auth';

/*admin_login*/
export function admin_login(attr) {
	return (dispatch) => {
      	auth.admin_login(attr, (success, err) => {
        	if (success) {
          		dispatch(changeForm({
					admin_token:localStorage.admintoken
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
export function catch_users(attr) {
	return (dispatch) => {
      	auth.catch_users(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					profiles:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*update_user*/
export function update_user(attr) {
	return (dispatch) => {
      	auth.update_user(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					profiles:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_user*/
export function catch_user(attr) {
	return (dispatch) => {
      	auth.catch_user(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					user:datas,
					profiles:[]
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_ad*/
export function catch_adlist(attr) {
	return (dispatch) => {
      	auth.catch_adlist(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					ad:datas,
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_addetail*/
export function catch_addetail(attr) {
	return (dispatch) => {
      	auth.catch_addetail(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					addetail:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_adCategory*/
export function catch_adCategory(attr) {
	return (dispatch) => {
      	auth.catch_adCategory(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					category:datas,
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_donation*/
export function catch_donation(attr) {
	return (dispatch) => {
      	auth.catch_donation(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					donation:datas,
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_userad*/
export function catch_userad(attr) {
	return (dispatch) => {
      	auth.catch_userad(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					userad:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*add_charity*/
export function add_charity(attr) {
	return (dispatch) => {
      	auth.add_charity(attr, (success, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*update_charity*/
export function update_charity(attr) {
	return (dispatch) => {
      	auth.update_charity(attr, (success, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*del_charity*/
export function del_charity(attr) {
	return (dispatch) => {
      	auth.del_charity(attr, (success, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*add_category*/
export function add_category(attr) {
	return (dispatch) => {
      	auth.add_category(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*del_category*/
export function del_category(attr) {
	return (dispatch) => {
      	auth.del_category(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*del_donation*/
export function del_donation(attr) {
	return (dispatch) => {
      	auth.del_donation(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*update_category*/
export function update_category(attr) {
	return (dispatch) => {
      	auth.update_category(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					category:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_advertisers*/
export function catch_advertisers(attr) {
	return (dispatch) => {
      	auth.catch_advertisers(attr, (success,datas,err) => {
        	if (success) {
          		dispatch(changeForm({
					vertisers:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*add_vertisers*/
export function add_vertisers(attr) {
	return (dispatch) => {
      	auth.add_vertisers(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*del_advertisers*/
export function del_advertisers(attr) {
	return (dispatch) => {
      	auth.del_advertisers(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*update_advertisers*/
export function update_advertisers(attr) {
	return (dispatch) => {
      	auth.update_advertisers(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					vertisers:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_country*/
export function catch_country(attr) {
	return (dispatch) => {
      	auth.catch_country(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					country:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*catch_charities*/
export function catch_charities(attr) {
	return (dispatch) => {
      	auth.catch_charities(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					charity:datas
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
/*add_country*/
export function add_country(attr) {
	return (dispatch) => {
      	auth.add_country(attr, (success,datas, err) => {
        	if (success) {
          		dispatch(changeForm({
					add:true
          		}));
         	}else{
        		dispatch(setErrorMessage(err));
          	}
        });
	}
}
export function admin_logout() {
  return (dispatch) => {
    auth.admin_logout((success, err) => {
      if (success === true) {
        dispatch(changeForm({
		   logout:"",
           password: ""
        }));
      } else {
        dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
      }
    });
  }
}
export function VerifyMessage(attr){
  return (dispatch) => {
    dispatch(setErrorMessage(attr));
  }
}
export function changeForm(newState) {
  return { type: CHANGE_FORM, newState };
}

function setErrorMessage(message) {
  return (dispatch) => {
    dispatch({ type: SET_ERROR_MESSAGE, message });

    //const form = document.querySelector('.form-page__form-wrapper');
	const form = document.querySelector('.form');
    if (form) {
      form.classList.add('js-form__err-animation');
      // Remove the animation class after the animation is finished, so it
      // can play again on the next error
      setTimeout(() => {
        form.classList.remove('js-form__err-animation');
      }, 150);

      // Remove the error message after 3 seconds
      setTimeout(() => {
        dispatch({ type: SET_ERROR_MESSAGE, message: '' });
      }, 3000);
    }
  }
}
