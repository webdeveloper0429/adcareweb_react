import bcrypt from 'bcryptjs';
import { UPDATE,AD_DATAS,ADD_CHARITY,PRO_DATAS,ADD_DATAS,REISSUE_MAIL,CLEAR_FORM,SET_AUTH, CHANGE_FORM, SENDING_REQUEST, SET_ERROR_MESSAGE,RESET_ERROR,EMAIL_ERROR} from '../Constants/AppConstants';
import * as errorMessages  from '../Constants/MessageConstants';
import auth from '../utils/auth';
import genSalt from '../utils/salt';
import { browserHistory} from 'react-router-dom';
import {graph} from 'fb-react-sdk';
import home from '../image/logonin.jpg'

/**
 * Logs an user in
 * @param  {string} email The username of the user to be logged in
 * @param  {string} password The password of the user to be logged in
 */
export function login(email,password) {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    // Show the loading indicator, hide the last error
    // If no username or password was specified, throw a field-missing error
    if (anyElementsEmpty({email,password})) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
    // Generate salt for password encryption
    const salt = genSalt(email);
    // Encrypt password
    bcrypt.hash(password, salt, (err, hash) => {
		console.log(hash)
      // Something wrong while hashing
      if (err) {
        dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
        return;
      }
	  let attr={
		email:email,
		password:hash
	  }
      // Use auth.js to fake a request
      auth.login(attr, (success, err,userData) => {
        // When the request is finished, hide the loading indicator
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success === true) {
          // If the login worked, forward the user to the dashboard and clear the form
          dispatch(changeForm({
			name:"",
            email: "",
            password: "",
			token:localStorage.token,
			errorMessages:"",
          }));
        } else {
		  //dispatch(clearForm('true'));
          switch (err) {
			case 'needVerify':
			  dispatch(changeForm({
				email:attr.email,
				errorMessages:err
			  }));
              dispatch(setErrorMessage(errorMessages.NEEDVERIFY));
              return;
			case 'missingStep':
              dispatch(setErrorMessage(err));
			  dispatch(changeForm({
				  userData:userData,
				  email:'',
				  password:'',
				  token:localStorage.token
			  }));
              return;
            case 'user-doesnt-exist':
              dispatch(setErrorMessage(errorMessages.USER_NOT_FOUND));
              return;
            case 'Invalid password':
              dispatch(setErrorMessage(errorMessages.WRONG_PASSWORD));
              return;
            case 'Invalid email':
              dispatch(setErrorMessage(errorMessages.EMAIL_ERROR));
              return;
            default:
			  if(err.status){
				  dispatch(changeForm({
					name:"",
					email: "",
					password: "",
					token:localStorage.token,
					userid:err.userid
				  }));
			  }else{
              	dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
			  }
              return;
          }
        }
      });

    });
  }
}
/**
 * fblogin
 */

export function facebook_login(attr) {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    auth.facebook_login(attr,(success, err) => {
	  	dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success === true) {
          // If the login worked, forward the user to the dashboard and clear the form
          dispatch(changeForm({
			token:localStorage.token,
          }));
      } else {
          switch (err) {
			case 'Invalid fb token':
              dispatch(setErrorMessage(errorMessages.RESET_ERROR));
              return;
            default:
        	  dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
		  }
      }
    });
  }
}
/**
 * google login
 */

export function google_login(attr) {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    auth.google_login(attr,(success, err) => {
	  	dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success === true) {
          // If the login worked, forward the user to the dashboard and clear the form
          dispatch(changeForm({
			token:localStorage.token,
          }));
      } else {
          switch (err) {
			case 'Invalid token':
              dispatch(setErrorMessage(err));
              return;
            default:
        	  dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
		  }
      }
    });
  }
}
/**
 * forgot
 */

export function forgot(attr) {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    auth.forgot(attr,(success, err) => {
	  dispatch(sendingRequest(false));
      if (success === true) {
        dispatch(setAuthState(false));
        //browserHistory.replace(null, '/');
        dispatch(setErrorMessage(errorMessages.REISSUE_MAIL));
        //browserHistory.replace(null, '/');
      } else {
          switch (err) {
			case 'Can only reset password once a day':
              dispatch(setErrorMessage(errorMessages.RESET_ERROR));
              return;
			case 'No such user':
              dispatch(setErrorMessage(errorMessages.USER_NOT_FOUND));
              return;
			case 'Missing email':
              dispatch(setErrorMessage(errorMessages.EMAIL_ERROR));
              return;
            default:
        	  dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
		  }
      }
		  dispatch(changeForm({
			email: attr.email,
			password: "",
			open:success
		  }));
    });
  }
}
/**
 * forgotreset
 */
export function forgotreset(email,password,code) {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    const salt = genSalt(email);
    // Encrypt password
    bcrypt.hash(password, salt, (err, hash) => {
		console.log(hash)
      // Something wrong while hashing
      if (err) {
        dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
        return;
      }
		let attr={
			 email:email,
		 	 password:hash,
			 code:code
		}
		auth.forgotreset(attr,(success, err) => {
			dispatch(sendingRequest(false));
		  if (success === true) {
			//browserHistory.replace(null, '/');
			dispatch(clearForm('true'));
			dispatch(setErrorMessage(errorMessages.SEND_CODE));
		  } else {
			dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
		  }
		});
	});
  }
}
/**
/**
 * Logs the current user out
 */
export function logout() {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    auth.logout((success, err) => {
      if (success === true) {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(false));
        dispatch(changeForm({
		   name:"",
           email: "",
           password: "",
		   token:""
        }));
      } else {
        dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
      }
    });
  }
}

/**
 * Registers a user
 * @param  {string} username The username of the new user
 * @param  {string} password The password of the new user
 */
export function register(email,password,nickname,gender,old,hate,love,charity) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no email or password was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty({email,password,nickname})) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
    // Generate salt for password encryption
    const salt = genSalt(email);
    // Encrypt password
    bcrypt.hash(password, salt, (err, hash) => {
      // Something wrong while hashing
      if (err) {
        dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
        return;
      }
      let attr={
		email:email,
		password:hash,
		nickname:nickname,
		gender:gender,
		old:old,
		hate:hate,
		love:love,
		charity:charity
	  }
      // Use auth.js to fake a request
      auth.register(attr, (success, err) => {
        // When the request is finished, hide the loading indicator
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			nickname:attr.nickname,
			email:attr.email
          }));
        } else {
		  dispatch(clearForm('true'));
          switch (err) {
            case 'Email already registered':
			  dispatch(changeForm({
				nickname:'',
				email:'',
				password:''
			  }));
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            case 'Missing field':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    });
  }
}
/*set_gender*/
export function set_gender(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.set_gender(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(attr)
        if (success) {
          dispatch(changeForm({
			nickname:attr.nickname,
			email:attr.email,
			gender:attr.gender
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*set_age*/
export function set_age(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.set_age(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(attr)
        if (success) {
          dispatch(changeForm({
			nickname:attr.nickname,
			email:attr.email,
			gender:attr.gender,
			age:attr.age
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}

/*verify*/
export function verifycode(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.verifycode(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			token:localStorage.token
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*admin_add_ad*/
export function admin_add_ad(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.admin_add_ad(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			token:localStorage.token
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*add_wallet*/
export function add_wallet(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.add_wallet(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			add:true,
			id:attr.id
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*catch_wallet*/
export function catch_wallet(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_wallet(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			add:true,
			id:attr.id
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*get_photo*/
export function get_photo(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.get_photo(attr, (success,datas, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			photo:datas
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
export function clearForm(newState) {
  return { type: CLEAR_FORM, newState };
}

/**
 * Sets the authentication state of the application
 * @param {boolean} newState True means a user is logged in, false means no user is logged in
 */
export function setAuthState(newState) {
  return { type: SET_AUTH, newState };
}

/**
 * Sets the form state
 * @param  {object} newState          The new state of the form
 * @param  {string} newState.username The new text of the username input field of the form
 * @param  {string} newState.password The new text of the password input field of the form
 * @return {object}                   Formatted action for the reducer to handle
 */
export function changeForm(newState) {
  return { type: CHANGE_FORM, newState };
}
export function adddatas(newState) {
  return { type: ADD_DATAS, newState };
}
export function addcharity(newState) {
  return { type: ADD_CHARITY, newState };
}
export function pro_datas(newState) {
  return { type: PRO_DATAS, newState };
}

/**
 * Sets the requestSending state, which displays a loading indicator during requests
 * @param  {boolean} sending The new state the app should have
 * @return {object}          Formatted action for the reducer to handle
 */
export function sendingRequest(sending) {
  return { type: SENDING_REQUEST, sending };
}

export function VerifyJudgment(attr){
  return (dispatch) => {
    dispatch(setErrorMessage(attr));
  }
}

/**
 * Sets the errorMessage state, which displays the ErrorMessage component when it is not empty
 * @param message
 */
function setErrorMessage(message) {
  return (dispatch) => {
    dispatch({ type: SET_ERROR_MESSAGE, message });

    //const form = document.querySelector('.form-page__form-wrapper');
	const form = document.querySelector('.form');
    if (form) {
      form.classList.add('js-form__err-animation');
      // Remove the animation class after the animation is finished, so it
      // can play again on the next error
      setTimeout(() => {
        form.classList.remove('js-form__err-animation');
      }, 150);

      // Remove the error message after 3 seconds
      setTimeout(() => {
        dispatch({ type: SET_ERROR_MESSAGE, message: '' });
      }, 3000);
    }
  }
}

/**
 * Forwards the user
 * @param {string} location The route the user should be forwarded to
 */
export function forwardTo(location) {
  console.log('forwardTo(' + location + ')');
  console.log(browserHistory);
  //browserHistory.push(location);
}


/**
 * Checks if any elements of a JSON object are empty
 * @param  {object} elements The object that should be checked
 * @return {boolean}         True if there are empty elements, false if there aren't
 */
function anyElementsEmpty(elements) {
  for (let element in elements) {
    if (!elements[element]) {
      return true;
    }
  }
  return false;
}

