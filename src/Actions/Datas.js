import bcrypt from 'bcryptjs';
import {AD_DATAS,ADD_CHARITY,PRO_DATAS,ADD_DATAS,REISSUE_MAIL,CLEAR_FORM,SET_AUTH, CHANGE_FORM, SENDING_REQUEST, SET_ERROR_MESSAGE,RESET_ERROR,EMAIL_ERROR} from '../Constants/AppConstants';
import * as errorMessages  from '../Constants/MessageConstants';
import auth from '../utils/auth';
import genSalt from '../utils/salt';
import { browserHistory} from 'react-router-dom';
import {graph} from 'fb-react-sdk';
import home from '../image/logonin.jpg'

/*set_hate_ad_category*/
export function set_hate_ad_category(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.set_hate_ad_category(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(attr)
        if (success) {
          dispatch(changeForm({
			love:'',
			hate:attr.ids,
			img:attr.Img
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*set_pref_charity*/
export function set_pref_charity(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    //if (anyElementsEmpty(attr)) {
    //  dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
    //  return;
   // }
      // Use auth.js to fake a request
      auth.set_pref_charity(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(attr)
        if (success) {
			localStorage.setItem('userid', attr.charityId);
          	dispatch(changeForm({
				nickname:attr.nickname,
				email:attr.email,
				gender:attr.gender,
				love:attr.love,
				charity:attr.charity,
				code:false
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*set_love_ad_category*/
export function set_love_ad_category(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.set_love_ad_category(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(attr)
        if (success) {
          dispatch(changeForm({
			love:attr.ids,
			img:attr.Img,
			rows:attr.rows
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*profile*/
export function catch_profile(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_profile(attr, (success, datas , err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(datas)
        if (success) {
          dispatch(pro_datas({rows:datas,edit:false}));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*update profile*/
export function update_profile(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.update_profile(attr, (success, datas , err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(datas)
        if (success) {
          dispatch(pro_datas({rows:datas,edit:true}));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*get_photo*/
export function get_photo(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.get_photo(attr, (success,datas, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			photo:datas
          }));
        } else {
              dispatch(setErrorMessage('No Profile Photo'));
        }
      });
    //});
  }
}
/*charity*/
export function catch_charity(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_charity(attr, (success, datas , err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
		console.log(datas)
        if (success) {
          dispatch(addcharity({
			  CharityDatas:datas
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*catch_category*/
export function catch_category(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_category(attr, (success, datas , err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(adddatas({loveDatas:datas}));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*catch_ad*/
export function catch_ad(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_ad(attr, (success, datas , err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(ad({
			  AdDatas:datas
          }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*catch_detail*/
export function catch_detail(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_detail(attr, (success, datas , err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			  rows:datas
          }));
        } else {
          switch (err) {
            case 'Error occured when loading this ad':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*send_code*/
export function send_code(attr) {
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
      // Use auth.js to fake a request
      auth.send_code(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			  code:true,
			  email:attr.email,
		 	  nickname:attr.nickname
		  }));
        } else {
          switch (err) {
            case 'Invalid Code':
              dispatch(setErrorMessage(errorMessages.USERNAME_TAKEN));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*add_wallet*/
export function add_wallet(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.add_wallet(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			add:true,
			id:attr.id
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*catch_wallet*/
export function catch_wallet(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.catch_wallet(attr, (success, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			add:true,
			id:attr.id
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*get_donation*/
export function get_donation(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.get_donation(attr, (success,datas, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			donation:datas
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*get_spending*/
export function get_spending(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.get_spending(attr, (success,datas, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			donation:datas
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*get_amount*/
export function get_amount(attr) {
	console.log(attr)
  return (dispatch) => {
    // Show the loading indicator, hide the last error
    // If no code was specified, throw a field-missing error
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      // Use auth.js to fake a request
      auth.get_amount(attr, (success,datas, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			amount:datas
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
/*get_country*/
export function get_country(attr) {
  return (dispatch) => {
	dispatch(sendingRequest(true));
    if (anyElementsEmpty(attr)) {
      dispatch(setErrorMessage(errorMessages.FIELD_MISSING));
      return;
    }
      auth.get_country(attr, (success,datas, err) => {
		dispatch(sendingRequest(false));
        dispatch(setAuthState(success));
        if (success) {
          dispatch(changeForm({
			country:datas
          }));
        } else {
          switch (err) {
            case 'Record already added':
              dispatch(setErrorMessage(err));
              return;
            case 'Error occured when adding record':
              dispatch(setErrorMessage(err));
              return;
            case 'No such ad or charity':
              dispatch(setErrorMessage(err));
              return;
            default:
              dispatch(setErrorMessage(errorMessages.GENERAL_ERROR));
              return;
          }
        }
      });
    //});
  }
}
export function clearForm(newState) {
  return { type: CLEAR_FORM, newState };
}

/**
 * Sets the authentication state of the application
 * @param {boolean} newState True means a user is logged in, false means no user is logged in
 */
export function setAuthState(newState) {
  return { type: SET_AUTH, newState };
}

/**
 * Sets the form state
 * @param  {object} newState          The new state of the form
 * @param  {string} newState.username The new text of the username input field of the form
 * @param  {string} newState.password The new text of the password input field of the form
 * @return {object}                   Formatted action for the reducer to handle
 */
export function changeForm(newState) {
  return { type: CHANGE_FORM, newState };
}
export function adddatas(newState) {
  return { type: ADD_DATAS, newState };
}
export function addcharity(newState) {
  return { type: ADD_CHARITY, newState };
}
export function ad(newState) {
  return { type: AD_DATAS, newState };
}
export function pro_datas(newState) {
  return { type: PRO_DATAS, newState };
}

/**
 * Sets the requestSending state, which displays a loading indicator during requests
 * @param  {boolean} sending The new state the app should have
 * @return {object}          Formatted action for the reducer to handle
 */
export function sendingRequest(sending) {
  return { type: SENDING_REQUEST, sending };
}

export function VerifyJudgment(attr){
  return (dispatch) => {
    dispatch(setErrorMessage(attr));
  }
}

/**
 * Sets the errorMessage state, which displays the ErrorMessage component when it is not empty
 * @param message
 */
function setErrorMessage(message) {
  return (dispatch) => {
    dispatch({ type: SET_ERROR_MESSAGE, message });

    //const form = document.querySelector('.form-page__form-wrapper');
	const form = document.querySelector('.form');
    if (form) {
      form.classList.add('js-form__err-animation');
      // Remove the animation class after the animation is finished, so it
      // can play again on the next error
      setTimeout(() => {
        form.classList.remove('js-form__err-animation');
      }, 150);

      // Remove the error message after 3 seconds
      setTimeout(() => {
        dispatch({ type: SET_ERROR_MESSAGE, message: '' });
      }, 3000);
    }
  }
}

/**
 * Forwards the user
 * @param {string} location The route the user should be forwarded to
 */
export function forwardTo(location) {
  console.log('forwardTo(' + location + ')');
  console.log(browserHistory);
  //browserHistory.push(location);
}


/**
 * Checks if any elements of a JSON object are empty
 * @param  {object} elements The object that should be checked
 * @return {boolean}         True if there are empty elements, false if there aren't
 */
function anyElementsEmpty(elements) {
  for (let element in elements) {
    if (!elements[element]) {
      return true;
    }
  }
  return false;
}

