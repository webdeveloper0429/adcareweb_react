import { LOAD, SAVE, UPDATE, DELETE } from './Constants/UserData';
export const load = () => {
  return {
    type: LOAD,
  }
}
export const update = (payload) => {
  return {
    type: UPDATE,
    payload: payload,
  };
}
export const deleteAll = () => {
  return {
    type: DELETE,
  }
}
export const save = () => {
  return {
    type: SAVE,
  }
}
