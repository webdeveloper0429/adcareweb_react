import React,{Component} from 'react';
import {Navbar,Nav,NavItem} from 'react-bootstrap';
import {connect} from 'react-redux';
import { admin_logout } from './Actions/Admin';
import {Link} from 'react-router-dom'
import styles from './sideBarMenu.css';
class AdminHeader extends Component {
	constructor(props) {
		super(props);
		const {AdminloggIn}=props.Admin;
		this.state = { AdminloggIn:AdminloggIn,appTitle: "Login",showModal: false,title:""}
		this.logout = this.logout.bind(this);
	}
	logout(){
		this.props.dispatch(admin_logout());
		console.log(this.props.history)
		this.props.history.push('./login');
	}
 
  render() {
	return (
		<div className="col-sm-3 col-lg-2">
			<div id="sidebar" className={styles.sideBarMenuContainer}>
				<Navbar fluid className={styles.sidebar} inverse >
					<Navbar.Header>
						<Navbar.Brand>
						</Navbar.Brand>
						<Navbar.Toggle />
					</Navbar.Header>
					<Navbar.Collapse>
					{localStorage.admintoken?(
						<Nav>
							<NavItem eventKey={1}><Link to={`/users`}>Profile</Link></NavItem>
							<NavItem eventKey={2}><Link to={`/AdList`}>AdList</Link></NavItem>
							<NavItem eventKey={3}><Link to={`/AddAd`}>Add Ad</Link></NavItem>
							<NavItem eventKey={4}><Link to={`/EditAd`}>Edit Ad</Link></NavItem>
							<NavItem eventKey={5}><Link to={`/CharityList`}>CharityList</Link></NavItem>
							<NavItem eventKey={7}><Link to={`/Advertisers`}>Vertiser</Link></NavItem>
							<NavItem eventKey={8}><Link to={`/Category`}>Category</Link></NavItem>
							<NavItem eventKey={9}><Link to={`/addCountry`}>Country</Link></NavItem>
							<NavItem eventKey={10} onClick={this.logout}>Logout</NavItem>
						</Nav>
						):('')}
					</Navbar.Collapse>

				</Navbar>
        	</div>
        </div>
	);
  }
}
function select(state) {
  return {
    Admin: state.Admin
  };
}
module.exports=connect(select)(AdminHeader);

