/**
 *
 * Select.react.js
 *
 * A common button, if you pass it a prop "route" it'll render a link to a react-router route
 * otherwise it'll render a link with an onclick
 */

import React, { PropTypes } from 'react';

function Select (props){
	return(
	  <div className="form-group">
		<select
		  name={props.name}
		  value={props.selectedOption}
		  onChange={props.controlFunc}
		  className="form-select">
		  <option value="">{props.placeholder}</option>
		  {props.options.map(opt => {
			return (
			  <option
				key={opt.key}
				value={opt.value}>{opt.value}</option>
			);
		  })}
		</select>
	  </div>
	  );
}

Select.propTypes = {  
  name: React.PropTypes.string.isRequired,
  options: React.PropTypes.array.isRequired,
  selectedOption: React.PropTypes.string,
  controlFunc: React.PropTypes.func.isRequired,
  placeholder: React.PropTypes.string
};

export default Select;
