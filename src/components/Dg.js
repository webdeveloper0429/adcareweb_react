import React, { Component } from 'react';
import H2 from '../components/H2';
import H4 from '../components/H4';
class Dg extends Component {
  render() {
    return(
		<div className={this.props.style}>
			<div className="row">
				<div className="col-sm-12">
					<H2>{this.props.Title}</H2>
					<H2>{this.props.Title2}</H2>
					<input type="button" name="account" value={this.props.value} className="account"/>
				</div>
			</div>
			{this.props.open?(
				<div className="row">
					<div className="col-sm-5">
						<H4>{this.props.eorned}</H4>
						<h3>{this.props.ET}<span>USD</span></h3>
					</div>
					<div className="col-sm-7">
						<H4>{this.props.donated}</H4>
						<h3>{this.props.DT}<span>USD</span></h3>
					</div>
				</div>
				):(
				<div className="row">
					<div className="col-sm-8">
					</div>
					<div className="col-sm-2">
						<button icon="cross" className="btn"  type="submit"  >{this.props.btnText}&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
					</div>
				</div>
			)}
		</div>
	);
	}
}
Dg.propTypes={
  Title: React.PropTypes.string.isRequired,
  Title2: React.PropTypes.string,
  btnText: React.PropTypes.string,
  value: React.PropTypes.string.isRequired,
  style: React.PropTypes.string.isRequired,
  eorned: React.PropTypes.string,
  id: React.PropTypes.string,
  donated: React.PropTypes.string,
  ET: React.PropTypes.string,
  DT: React.PropTypes.string,
  open: React.PropTypes.bool,
}
export default Dg;
