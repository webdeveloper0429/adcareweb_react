import React, { Component} from 'react';
import { connect } from 'react-redux';
import { fblogin} from '../Actions/AppActions';
import {graph,FB} from 'fb-react-sdk';
import {Image} from 'react-bootstrap';
import Button from '../components/Button';

class Fb extends Component {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
	  this.props.dispatch(fblogin());
	}

   render() {
	   const dispatch = this.props.dispatch;
	    const { formState} = this.props.data;
	   const wellStyles = {maxWidth: 200,height:20, margin: '0 auto 10px'};
	   const imgStyles = {padding: 0,maxWidth: 200,height:40, backgroundColor: 'rgb(66,93,174)'};
      return (
				<Button type="button" bsStyle="primary" bsSize="large"  onClick={this.handleClick}>login with facebook</Button>
      );
   }
}
function select(state) {
  return {
    data: state
  };
}
module.exports=connect(select)(Fb);

