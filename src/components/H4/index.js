import styled from 'styled-components';

const H4 = styled.h4`
  font-size: 12px;
  color: #FFFFFF;
`;

export default H4;
