import React, { Component} from 'react';
import H2 from './H2';

class DownForm extends Component {
	constructor(props) {
		super(props);
		this.onClick = this.onClick.bind(this);
	}
	onClick() {
		this.props.history.push('./')
	}

  render() {
	var pathname=this.props.history.location.pathname
	if(pathname==='/profile' || pathname==='/edit'|| pathname==='/country'){
		var color='black'
	}else{
		var color='white'
	}
    return(
		<div>
			<div className="col-sm-2">
				<a href="#" style={{color:color}}>{'Community'}</a>
			</div>
			<div className="col-sm-1">
				<a href="#" style={{color:color}}>{'FAQ'}</a>
			</div>
			<div className="col-sm-2">
				<a href="#" style={{color:color}}>{'Privacy & Trivacy'}</a>
			</div>
			<div className="col-sm-2">
				<a href="#" style={{color:color}}>{'Contact Us'}</a>
			</div>
		</div>

    );
  }

}

DownForm.propTypes = {
  color: React.PropTypes.string
}
export default DownForm;
