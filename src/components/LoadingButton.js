import React from 'react';
import LoadingIndicator from './LoadingIndicator';
import { Dimmer, Loader, Image, Segment } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
const LoadingButton = () => (
      <Dimmer active >
		 <Loader size='small'>{'Loading'}</Loader>
      </Dimmer>
)

export default LoadingButton;
