import React, { Component } from 'react';

class Input extends Component {
  render() {
    return(
            <div className="col-sm-12">
				  <input
					className={this.props.classname}
					type={this.props.type}
					value={this.props.value}
					placeholder={this.props.placeholder}
					onChange={this.props.onChange}
				  />
			</div>
	);
	}
}
Input.propTypes={
  classname: React.PropTypes.string.isRequired,
  value: React.PropTypes.string.isRequired,
  type: React.PropTypes.string.isRequired,
  placeholder: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired
}
export default Input;
