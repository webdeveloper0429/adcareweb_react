import React, { Component} from 'react';
import { changeForm,VerifyJudgment } from '../Actions/AppActions';
import LoadingButton from './LoadingButton';
import Input from '../components/Input';
import ErrorMessage from './ErrorMessage';
// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign || require('object.assign');

class VerifyForm extends Component {
	constructor(props) {
		super(props);
		this.state={confirmpw:'',code:''}
		this.changeCode = this.changeCode.bind(this);
		this.changeConfirmpw = this.changeConfirmpw.bind(this);
		this.hasEmpty = this.hasEmpty.bind(this);
	}
	 hasEmpty() {
		const {email,password} = this.props.data;
		if (!email) {
			this.props.dispatch(VerifyJudgment('email is empty'));
		  return true;
		}
		if (!password) {
			this.props.dispatch(VerifyJudgment('password is empty'));
		  return true;
		}
		if (!this.state.code) {
			this.props.dispatch(VerifyJudgment('code is empty'));
		  return true;
		}
		if (!this.state.confirmpw) {
			this.props.dispatch(VerifyJudgment('confirmpw is empty'));
		  return true;
		}
		return false;
	  }

	changeCode(evt){
		this.setState({code:evt.target.value})
	}
	changeConfirmpw(evt){
		this.setState({confirmpw:evt.target.value})
	}
  render() {
	  const button=(
		<div className="row">
			<div className="col-sm-10">
			</div>
			<div className="col-sm-2">
			  {this.props.currentlySending ? (
				<LoadingButton />
			  ) : (
				<button icon="cross" className="btn" type='submit' >{this.props.btnText}>&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
			  )}
			</div>
        </div>
			)
    return(
      <form className="form" onSubmit={this._onSubmit.bind(this)}>
	    <ErrorMessage />
		<div className="row">
			<Input classname={'email'} type={'text'} value={this.props.data.email} placeholder={'Enter Your Email'} onChange={this._changeEmail.bind(this)}/>
		</div>
		<div className="row">
			<Input classname={'code'} type={'text'} value={this.state.code} placeholder={'Enter Your Code'} onChange={this.changeCode}/>
		</div>
		<div className="row">
			<Input classname={'password'} type={'password'} value={this.props.data.password} placeholder={'Enter Your Password'} onChange={this._changePassword.bind(this)}/>
		</div>
		<div className="row">
			<Input classname={'confirmpw'} type={'password'} value={this.state.confirmpw} placeholder={'Enter Your confirmpw'} onChange={this.changeConfirmpw}/>
		</div>
		&nbsp;&nbsp;&nbsp;
		{button}
      </form>
    );
  }

  // Change the email in the app state
  _changeEmail(evt) {
    var newState = this._mergeWithCurrentState({
      email: evt.target.value
    });

    this._emitChange(newState);
  }

  // Change the password in the app state
  _changePassword(evt) {
    var newState = this._mergeWithCurrentState({
      password: evt.target.value
    });

    this._emitChange(newState);
  }
  // Merges the current state with a change
  _mergeWithCurrentState(change) {
    return assign(this.props.data, change);
  }

  // Emits a change of the form state to the application state
  _emitChange(newState) {
    this.props.dispatch(changeForm(newState));
  }

  // onSubmit call the passed onSubmit function
  _onSubmit(evt) {
    evt.preventDefault();
	if (this.hasEmpty()) return;
	if(this.props.data.password !==this.state.confirmpw){
    	this.props.dispatch(VerifyJudgment('Verify Password Wrong'));
		this.setState({confirmpw:''})
		this.props.data.password=''
		return false;
	}
    this.props.onSubmit(this.props.data.email,this.props.data.password,this.state.code,this.state.confirmpw);
  }
}

VerifyForm.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
  btnText: React.PropTypes.string.isRequired,
  data: React.PropTypes.object.isRequired
}

export default VerifyForm;
