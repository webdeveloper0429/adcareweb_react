import React, { Component } from 'react';
import { changeForm } from '../Actions/AppActions';
import LoadingButton from './LoadingButton';
import Input from '../components/Input';
import ErrorMessage from './ErrorMessage';
// Object.assign is not yet fully supported in all browsers, so we fallback to
// a polyfill
const assign = Object.assign || require('object.assign');

class LoginForm extends Component {
  render() {
    return(
      <form className="form" onSubmit={this._onSubmit.bind(this)}>
	    <ErrorMessage />
		<div className="row">
			<Input classname='email' type={'text'} value={this.props.data.email} placeholder={'Enter Your Email'} onChange={this._changeEmail.bind(this)}/>
		</div>
		<div className="row">
			<Input classname='password' type={'password'} value={this.props.data.password} placeholder={'Enter Your Password'} onChange={this._changePassword.bind(this)}/>
        </div>
		&nbsp;&nbsp;&nbsp;
		<div className="row">
			<div className="col-sm-10">
			</div>
			<div className="col-sm-2">
          {this.props.loading ? (
            <LoadingButton />
          ) : (
            <button icon="cross" className="btn"  type="submit">{this.props.btnText}>&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
          )}
			</div>
        </div>
      </form>
    );
  }

  // Change the username in the app state
  _changeEmail(evt) {
    var newState = this._mergeWithCurrentState({
      email: evt.target.value
    });
    this._emitChange(newState);
  }

  // Change the password in the app state
  _changePassword(evt) {
    var newState = this._mergeWithCurrentState({
      password: evt.target.value
    });

    this._emitChange(newState);
  }

  // Merges the current state with a change
  _mergeWithCurrentState(change) {
    return assign(this.props.data, change);
  }

  // Emits a change of the form state to the application state
  _emitChange(newState) {
    this.props.dispatch(changeForm(newState));
  }

  // onSubmit call the passed onSubmit function
  _onSubmit(evt) {
    evt.preventDefault();
    this.props.onSubmit(this.props.data.email, this.props.data.password);
  }
}

LoginForm.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
  btnText: React.PropTypes.string.isRequired,
  loading:React.PropTypes.bool,
  data: React.PropTypes.object.isRequired
}

export default LoginForm;
