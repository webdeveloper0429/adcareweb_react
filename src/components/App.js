import React, { Component } from 'react';
import Nav from './Nav';
import { connect } from 'react-redux';

class App extends Component {
  render() {
    return(
      <div className="wrapper">
        <Nav loggedIn={this.props.data.loggedIn} history={this.props.history} location={this.props.location} dispatch={this.props.dispatch} currentlySending={this.props.data.currentlySending} />
        { this.props.children }
      </div>
    )
  }
}
function select(state) {
  return {
    data: state
  };
}

module.exports=connect(select)(App);
