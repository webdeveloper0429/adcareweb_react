
/**
 *
 * Select.react.js
 *
 * A common button, if you pass it a prop "route" it'll render a link to a react-router route
 * otherwise it'll render a link with an onclick
 */

import React, { PropTypes } from 'react';

function DropdownButton(props) {
  return (
    <DropdownButton bsStyle={title.toLowerCase()} title={props.title} key={props.i} id={`dropdown-basic-${props.i}`}>
      <MenuItem eventKey="1">Action</MenuItem>
      <MenuItem eventKey="2">Another action</MenuItem>
      <MenuItem eventKey="3" active>Active Item</MenuItem>
      <MenuItem divider />
      <MenuItem eventKey="4">Separated link</MenuItem>
    </DropdownButton>
  );
}
DropdownButton.propTypes = {  
};

export default DropdownButton;
