import React, { Component } from 'react';

class CharityForm extends Component {
  _onClick(evt) {
    evt.preventDefault();
    this.props.onClick(this.props.id,this.props.value);
  }
  render() {
		return(
				<div className={this.props.classname}>
					<li><span className="charity">{this.props.description}</span></li>
					<input type="button" id={this.props.id} onClick={this._onClick.bind(this)} name={this.props.name} value={this.props.value}  className="charity_tab"/>
				</div>
		);
	}
}
 CharityForm.propTypes={
  	id:React.PropTypes.string.isRequired,
  	name:React.PropTypes.string,
  	value:React.PropTypes.string,
  	classname:React.PropTypes.string,
  	description:React.PropTypes.string,
  	onClick: React.PropTypes.func.isRequired,
}
export default CharityForm;
