import React, { Component } from 'react';
import H4 from '../components/H4';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col} from 'react-bootstrap';
import {catch_category,VerifyJudgment,get_photo} from '../Actions/AppActions';
import Select from 'react-select';
import Input from '../components/Input';
import ErrorMessage from './ErrorMessage';
import _ from 'lodash';
import jwt_decode from 'jwt-decode';
import { connect } from 'react-redux';

class EditForm extends Component {
  constructor(props) {
    super(props);
	const {domainName} = props.Datas;
	this.imageRoot=`${domainName}/adcare/webadmin/add_profile_photo`;
    this.state = {
      imagePreviewUrl:'',
	  file:'',
	  filename:''
    };
  }
  hasEmpty() {
	if (!this.props.age) {
		this.props.dispatch(VerifyJudgment('age is empty'));
	  	return true;
	}
	if (!this.props.email) {
		this.props.dispatch(VerifyJudgment('email is empty'));
	  	return true;
	}
	if (_.isEmpty(this.props.loveid)) {
		this.props.dispatch(VerifyJudgment('love is empty'));
	  	return true;
	}
	if (_.isEmpty(this.props.hateid)) {
		this.props.dispatch(VerifyJudgment('hate is empty'));
	  	return true;
	}

  }
  onSubmit(evt) {
	evt.preventDefault();
	if (this.hasEmpty()) return;
	this.props.onSubmit(this.props.gender,this.props.age,this.props.email,
						this.props.password,this.props.loveid,this.props.hateid,
						this.state.file);
  }
  onChange(type,e) {
	  var value = [];
	  var id = [];
	  for (var i = 0, l = e.length; i < l; i++) {
		  value.push({name:e[i].name,id:e[i].id});
		  if(type==='love'){
		  	id.push(e[i].id)
		  }else{
		  	id.push(e[i].id)
		  }
	  }

	this.props.onChange(type,value,id);
  }
  onbuttonChange(type,evt) {
	this.props.onbuttonChange(type,evt.target.value)
  }
  onClick(){
	this.props.onClick('True')
  }
 _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        filename: file.name,
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(file)
		var formData  = new FormData();
		var decoded=jwt_decode(localStorage.token)
		formData.append('uploadfile', file);
    	formData.append('filename',this.state.filename);
		formData.append('profileId',decoded.id);
		console.log(decoded.id)
		var Headers={
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
		const url = `${this.imageRoot}`;
		fetch(url, {
			method: 'POST',
			body:formData
		}).then(response =>{
			if (response.status >= 200 && response.status < 300) {
				return response 
			} else {
				var error = new Error(response.statusText)
				error.response = response
				throw error
			}
			}).then(function (textValue) {
				const response = textValue;
				console.log(response);
				if(response.ok){
					this.setState({
						edit: true
					});
				}
			}.bind(this)).catch(function(error) {
				console.log(error);
			});
  }
  render() {
	 	let {imagePreviewUrl} = this.state;
		let $imagePreview = null;
	   	if (imagePreviewUrl) {
			$imagePreview = (<img className="edit_img" src={imagePreviewUrl} />);
		}
		else{
			$imagePreview = (<img className="edit_img" src={this.props.src} />);
		}
		return(
      		<form className="form" id="editform" onSubmit={this.onSubmit.bind(this)}>
				<div className="row" style={{height:100}}>
					<div className="col-sm-3">
						<h1>{this.props.name},</h1>
					</div>
				</div>
				<ErrorMessage />
				<div className="row">
					<div className="col-sm-6">
						<h1>{'Account Basic,'}</h1>
					</div>
					<div className="col-sm-3">
					 	{$imagePreview}
					</div>
					<div className="col-sm-2">
						<input className='change_tab' type="file" onChange={this._handleImageChange.bind(this)} />
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel>{'Gender'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<FormControl onChange={this.onbuttonChange.bind(this,'gender')} componentClass={'select'} placeholder="select" value={this.props.selected || ''}>
							<option value="" >{'Please choose'}</option>
							<option value="Male" >Male</option>
							<option value="Female">Female</option>
						</FormControl>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel >{'Age'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<FormControl type="age" onChange={this.onbuttonChange.bind(this,'age')} value={this.props.age || ''} placeholder={"years old"} />
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel>{'Email'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<FormControl type="email" onChange={this.onbuttonChange.bind(this,'email')} value={this.props.email|| ''} placeholder={"email"} />
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel>{'Password'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<FormControl type="password" onChange={this.onbuttonChange.bind(this,'password')} value={this.props.password} placeholder={"Change you password"} />
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel>{'Language'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<FormControl onChange={this.onbuttonChange.bind(this,'language')} componentClass={'select'} placeholder="select" value={this.props.selected || ''}>
							<option value="English(USD)" >English(USD)</option>
						</FormControl>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel>{'Love'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<Select multi
							name="form-field-name"
							options={this.props.options}
							onChange={this.onChange.bind(this,'love')}
							value={this.props.value || ''}
							valueKey="id" 
							labelKey="name"
							value={this.props.loveid}
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-1">
						<FormGroup controlId="formControlsSelect">
							<ControlLabel>{'Hate'}</ControlLabel>
						</FormGroup>
					</div>
					<div className="col-sm-6">
						<Select multi
							name="form-field-name"
							options={this.props.options}
							onChange={this.onChange.bind(this,'hate')}
							value={this.props.value || ''}
							valueKey="id" 
							labelKey="name"
							value={this.props.hateid}
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-7">
					</div>
					<div className="col-sm-3" id="profile">
						<input type="submit"  value={'Save Change,'}  className="profile_tab"/>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-7">
					</div>
					<div className="col-sm-3" id="profile">
						<input type="button" onClick={this.onClick.bind(this)} value={'Cancel'}  className="profile_tab"/>
					</div>
				</div>
			</form>
		);
	}
}
 EditForm.propTypes={
  	email:React.PropTypes.string,
  	password:React.PropTypes.string,
  	selected:React.PropTypes.string,
  	name:React.PropTypes.string,
  	gender:React.PropTypes.string,
  	value:React.PropTypes.array,
  	options:React.PropTypes.array,
  	age:React.PropTypes.number,
  	loveid:React.PropTypes.array,
  	hateid:React.PropTypes.array,
	src: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.object,
	]),
  	onChange: React.PropTypes.func.isRequired,
  	onbuttonChange: React.PropTypes.func.isRequired,
  	onSubmit: React.PropTypes.func.isRequired,
  	onClick: React.PropTypes.func.isRequired,
}
function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(EditForm);
