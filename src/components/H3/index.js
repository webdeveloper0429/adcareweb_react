import styled from 'styled-components';

const H3 = styled.h3`
  font-size: 16px;
  color: #FFFFFF;
  font-weight:lighter;
`;

export default H3;
