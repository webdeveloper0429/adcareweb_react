import React, { Component} from 'react';
import AppHeader from '../AppHeader.js';
import black_logo from '../image/logo_black.svg';
import white_logo from '../image/logo_white.svg';
import kingdom from '../image/kingdom.jpg';
import japan from '../image/japan.jpg';
import usa from '../image/usa.jpg';
import hongkong from '../image/hongkong.jpg';
import france from '../image/france.jpg';
import DownForm from './DownForm.js';
import { connect } from 'react-redux';
import LoadingButton from '../components/LoadingButton';
class Backstretch extends Component {
	constructor(props) {
		super(props);
		this.state={open:false}
		this.onClick = this.onClick.bind(this);
	}
	onClick() {
		this.props.history.push('./')
	}

  render() {
	var logo='';
	let downline='';
	var pathname=this.props.history.location.pathname
	if(pathname==='/profile'|| pathname==='/ranking' || pathname==='/'){
		var image=( <img src={black_logo}/>)
	}else{
		var image=( <img src={white_logo}/>)
	}
	if(pathname==='/country'){
		downline='downline1';
		var img=(
				<div>
					<div className="row">
						<div className="col-sm-12">
							<img src={kingdom} className='kingdom' />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-3">
							<img src={usa} className='kingdom1' />
						</div>
						<div className="col-sm-3">
							<img src={japan} className='kingdom1' />
						</div>
						<div className="col-sm-3">
							<img src={france} className='kingdom1' />
						</div>
						<div className="col-sm-3">
							<img src={hongkong} className='kingdom1' />
						</div>
					</div>
				</div>
			)
	}else{
		downline='downline';
		var img=(
			<img ref="img" className={this.props.className} src={this.props.src}  />
		)
		var down=(
			<div className={downline}>
				<div className="down">
					<DownForm history={this.props.history} color={this.props.color}/>
				</div>
			</div>
		)
	}
	if(this.props.loading){
		var load=(
			<LoadingButton/>
		)
	}
    return(
		<div className="backstretch" style={{fontFamily: "Font"}}>
			{img}
			<AppHeader history={this.props.history}/>
			<button className='logo' type="button" onClick={this.onClick}>{image}</button>
			<div  className="col-sm-3">
			</div>
			{down}
		</div>
    );
  }

}

Backstretch.propTypes = {
  src: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.object,
  ]).isRequired,
  loading:React.PropTypes.bool,
  className: React.PropTypes.string,
  color: React.PropTypes.string,
  open: React.PropTypes.string,
  history: React.PropTypes.object,
}
export default Backstretch;
