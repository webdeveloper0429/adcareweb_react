import React, { Component} from 'react';
import { connect } from 'react-redux';
import {catch_adlist} from '../../Actions/Admin';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
class BSTable extends React.Component {
  render() {
    if (this.props.data) {
		let rows=[]
		var datas = _.map(this.props.data, function(n, key) {
			rows.push(n)
		});
		console.log(rows)
      return (
        <BootstrapTable data={rows}>
          <TableHeaderColumn dataField='id' isKey={true} >id</TableHeaderColumn>
          <TableHeaderColumn dataField='url' >Url</TableHeaderColumn>
          <TableHeaderColumn dataField='coverUrl'>coverUrl</TableHeaderColumn>
          <TableHeaderColumn dataField='backgroundUrl'>backgroundUrl</TableHeaderColumn>
        </BootstrapTable>);
    } else {
      return (<p>?</p>);
    }
  }
}

class AdListPage extends Component {
	constructor(props) {
		super(props);
		this.afterSaveCell = this.afterSaveCell.bind(this);
		this.expandColumnComponent = this.expandColumnComponent.bind(this);
		this.expandComponent = this.expandComponent.bind(this);
		this.handleRowSelect = this.handleRowSelect.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{
			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_adlist(attr));
		}
	}
	renderCustomClearSearch = (onClick) => {
		return (
		  <button
			className='btn btn-success'
			onClick={ onClick }>
			Empty
		  </button>
		);
	}
	afterSaveCell(row, cellName, cellValue) {
	  // do your stuff...
		console.log(row)
		console.log(cellName)
		console.log(cellValue)
		let attr={
			id:row.id,
			email:row.email,
			age:row.age,
			gender:row.gender,
			nickname:row.nickname,
			token:localStorage.admintoken
		}
		//this.props.dispatch(catch_ad(attr));
	}
	handleRowSelect(row, isSelected, e) {
		//this.props.history.push('./info/'+row.id);
	}
	isExpandableRow(row) {
		return true;
	}
	expandComponent(row) {
		console.log(row)
		return (
		  <BSTable data={ row.expand } />
		);
	}
	expandColumnComponent({ isExpanded }) {
		let content = '';

		content = (isExpanded ? '(-)' : '(+)' );
		return (
		  <div> { content } </div>
		);
	  }


	render() {
		function addProducts(quantity) {
			const products=[]
			//let rows=[] 
			var datas=_.map(quantity, function(n, key) {
				console.log(n);
				products.push({
					id:n.id,
					categoryId:n.categoryId,
					url:n.url,
					expand: [ {coverUrl:n.coverUrl}, {backgroundUrl:n.backgroundUrl}]
				});
				//rows.push(products)
			});
			//console.log(rows)
		}

		const {ad} = this.props.Admin;
		const dispatch=this.props.dispatch;
		let rows=[] 
		if(!_.isEmpty(ad)){
			//addProducts(ad);
			var datas = _.map(ad, function(n, key) {
				console.log(n.id)
				rows.push({
					id:n.id,
					categoryId:n.categoryId,
					title:n.title,
					subtitle:n.subtitle,
					amount:n.amount,
					datetime:n.datetime,
					expand: [ {id:n.id,url:n.url,coverUrl:n.coverUrl,backgroundUrl:n.backgroundUrl}]
				});
			}.bind(this))
			console.log(rows)
		}
		const options = {
            noDataText:"no datas",
			defaultSortName: 'id',
			defaultSortOrder: 'asc',
      		clearSearch: true,
      		clearSearchBtn: this.renderCustomClearSearch,
            sizePerPageList: [ 10, 50, 100, 500 ],
            sizePerPage: 10, 
			expandRowBgColor: 'rgb(242, 255, 163)',
			paginationShowsTotal: (start, to, total) =>(<span style={{color:'black'}}>從第 { start } 筆到第 { to } 筆資料, 共有 { total } 筆資料</span>),
      		//searchPanel: (props) => (<MySearchPanel { ...props }/>)
    	};
		const cellEditProp = {
			mode: 'click',
			//iblurToSave: true
			afterSaveCell: this.afterSaveCell
		};
		 const selectRow = {
			mode: 'checkbox',  // multi select
			clickToSelect: true,
			bgColor: 'pink',
			clickToExpand: true,
			onSelect: this.handleRowSelect
		  };
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch} history={this.props.history}/>

					<form className="form" id="editform" onSubmit={this.onSubmit} >
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>AdList</h1>
							</div>
						</div>
                    	<div className="container-fluid">
                            <div className='margin-bottom-10'>
                                <BootstrapTable ref="retailstores" 
												data={rows} 
												striped={true} 
												hover={true} 
												height={500} 
												keyField="id" 
												options={options} 
                                                selectRow={selectRow}
												cellEdit={ cellEditProp }
												insertRow={true}
												expandableRow={ this.isExpandableRow }
												expandComponent={ this.expandComponent }
												expandColumnOptions={ {
												  expandColumnVisible: true,
												  expandColumnComponent: this.expandColumnComponent,
												  columnWidth: 50
												} }
												search pagination  multiColumnSearch>
                                    <TableHeaderColumn dataField="id" width ="50px" dataSort={true} searchable={true} editable={false}>{'id'}</TableHeaderColumn>
									<TableHeaderColumn dataField="categoryId" width ="100px" dataSort={true} searchable={false} editable={false}>{'categoryId'}</TableHeaderColumn>
									<TableHeaderColumn dataField="title" width ="200px" dataSort={true} searchable={true} editable={false}>{'title'}</TableHeaderColumn>
									<TableHeaderColumn dataField="subtitle" width ="300px" dataSort={true} searchable={true} editable={false}>{'subtitle'}</TableHeaderColumn>
									<TableHeaderColumn dataField="amount" width ="300px" dataSort={true} searchable={true} editable={false}>{'amount'}</TableHeaderColumn>
									<TableHeaderColumn dataField="datetime" width ="300px" dataSort={true} searchable={true} editable={false}>{'datetime'}</TableHeaderColumn>
                                </BootstrapTable>
                            </div>
                   		 </div>
					</form>
				</div>
			);
	}
}

function select(state) {
  return {
    Admin: state.Admin
  };
}
module.exports=connect(select)(AdListPage);
