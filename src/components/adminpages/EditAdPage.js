import React, { Component} from 'react';
import { connect } from 'react-redux';
import {VerifyMessage,catch_adlist,catch_addetail,catch_advertisers} from '../../Actions/Admin';
import Backstretch from '../Backstretch';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col,TextField} from 'react-bootstrap';
import Select from 'react-select';
import ErrorMessage from '../ErrorMessage';
class EditAdPage extends Component {
	constructor(props) {
		super(props);
		const {domainName} = props.Datas;
		this.imageRoot=`${domainName}/adcare/webadmin/add_photo`;
		this.dataRoot=`${domainName}/adcare/webadmin/update_ad`;
		this.state={categoryId:'',title:'',subtitle:'',coverUrl:'',url:'',
					backgroundUrl:'',amount:'',options:[],value:'',open:false,
					file:'',imageType:'',imagePreviewUrl:'',filename:''}
		this.onSubmit = this.onSubmit.bind(this);
		this.onClick = this.onClick.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onselectChange = this.onselectChange.bind(this);
		this.onCategoryselectChange = this.onCategoryselectChange.bind(this);
    	this._handleImageChange = this._handleImageChange.bind(this);
    	this._handleSubmit = this._handleSubmit.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{
			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_adlist(attr));
			this.props.dispatch(catch_advertisers(attr));
		
		}
	}
	componentWillReceiveProps(nextProps) {
		if(this.state.open && nextProps.Admin.addetail){
			console.log(nextProps.Admin.addetail)
			this.setState({
				categoryId:nextProps.Admin.addetail.categoryId || null,
				title:nextProps.Admin.addetail.title || null,
				subtitle:nextProps.Admin.addetail.subtitle|| null,
				coverUrl:nextProps.Admin.addetail.coverUrl|| null,
				url:nextProps.Admin.addetail.url|| null,
				backgroundUrl:nextProps.Admin.addetail.backgroundUrl|| null,
				amount:nextProps.Admin.addetail.amount|| null,
			});
		
		}
		
	}
	onselectChange(e) {
		var value=[]
		value.push({name:e.name,id:e.id});
		this.setState({value:e.id,open:true})
		let attr={
			token:localStorage.admintoken,
			id:e.id
		}
		this.props.dispatch(catch_addetail(attr));
	}
	onCategoryselectChange(e) {
		var value=[]
		value.push({name:e.name,id:e.id});
		this.setState({categoryId:e.id,open:true})
	}
	onClick(){
		this.setState({
			categoryId:'',
			title:'',
			subtitle:'',
			coverUrl:'',
			url:'',
			backgroundUrl:'',
			amount:'',
			open:false
		});
	
	}
  	hasEmpty() {
		if (!this.state.categoryId) {
			this.props.dispatch(VerifyMessage('categoryId is empty'));
	  		return true;
		}
		if (!this.state.title) {
			this.props.dispatch(VerifyMessage('title is empty'));
	  		return true;
		}
		if (!this.state.subtitle) {
			this.props.dispatch(VerifyMessage('subtitle is empty'));
	  		return true;
		}
		if (!this.state.coverUrl) {
			this.props.dispatch(VerifyMessage('coverUrl is empty'));
	  		return true;
		}
		if (!this.state.amount) {
			this.props.dispatch(VerifyMessage('amount is empty'));
	  		return true;
		}
		if (!this.state.url) {
			this.props.dispatch(VerifyMessage('url is empty'));
	  		return true;
		}
		if (!this.state.backgroundUrl) {
			this.props.dispatch(VerifyMessage('backgroundUrl is empty'));
	  		return true;
		}
  	}
	onChange(evt){
		 switch (evt.target.placeholder) {
			 case 'categoryId':
				this.setState({categoryId:evt.target.value})
				return;
			 case 'title':
				this.setState({title:evt.target.value})
				return;
			 case 'subtitle':
				this.setState({subtitle:evt.target.value})
				return;
			 case 'coverUrl':
				this.setState({coverUrl:evt.target.value})
				return;
			 case 'url':
				this.setState({url:evt.target.value})
				return;
			 case 'backgroundUrl':
				this.setState({backgroundUrl:evt.target.value})
				return;
			 case 'Amount':
				this.setState({amount:evt.target.value})
				return;
			 default:
				return;
		}
	
	}
  _handleSubmit(e) {
    e.preventDefault();
	//var buf = new Buffer(this.state.imagePreviewUrl.replace(/^data:image\/\w+;base64,/, ""),'base64');
	var formData  = new FormData();
	formData.append('adId', this.state.value);
	formData.append('uploadfile', this.state.file);
    formData.append('filename',this.state.filename);
    formData.append('imageType',this.state.imageType);
  	var Headers={
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	}
	const url = `${this.imageRoot}`;
	fetch(url, {
  		method: 'POST',
  		body:formData
	}).then(response =>{
        if (response.status >= 200 && response.status < 300) {
            return response 
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
        }).then(function (textValue) {
			const response = textValue;
            console.log(response);
			if(response.ok){
				console.log(this.props)
				this.props.dispatch(VerifyMessage('Image is insert'));
			  	this.setState({
					file: '',
					imageType:'',
					filename: '',
					imagePreviewUrl: ''
			  	});
			}
		}.bind(this)).catch(function(error) {
            console.log(error);
        });
  }
 _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
		imageType:file.type,
        filename: file.name,
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(file)
  }
	onSubmit(evt) {
		evt.preventDefault();
		if (this.hasEmpty()) return;
		var formData  = new FormData();
		formData.append('categoryId', this.state.categoryId);
		formData.append('title',this.state.title);
		formData.append('subtitle',this.state.subtitle);
		formData.append('coverUrl',this.state.coverUrl);
		formData.append('url',this.state.url);
		formData.append('backgroundUrl',this.state.backgroundUrl);
		formData.append('amount',this.state.amount);
		formData.append('id',this.state.value);
		const url = `${this.dataRoot}`;
		fetch(url, {
			method: 'POST',
			body:formData
		}).then(response =>{
			if (response.status >= 200 && response.status < 300) {
				return response 
			} else {
				var error = new Error(response.statusText)
				error.response = response
				throw error
			}
			}).then(function (textValue) {
				const response = textValue;
				console.log(response);
				if(response.ok){
					this.props.dispatch(VerifyMessage('Ad is update'));
					this.setState({
						categoryId:'',
						title:'',
						subtitle:'',
						coverUrl:'',
						url:'',
						backgroundUrl:'',
						amount:'',
						open:false
					});
				}
			}.bind(this)).catch(function(error) {
				console.log(error);
			});
  	}
	render() {
		const dispatch = this.props.dispatch;
		const height=110;
		const {ad,vertisers} = this.props.Admin;
		if(_.toArray(ad)){
			var options=[]
			var datas = _.map(ad, function(n, key) {
				options.push({id:n.id,name:n.title})
			})
		}
		if(_.toArray(vertisers)){
			var Categoryoptions=[]
			var datas = _.map(vertisers, function(n, key) {
				Categoryoptions.push({id:n.id,name:n.name})
			})
		}
		let {imagePreviewUrl} = this.state;
		let $imagePreview = null;
		if (imagePreviewUrl) {
		  $imagePreview = (<img className="ad_imag" src={imagePreviewUrl} />);
		}
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch} history={this.props.history}/>
					<form className="form" id="editform" onSubmit={this.onSubmit} >
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>Edit Ad</h1>
							</div>
						</div>
						<ErrorMessage />
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControlsSelect">
									<ControlLabel>{'Ad'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<Select 
									name="form-field-name"
									options={options}
									onChange={this.onselectChange}
									value={this.state.value}
									valueKey="id" 
									labelKey="name"
								/>
							</div>
						</div>
						{this.state.open?(
						<div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControlsSelect">
										<ControlLabel>{'CategoryId'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<Select 
										name="form-field-name"
										options={Categoryoptions}
										onChange={this.onCategoryselectChange}
										value={this.state.categoryId}
										valueKey="id" 
										labelKey="name"
									/>
								</div>
							</div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControls">
										<ControlLabel >{'Title'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<FormControl type="title" onChange={this.onChange} value={this.state.title} placeholder={"title"} />
								</div>
							</div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControls">
										<ControlLabel >{'Subtitle'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<FormControl type="text" onChange={this.onChange} value={this.state.subtitle} placeholder={"subtitle"} />
								</div>
							</div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControls">
										<ControlLabel >{'CoverUrl'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<FormControl type="coverUrl" onChange={this.onChange} value={this.state.coverUrl} placeholder={"coverUrl"} />
								</div>
							</div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControls">
										<ControlLabel >{'Ad url'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<FormControl type="adUrl" onChange={this.onChange} value={this.state.url} placeholder={"url"} />
								</div>
							</div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControls">
										<ControlLabel >{'bg Url'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<FormControl type="backgroundUrl" onChange={this.onChange} value={this.state.backgroundUrl} placeholder={"backgroundUrl"} />
								</div>
							</div>
							<div className="row">
								<div className="col-sm-1">
									<FormGroup controlId="formControls">
										<ControlLabel >{'Amount'}</ControlLabel>
									</FormGroup>
								</div>
								<div className="col-sm-6">
									<FormControl type="amount" onChange={this.onChange} value={this.state.amount} placeholder={"amount"} />
								</div>
							</div>
							<div>
								<form onSubmit={this._handleSubmit}>
								  <input type="file" onChange={this._handleImageChange} />
								  <button type="submit" onClick={this._handleSubmit}>Upload Image</button>
								</form>
							</div>
							<div className="row">
								<div className="col-sm-12 col-lg-12">
									{$imagePreview}
								</div>
							</div>
							<div className="row">
								<div className="col-sm-7">
								</div>
								<div className="col-sm-3" id="profile">
									<input type="submit"  value={'Submit,'}  className="profile_tab"/>
								</div>
							</div>
							<div className="row">
								<div className="col-sm-7">
								</div>
								<div className="col-sm-3" id="profile">
									<input type="button"  value={'Cancel'}  onClick={this.onClick} className="profile_tab"/>
								</div>
							</div>
						</div>
						):("")}
					</form>
				</div>
			);
	}
}

function select(state) {
  return {
    Datas: state.Datas,
    Admin: state.Admin
  };
}
module.exports=connect(select)(EditAdPage);
