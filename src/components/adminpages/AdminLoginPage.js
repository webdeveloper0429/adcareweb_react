import React, { Component} from 'react';
import { connect } from 'react-redux';
import {admin_login,VerifyMessage} from '../../Actions/Admin';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col,TextField} from 'react-bootstrap';
import ErrorMessage from '../ErrorMessage';
class AdminLoginPage extends Component {
	constructor(props) {
		super(props);
		this.state={name:'',password:''}
		this.onSubmit = this.onSubmit.bind(this);
		this.onClick = this.onClick.bind(this);
		this.onChange = this.onChange.bind(this);
	}
	componentDidMount() {
 		if(localStorage.admintoken){
			this.props.history.push('./users')
		}
	}
	onClick(){
		this.setState({
			name:'',
			password:''
		});
	
	}
  	hasEmpty() {
		if (!this.state.name) {
			this.props.dispatch(VerifyMessage('name is empty'));
	  		return true;
		}
		if (!this.state.password) {
			this.props.dispatch(VerifyMessage('password is empty'));
	  		return true;
		}
  	}
	onChange(evt){
		 switch (evt.target.placeholder) {
			 case 'name':
				this.setState({name:evt.target.value})
				return;
			 case 'password':
				this.setState({password:evt.target.value})
				return;
		}
	
	}
	componentWillReceiveProps (nextProps) {
		if(nextProps.Admin.admin_token){
			this.props.history.push('./users')
		}
	}
	onSubmit(evt) {
		evt.preventDefault();
		if (this.hasEmpty()) return;
		let attr={
			name:this.state.name,
			password:this.state.password	
		}
		this.props.dispatch(admin_login(attr));
  	}
	render() {
		const dispatch = this.props.dispatch;
		const height=110;
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch}/>
					<form className="form" id="editform" onSubmit={this.onSubmit} >
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>Login</h1>
							</div>
						</div>
						<ErrorMessage />
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'name'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="text"  onChange={this.onChange} value={this.state.name} placeholder={"name"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'password'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="password"  onChange={this.onChange} value={this.state.password} placeholder={"password"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
							</div>
							<div className="col-sm-2">
								<button type="submit" >{'Sign in'}</button>
								&nbsp;&nbsp;
								<button type="button" onClick={this.onClick}>{'Back'}</button>
							</div>
							<div className="col-sm-2" id="profile">
							</div>
						</div>
					</form>
				</div>
			);
	}
}

function select(state) {
  return {
    Admin: state.Admin
  };
}
module.exports=connect(select)(AdminLoginPage);
