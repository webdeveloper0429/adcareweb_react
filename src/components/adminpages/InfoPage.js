import React, { Component} from 'react';
import { connect } from 'react-redux';
import {catch_user,update_user,catch_userad,catch_donation} from '../../Actions/Admin';
import {catch_category} from '../../Actions/Datas';
import AdminHeader from '../../AdminHeader';
import AdTabs from './AdTabs';
import DonationTabs from './DonationTabs';
import _ from 'lodash';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col,TextField} from 'react-bootstrap';
import ErrorMessage from '../ErrorMessage';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

class RegionsEditor extends React.Component {
  constructor(props) {
    super(props);
    this.updateData = this.updateData.bind(this);
    this.state = { regions: props.defaultValue ,option:props.loveDatas};
    this.onToggleRegion = this.onToggleRegion.bind(this);
  }
	componentWillReceiveProps (nextProps) {
		if(nextProps.Datas.data){
      		this.setState({option: nextProps.Datas.data});
		}
	}
  focus() {
  }
  onToggleRegion(event) {
	console.log(event)
    const region = event.currentTarget.name;
    if (this.state.regions.indexOf(region) < 0) {
      this.setState({ regions: this.state.regions.concat([ region ]) });
    } else {
      this.setState({ regions: this.state.regions.filter(r => r !== region) });
    }
  }
  updateData() {
    this.props.onUpdate(this.state.regions);
  }
  render() {
	const regions = this.state.option;
    const regionCheckBoxes = regions.map(region => (
      <span key={ `span-${region.name}` }>
        <input
          type='checkbox'
          key={ region.id }
          name={ region.name }
          checked={ this.state.regions.indexOf(region.id) > -1 }
          onKeyDown={ this.props.onKeyDown }
          onChange={ this.onToggleRegion } />
        <label key={ `label-${region.id}` } htmlFor={ region.id }>{ region.name }</label>
      </span>
    ));
    return (
      <span ref='inputRef'>
        { regionCheckBoxes }
        <button
          className='btn btn-info btn-xs textarea-save-btn'
          onClick={ this.updateData }>
          save
        </button>
      </span>
    );
  }
}

class InfoPage extends Component {
	constructor(props) {
		super(props);
		this.state={datas:[],userad:[]}
		this.afterSaveCell = this.afterSaveCell.bind(this);
		this.handleRowSelect = this.handleRowSelect.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{
			let attr={
				id:this.props.match.params.id,
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_user(attr));
			this.props.dispatch(catch_category(attr));
			this.props.dispatch(catch_userad(attr));
			this.props.dispatch(catch_donation(attr));
		}
	}
	afterSaveCell(row, cellName, cellValue) {
	  // do your stuff...
		console.log(row)
		console.log(cellName)
		console.log(cellValue)
		let attr={
			id:row.id,
			email:row.email,
			age:row.age,
			gender:row.gender,
			nickname:row.nickname,
			token:localStorage.admintoken
		}
		this.props.dispatch(update_user(attr));
	}
	componentWillReceiveProps (nextProps) {
		if(nextProps.Admin.user){
			let love_datas=[];
			_.forEach(nextProps.Admin.user, function(value, key) {
				if (key==='hateAdCategory'){
					_.forEach(value, function(v, k) {
						love_datas.push(v.name)
					});
				}
			});
			this.setState({datas:love_datas})
		}
	}
	renderCustomClearSearch = (onClick) => {
		return (
		  <button
			className='btn btn-success'
			onClick={ onClick }>
			Empty
		  </button>
		);
	}
	handleRowSelect(row, isSelected, e) {
	}

	render() {
		const {user} = this.props.Admin;
		const {loveDatas} = this.props.Datas;
		const dispatch = this.props.dispatch;
		const {datas} = this.state;
		let rows=[] 
		let love_datas=[]
		if(!_.isEmpty(user)){
			let data={}
			_.forEach(user, function(value, key) {
				data[key]=value;
			});
			rows.push(data)
		}
		const options = {
            noDataText:"no datas",
			defaultSortName: 'id',
			defaultSortOrder: 'asc',
      		clearSearch: true,
      		clearSearchBtn: this.renderCustomClearSearch,
            sizePerPageList: [ 10, 50, 100, 500 ],
            sizePerPage: 10, 
			paginationShowsTotal: (start, to, total) =>(<span style={{color:'black'}}>從第 { start } 筆到第 { to } 筆資料, 共有 { total } 筆資料</span>),
      		//searchPanel: (props) => (<MySearchPanel { ...props }/>)
    	};
		const cellEditProp = {
			mode: 'click',
			//iblurToSave: true
			afterSaveCell: this.afterSaveCell
		};
		 const selectRow = {
			mode: 'checkbox',  // multi select
			clickToSelect: true,
			bgColor: 'pink',
			onSelect: this.handleRowSelect
		  };
		function regionsFormatter(cell, rows){
			let datas=[]
			_.forEach(cell, function(v, k) {
				datas.push(v.name)
			});
			return	(<span style={{color:'black'}}>{ ( datas|| []).join(',') }</span>)
		}
		function Formatter(cell, rows){
			let datas=[]
			if(!_.isNull(cell)){
				datas.push(cell.name)
			}else{
				datas=[]
			}
			return	(<span style={{color:'black'}}>{ (datas).join(',') }</span>)
		}
		const createRegionsEditor = (onUpdate, props) => (<RegionsEditor loveDatas={loveDatas} onUpdate={ onUpdate } {...props}/>);
	
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch} history={this.props.history}/>

					<div className="form" id="editform">
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>Users</h1>
							</div>
						</div>
                    	<div className="container-fluid">
                            <div className='margin-bottom-10'>
                                <BootstrapTable ref="retailstores" 
												data={rows} 
												striped={true} 
												hover={true} 
												height={90} 
												keyField="id" 
												options={options} 
                                                selectRow={selectRow}
												cellEdit={cellEditProp}
												insertRow={false}
												search  multiColumnSearch>
                                    <TableHeaderColumn dataField="userid" width ="100px" dataSort={true} searchable={true} >{'userid'}</TableHeaderColumn>
                                    <TableHeaderColumn dataField="email" width ="200px" dataSort={true} searchable={true}>{'email'}</TableHeaderColumn>
									<TableHeaderColumn dataField="nickname" width ="100px" dataSort={true} searchable={true}>{'nickname'}</TableHeaderColumn>
									<TableHeaderColumn dataField="age" width ="100px" dataSort={true} searchable={true}>{'age'}</TableHeaderColumn>
									<TableHeaderColumn dataField="gender" width ="100px" dataSort={true} searchable={true} editable={ { type: 'select', options:{values: ['Female','Male']}} }>{'gender'}</TableHeaderColumn>
									<TableHeaderColumn dataField="hateAdCategory" width ="200px" dataSort={true} dataFormat={regionsFormatter} editable={false}>{'hate'}</TableHeaderColumn>
									<TableHeaderColumn dataField="loveAdCategory" width ="200px" dataSort={true} dataFormat={ regionsFormatter } editable={false}>{'love'}</TableHeaderColumn>
									<TableHeaderColumn dataField="prefCharity" width ="150px" dataSort={true} searchable={true} dataFormat={Formatter} editable={false}>{'Charity'}</TableHeaderColumn>
                                </BootstrapTable>
                            </div>
                   		 </div>
					</div>
					<div className="form" id="editform1">
						<div className="row" style={{height:100}}>
							<div className="col-sm-12">
							 <Tabs>
								<TabList className="list-inline">
								  <Tab style={{color:'black'}}><button className='btn btn-success'>{'Ad Times'}</button></Tab>
								  <Tab style={{color:'black'}}><button className='btn btn-success'>{'Donations'}</button></Tab>
								  <Tab style={{color:'black'}}><button className='btn btn-success'>{'Transactions'}</button></Tab>
								  <Tab style={{color:'black'}}><button className='btn btn-success'>{'Payout'}</button></Tab>
								  <Tab style={{color:'black'}}><button className='btn btn-success'>{'Notification'}</button></Tab>
								</TabList>
								<TabPanel>
									<AdTabs dispatch={this.props.dispatch} props={this.props} id={this.props.match.params.id}/>
								</TabPanel>
								<TabPanel>
									<DonationTabs open={false} dispatch={this.props.dispatch}/>
								</TabPanel>
								<TabPanel>
									<DonationTabs open={true} dispatch={this.props.dispatch}/>
								</TabPanel>
								<TabPanel>
								</TabPanel>
								<TabPanel>
								</TabPanel>
							  </Tabs>
							</div>
						</div>
					</div>
				</div>
			);
	}
}

function select(state) {
  return {
    Admin: state.Admin,
    Datas: state.Datas
  };
}
module.exports=connect(select)(InfoPage);
