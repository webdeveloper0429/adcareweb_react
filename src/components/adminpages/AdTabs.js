import React, { Component} from 'react';
import { connect } from 'react-redux';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

class AdTabs extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }
	}
	renderCustomClearSearch = (onClick) => {
		return (
		  <button
			className='btn btn-success'
			onClick={ onClick }>
			Empty
		  </button>
		);
	}

	render() {
		const options = {
            noDataText:"no datas",
			defaultSortName: 'id',
			defaultSortOrder: 'asc',
      		clearSearch: true,
      		clearSearchBtn: this.renderCustomClearSearch,
            sizePerPageList: [ 10, 50, 100, 500 ],
            sizePerPage: 10, 
			paginationShowsTotal: (start, to, total) =>(<span style={{color:'black'}}>從第 { start } 筆到第 { to } 筆資料, 共有 { total } 筆資料</span>),
      		//searchPanel: (props) => (<MySearchPanel { ...props }/>)
    	};
		 const selectRow = {
			mode: 'checkbox',  // multi select
			clickToSelect: true,
			bgColor: 'pink',
			onSelect: this.handleRowSelect
		  };
		const {userad} = this.props.Admin;
		let adrows=[]
		if(!_.isEmpty(userad)){
 			var datas = _.map(userad, function(n, key) {
                let data={}
                _.forEach(n, function(value, key) {
                	_.forEach(value, function(v, k) {
                    	data[k]=v;
                	});
                });
                adrows.push(data)
            }.bind(this))
		}
		 function priceFormatter(cell, row) {
			return `<i class='glyphicon glyphicon-usd'></i> ${cell}`;
		 }
	
		return (
				<div className="form" >
				<h1>{'Ad times'}</h1>
				<div className="container-fluid">
					<div className='margin-bottom-10'>
						<BootstrapTable ref="adstores" 
										data={adrows} 
										striped={true} 
										hover={true} 
										height={500} 
										keyField="id" 
										options={options} 
										selectRow={selectRow}
										insertRow={false}
										search pagination  multiColumnSearch>
							<TableHeaderColumn dataField="id" width ="50px" dataSort={true} searchable={true} >{'id'}</TableHeaderColumn>
							<TableHeaderColumn dataField="title" width ="100px" dataSort={true} searchable={true} >{'title'}</TableHeaderColumn>
							<TableHeaderColumn dataField="subtitle" width ="100px" dataSort={true} searchable={true} >{'subtitle'}</TableHeaderColumn>
							<TableHeaderColumn dataField="amount" dataFormat={priceFormatter} width ="100px" dataSort={true} searchable={true}>{'amount'}</TableHeaderColumn>
							<TableHeaderColumn dataField="url" width ="250px" dataSort={true} searchable={false}>{'url'}</TableHeaderColumn>
							<TableHeaderColumn dataField="coverUrl" width ="250px" dataSort={true} searchable={false}>{'coverUrl'}</TableHeaderColumn>
							<TableHeaderColumn dataField="backgroundUrl" width ="250px" dataSort={true} searchable={false}>{'backgroundUrl'}</TableHeaderColumn>
							<TableHeaderColumn dataField="categoryId" width ="100px" dataSort={true} searchable={true}>{'categoryId'}</TableHeaderColumn>
							<TableHeaderColumn dataField="datetime" width ="150px" dataSort={true} searchable={true}>{'datetime'}</TableHeaderColumn>
						</BootstrapTable>
					</div>
				 </div>
				 </div>
			);
	}
}
function select(state) {
  return {
    Admin: state.Admin,
    Datas: state.Datas
  };
}
module.exports=connect(select)(AdTabs);
