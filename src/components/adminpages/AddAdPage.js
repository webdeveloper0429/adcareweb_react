import React, { Component} from 'react';
import { connect } from 'react-redux';
import {catch_advertisers,VerifyMessage} from '../../Actions/Admin';
import Backstretch from '../Backstretch';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col,TextField} from 'react-bootstrap';
import Select from 'react-select';
import ErrorMessage from '../ErrorMessage';
class AddAdPage extends Component {
	constructor(props) {
		super(props);
		const {domainName} = props.Datas;
		this.imageRoot=`${domainName}/adcare/webadmin/add_ad`;
		this.state={categoryId:'',title:'',subtitle:'',coverUrl:'',url:'',backgroundUrl:'',amount:''}
		this.onSubmit = this.onSubmit.bind(this);
		this.onClick = this.onClick.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onselectChange = this.onselectChange.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{
			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_advertisers(attr));
		}
	}
	onselectChange(e) {
		var value=[]
		value.push({name:e.name,id:e.id});
		this.setState({categoryId:e.id,open:true})
		let attr={
			token:localStorage.token,
			id:e.id
		}
		//this.props.dispatch(catch_detail(attr));
	}
	onClick(){
		this.setState({
			categoryId:'',
			title:'',
			subtitle:'',
			coverUrl:'',
			url:'',
			backgroundUrl:'',
			amount:''
		});
	
	}
  	hasEmpty() {
		if (!this.state.categoryId) {
			this.props.dispatch(VerifyMessage('categoryId is empty'));
	  		return true;
		}
		if (!this.state.title) {
			this.props.dispatch(VerifyMessage('title is empty'));
	  		return true;
		}
		if (!this.state.subtitle) {
			this.props.dispatch(VerifyMessage('subtitle is empty'));
	  		return true;
		}
		if (!this.state.coverUrl) {
			this.props.dispatch(VerifyMessage('coverUrl is empty'));
	  		return true;
		}
		if (!this.state.amount) {
			this.props.dispatch(VerifyMessage('amount is empty'));
	  		return true;
		}
		if (!this.state.url) {
			this.props.dispatch(VerifyMessage('url is empty'));
	  		return true;
		}
		if (!this.state.backgroundUrl) {
			this.props.dispatch(VerifyMessage('backgroundUrl is empty'));
	  		return true;
		}
  	}
	onChange(evt){
		 switch (evt.target.placeholder) {
			 case 'categoryId':
				this.setState({categoryId:evt.target.value})
				return;
			 case 'title':
				this.setState({title:evt.target.value})
				return;
			 case 'subtitle':
				this.setState({subtitle:evt.target.value})
				return;
			 case 'coverUrl':
				this.setState({coverUrl:evt.target.value})
				return;
			 case 'url':
				this.setState({url:evt.target.value})
				return;
			 case 'backgroundUrl':
				this.setState({backgroundUrl:evt.target.value})
				return;
			 case 'Amount':
				this.setState({amount:evt.target.value})
				return;
			 default:
				return;
		}
	
	}
	onSubmit(evt) {
		evt.preventDefault();
		if (this.hasEmpty()) return;
		var formData  = new FormData();
		formData.append('token',localStorage.admintoken);
		formData.append('categoryid', this.state.categoryId);
		formData.append('title',this.state.title);
		formData.append('subtitle',this.state.subtitle);
		formData.append('coverUrl',this.state.coverUrl);
		formData.append('url',this.state.url);
		formData.append('backgroundUrl',this.state.backgroundUrl);
		formData.append('amount',this.state.amount);
		const url = `${this.imageRoot}`;
		fetch(url, {
			method: 'POST',
			body:formData
		}).then(response =>{
			if (response.status >= 200 && response.status < 300) {
				return response 
			} else {
				var error = new Error(response.statusText)
				error.response = response
				throw error
			}
			}).then(function (textValue) {
				const response = textValue;
				console.log(response);
				if(response.ok){
					this.props.dispatch(VerifyMessage('Ad is insert'));
					this.setState({
						categoryId:'',
						title:'',
						subtitle:'',
						coverUrl:'',
						url:'',
						backgroundUrl:'',
						amount:''
					});
				}
			}.bind(this)).catch(function(error) {
				console.log(error);
			});
  	}
	render() {
		const dispatch = this.props.dispatch;
		const height=110;
		const {vertisers} = this.props.Admin;
		if(_.toArray(vertisers)){
			var options=[]
			var datas = _.map(vertisers, function(n, key) {
				options.push({id:n.id,name:n.name})
			})
		}
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch} history={this.props.history}/>
					<form className="form" id="editform" onSubmit={this.onSubmit} >
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>Add Ad</h1>
							</div>
						</div>
						<ErrorMessage />
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControlsSelect">
									<ControlLabel>{'CategoryId'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<Select 
									name="form-field-name"
									options={options}
									onChange={this.onselectChange}
									value={this.state.categoryId}
									valueKey="id" 
									labelKey="name"
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'Title'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="title" onChange={this.onChange} value={this.state.title} placeholder={"title"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'Subtitle'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="subtitle" onChange={this.onChange} value={this.state.subtitle} placeholder={"subtitle"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'CoverUrl'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="coverUrl" onChange={this.onChange} value={this.state.coverUrl} placeholder={"coverUrl"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'Ad url'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="adUrl" onChange={this.onChange} value={this.state.url} placeholder={"url"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'bg Url'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="backgroundUrl" onChange={this.onChange} value={this.state.backgroundUrl} placeholder={"backgroundUrl"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
								<FormGroup controlId="formControls">
									<ControlLabel >{'Amount'}</ControlLabel>
								</FormGroup>
							</div>
							<div className="col-sm-6">
								<FormControl type="amount" onChange={this.onChange} value={this.state.amount} placeholder={"Amount"} />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-7">
							</div>
							<div className="col-sm-3" id="profile">
								<input type="submit"  value={'Submit,'}  className="profile_tab"/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-7">
							</div>
							<div className="col-sm-3" id="profile">
								<input type="button"  value={'Cancel'}  onClick={this.onClick} className="profile_tab"/>
							</div>
						</div>
					</form>
				</div>
			);
	}
}

function select(state) {
  return {
    Datas: state.Datas,
    Admin: state.Admin
  };
}
module.exports=connect(select)(AddAdPage);
