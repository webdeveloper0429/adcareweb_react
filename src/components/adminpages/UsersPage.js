import React, { Component} from 'react';
import { connect } from 'react-redux';
import {admin_login,VerifyMessage,catch_users,update_user} from '../../Actions/Admin';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col,TextField} from 'react-bootstrap';
import ErrorMessage from '../ErrorMessage';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
class MySearchPanel extends React.Component {
  	render() {
    	return (
		  	<div>
				<div className='input-group'>
			  		<span className='input-group-btn'>
						{ this.props.clearBtn }
			  		</span>
				</div>
				{ this.props.searchField }
		  </div>
		);
	}
}
class UsersPage extends Component {
	constructor(props) {
		super(props);
		this.afterSaveCell = this.afterSaveCell.bind(this);
		this.handleRowSelect = this.handleRowSelect.bind(this);
	}
	componentDidMount() {
		
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{

			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_users(attr));	
		}
	}
	onSubmit(evt) {
		evt.preventDefault();
		if (this.hasEmpty()) return;
		let attr={
			name:this.state.name,
			password:this.state.password	
		}
		this.props.dispatch(admin_login(attr));
  	}
	renderCustomClearSearch = (onClick) => {
		return (
		  <button
			className='btn btn-success'
			onClick={ onClick }>
			Empty
		  </button>
		);
	}
	afterSaveCell(row, cellName, cellValue) {
		let attr={
			id:row.id,
			email:row.email,
			age:row.age,
			gender:row.gender,
			nickname:row.nickname,
			token:localStorage.admintoken
		}
		this.props.dispatch(update_user(attr));
	}
	handleRowSelect(row, isSelected, e) {
		this.props.history.push('./info/'+row.id);
	}

	render() {
		const {profiles} = this.props.Admin;
		const dispatch=this.props.dispatch;
		let rows=[] 
		if(!_.isEmpty(profiles)){
			var datas = _.map(profiles, function(n, key) {
				let data={}
				_.forEach(n, function(value, key) {
					data[key]=value;
				});
				rows.push(data)
			}.bind(this))
		}
		const options = {
            noDataText:"no datas",
			defaultSortName: 'id',
			defaultSortOrder: 'asc',
      		clearSearch: true,
      		clearSearchBtn: this.renderCustomClearSearch,
            sizePerPageList: [ 10, 50, 100, 500 ],
            sizePerPage: 10, 
			paginationShowsTotal: (start, to, total) =>(<span style={{color:'black'}}>從第 { start } 筆到第 { to } 筆資料, 共有 { total } 筆資料</span>),
      		//searchPanel: (props) => (<MySearchPanel { ...props }/>)
    	};
		const cellEditProp = {
			mode: 'click',
			//iblurToSave: true
			afterSaveCell: this.afterSaveCell
		};
		 const selectRow = {
			mode: 'checkbox',  // multi select
			clickToSelect: true,
			 bgColor: 'pink',
			onSelect: this.handleRowSelect
		  };
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch} history={this.props.history}/>
					<form className="form" id="editform" onSubmit={this.onSubmit} >
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>Users</h1>
							</div>
						</div>
                    	<div className="container-fluid">
                            <div className='margin-bottom-10'>
                                <BootstrapTable ref="retailstores" 
												data={rows} 
												striped={true} 
												hover={true} 
												height={500} 
												keyField="id" 
												options={options} 
                                                selectRow={selectRow}
												cellEdit={ cellEditProp }
												insertRow={false}
												search pagination  multiColumnSearch>
                                    <TableHeaderColumn dataField="id" width ="40px" dataSort={true} searchable={true} >{'id'}</TableHeaderColumn>
                                    <TableHeaderColumn dataField="email" width ="200px" dataSort={true} searchable={true}>{'email'}</TableHeaderColumn>
									<TableHeaderColumn dataField="verified" width ="100px" dataSort={true} editable={ { type: 'checkbox', options: { values: 'Y:N' } } }searchable={true}>{'verified'}</TableHeaderColumn>
									<TableHeaderColumn dataField="nickname" width ="100px" dataSort={true} searchable={true}>{'nickname'}</TableHeaderColumn>
									<TableHeaderColumn dataField="age" width ="60px" dataSort={true} searchable={true}>{'age'}</TableHeaderColumn>
									<TableHeaderColumn dataField="gender" width ="100px" dataSort={true} searchable={true} editable={ { type: 'select', options:{values: ['Female','Male']}} }>{'gender'}</TableHeaderColumn>
									<TableHeaderColumn dataField="resetcode" width ="100px" dataSort={true} searchable={true} editable={false}>{'resetcode'}</TableHeaderColumn>
									<TableHeaderColumn dataField="esetemailts" width ="100px" dataSort={true} searchable={true} editable={false}>{'esetemailts'}</TableHeaderColumn>
									<TableHeaderColumn dataField="countryId" width ="100px" dataSort={true} searchable={true} editable={false}>{'countryId'}</TableHeaderColumn>
                                </BootstrapTable>
                            </div>
                   		 </div>
					</form>
				</div>
			);
	}
}

function select(state) {
  return {
    Admin: state.Admin
  };
}
module.exports=connect(select)(UsersPage);
