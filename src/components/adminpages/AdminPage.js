import React, { Component} from 'react';
import FileUpload from 'react-fileupload';
import Backstretch from '../Backstretch';
import admin from '../../image/Black.jpg';
import { connect } from 'react-redux';
import style from '../../Admin.css';
import {admin_add_ad,VerifyJudgment} from '../../Actions/Datas';
import ImagesUploader from 'react-images-uploader';
import ErrorMessage from '../ErrorMessage';
import AdminHeader from '../../AdminHeader';

class AdminPage extends Component {
 constructor(props) {
    super(props);
    this.state = {
      file: '',
	  imageType:'',
      imagePreviewUrl: '',
	  filename:''
    };
    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
		}
	}

  _handleSubmit(e) {
    e.preventDefault();
	var buf = new Buffer(this.state.imagePreviewUrl.replace(/^data:image\/\w+;base64,/, ""),'base64');
	var formData  = new FormData();
	formData.append('uploadfile', this.state.file);
    formData.append('filename',this.state.filename);
    formData.append('imageType',this.state.imageType);
  	var Headers={
		'Accept': 'application/json',
		'Content-Type': 'application/json'
	}
	fetch('http://127.0.0.1:8000/adcare/webadmin/add_photo', {
  		method: 'POST',
  		body:formData
	}).then(response =>{
        if (response.status >= 200 && response.status < 300) {
            return response 
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
        }).then(function (textValue) {
			const response = textValue;
            console.log(response);
			if(response.ok){
				console.log(this.props)
				this.props.dispatch(VerifyJudgment('Image is insert'));
			  	this.setState({
					file: '',
					imageType:'',
					filename: '',
					imagePreviewUrl: ''
			  	});
			}
		}.bind(this)).catch(function(error) {
            console.log(error);
        });
/*
	var img=(this.state.imagePreviewUrl).split(',')
	let attr={
		file:this.state.file,
		img:img[1]
	}
*/
	//this.props.dispatch(admin_add_ad(attr));
    // TODO: do something with -> this.state.file
  }
 _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
		imageType:file.type,
        filename: file.name,
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(file)
  }
	componentWillReceiveProps (nextProps) {
		console.log(nextProps)
	}
 render() {
    let {imagePreviewUrl} = this.state;
	const dispatch= this.props.dispatch;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className="ad_imag" src={imagePreviewUrl} />);
    }

    return (
      	<form className="form">
			<div className="col-sm-12 col-lg-12">
				<AdminHeader dispatch={dispatch}/>
				<div className="container">
					<div className="row" style={{height:100}}>
					</div>
					<ErrorMessage />
					<div>
						<form onSubmit={this._handleSubmit}>
						  <input type="file" onChange={this._handleImageChange} />
						  <button type="submit" onClick={this._handleSubmit}>Upload Image</button>
						</form>
					</div>
					<div className="row">
						<div className="col-sm-12 col-lg-12">
							{$imagePreview}
						</div>
					</div>
				</div>
			</div>
		</form>
    )
  }
}
function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(AdminPage);
