import React, { Component} from 'react';
import { connect } from 'react-redux';
import {add_charity,VerifyMessage} from '../../Actions/Admin';
import ErrorMessage from '../ErrorMessage';
import AdminHeader from '../../AdminHeader';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col,TextField} from 'react-bootstrap';

class AddCharity extends Component {
 	constructor(props) {
    	super(props);
		const {domainName} = props.Datas;
		this.imageRoot=`${domainName}/adcare/webadmin/add_charity`;
    	this.state = {
      		name: '',
			description:'',
      		url: ''
    		};
		this.onSubmit = this.onSubmit.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onClick = this.onClick.bind(this);
	}
	onClick(){
		this.setState({
			name:'',
			description:'',
			url:''
		});
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }
	}
	componentWillReceiveProps (nextProps) {
		console.log(nextProps)
	}
  	hasEmpty() {
		if (!this.state.name) {
			this.props.dispatch(VerifyMessage('name is empty'));
	  		return true;
		}
		if (!this.state.description) {
			this.props.dispatch(VerifyMessage('description is empty'));
	  		return true;
		}
		if (!this.state.url) {
			this.props.dispatch(VerifyMessage('url is empty'));
	  		return true;
		}
	}
	onChange(evt){
		 switch (evt.target.placeholder) {
			 case 'name':
				this.setState({name:evt.target.value})
				return;
			 case 'description':
				this.setState({description:evt.target.value})
				return;
			 case 'url':
				this.setState({url:evt.target.value})
				return;
		}
	}
	onSubmit(evt) {
		evt.preventDefault();
		if (this.hasEmpty()) return;
		var formData  = new FormData();
		formData.append('name', this.state.name);
		formData.append('description',this.state.description);
		formData.append('url',this.state.url);
		const url = `${this.imageRoot}`;
		fetch(url, {
			method: 'POST',
			body:formData
		}).then(response =>{
			if (response.status >= 200 && response.status < 300) {
				return response 
			} else {
				var error = new Error(response.statusText)
				error.response = response
				throw error
			}
			}).then(function (textValue) {
				const response = textValue;
				console.log(response);
				if(response.ok){
					this.props.dispatch(VerifyMessage('Charity is insert'));
					this.setState({
						name:'',
						description:'',
						url:''
					});
				}
			}.bind(this)).catch(function(error) {
				console.log(error);
			});
  	}
 render() {
    return (
			<div className="col-sm-12 col-lg-12">
				<AdminHeader history={this.props.history} disaptch={this.props.dispatch}/>
				<form className="form" id="editform" onSubmit={this.onSubmit} >
					<div className="row" style={{height:100}}>
						<div className="col-sm-3">
							<h1>{'Add Charity'}</h1>
						</div>
					</div>
					<ErrorMessage />
					<div className="row">
						<div className="col-sm-1">
							<FormGroup controlId="formControls">
								<ControlLabel >{'name'}</ControlLabel>
							</FormGroup>
						</div>
						<div className="col-sm-6">
							<FormControl type="text"  onChange={this.onChange} value={this.state.name} placeholder={"name"} />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-1">
							<FormGroup controlId="formControls">
								<ControlLabel >{'description'}</ControlLabel>
							</FormGroup>
						</div>
						<div className="col-sm-6">
							<FormControl type="title" onChange={this.onChange} value={this.state.description} placeholder={"description"} />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-1">
							<FormGroup controlId="formControls">
								<ControlLabel >{'url'}</ControlLabel>
							</FormGroup>
						</div>
						<div className="col-sm-6">
							<FormControl type="title" onChange={this.onChange} value={this.state.url} placeholder={"url"} />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-7">
						</div>
						<div className="col-sm-3" id="profile">
							<input type="submit"  value={'Submit,'}  className="profile_tab"/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-7">
						</div>
						<div className="col-sm-3" id="profile">
							<input type="button"  value={'Cancel'}  onClick={this.onClick} className="profile_tab"/>
						</div>
					</div>
				</form>
			</div>
    )
  }
}
function select(state) {
  return {
    Datas: state.Datas,
    Admin: state.Admin
  };
}
module.exports=connect(select)(AddCharity);
