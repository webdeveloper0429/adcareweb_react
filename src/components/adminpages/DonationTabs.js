import React, { Component} from 'react';
import { connect } from 'react-redux';
import {del_donation,catch_donation,catch_userad} from '../../Actions/Admin';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

class  DonationTabs extends Component {
	constructor(props) {
		super(props);
    	this.afterdeleteRow = this.afterdeleteRow.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{
		
		}
	}
	renderCustomClearSearch = (onClick) => {
		return (
		  <button
			className='btn btn-success'
			onClick={ onClick }>
			Empty
		  </button>
		);
	}
	afterdeleteRow(row){
		let attr={
			id:row[0],
			token:localStorage.admintoken
		}
		console.log(row[0])
		this.props.dispatch(del_donation(attr));

	
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Admin.add){
			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_donation(attr));
			this.props.dispatch(catch_userad(attr));
		}
	}

	render() {
		const {donation} = this.props.Admin;
		let adrows=[]
		if(!_.isEmpty(donation)){
 			var datas = _.map(donation, function(n, key) {
                let data={}
				console.log(n)
                _.forEach(n.walletRecord, function(value, key) {
					if(key==='datetime' ||key==='profileId'){
                    	data[key]=value;
					}
                });
                _.forEach(n.earningRecord, function(value, key) {
                    	data[key]=value;
                });
                adrows.push(data)
            }.bind(this))
		}
		const options = {
            noDataText:"no datas",
			defaultSortName: 'id',
			defaultSortOrder: 'asc',
      		clearSearch: true,
      		clearSearchBtn: this.renderCustomClearSearch,
            sizePerPageList: [ 10, 50, 100, 500 ],
            sizePerPage: 10, 
			afterDeleteRow: this.afterdeleteRow,
			paginationShowsTotal: (start, to, total) =>(<span style={{color:'black'}}>從第 { start } 筆到第 { to } 筆資料, 共有 { total } 筆資料</span>),
      		//searchPanel: (props) => (<MySearchPanel { ...props }/>)
    	};
		 const selectRow = {
			mode: 'checkbox',  // multi select
			clickToSelect: true,
			bgColor: 'pink',
			onSelect: this.handleRowSelect
		  };
		 function priceFormatter(cell, row) {
			return `<i class='glyphicon glyphicon-usd'></i> ${cell}`;
		 }
		return (
				<div className="form" >
				<h1>{'Donations'}</h1>
				<div className="container-fluid">
					<div className='margin-bottom-10'>
						{this.props.open?(
							<BootstrapTable ref="adstores" 
											data={adrows} 
											striped={true} 
											hover={true} 
											height={500} 
											keyField="id" 
											options={options} 
											selectRow={selectRow}
											insertRow={false}
											search pagination  multiColumnSearch>
								<TableHeaderColumn dataField="id" width ="50px" dataSort={true} searchable={true} >{'id'}</TableHeaderColumn>
								<TableHeaderColumn dataField="profileId" width ="50px" dataSort={true} searchable={true} >{'profileId'}</TableHeaderColumn>
								<TableHeaderColumn dataField="charityId" width ="50px" dataSort={true} searchable={false}>{'charityId'}</TableHeaderColumn>
								<TableHeaderColumn dataField="donated" dataFormat={priceFormatter} width ="50px" dataSort={true} searchable={true}>{'donated'}</TableHeaderColumn>
								<TableHeaderColumn dataField="earned" dataFormat={priceFormatter} width ="50px" dataSort={true} searchable={false}>{'earned'}</TableHeaderColumn>
								<TableHeaderColumn dataField="myself" dataFormat={priceFormatter} width ="50px" dataSort={true} searchable={false}>{'myself'}</TableHeaderColumn>
								<TableHeaderColumn dataField="datetime" width ="250px" dataSort={true} searchable={false}>{'datetime'}</TableHeaderColumn>
							</BootstrapTable>
						):(
							<BootstrapTable ref="adstores" 
											data={adrows} 
											striped={true} 
											hover={true} 
											height={500} 
											keyField="id" 
											options={options} 
											selectRow={selectRow}
											insertRow={false}
											deleteRow
											search pagination  multiColumnSearch>
								<TableHeaderColumn dataField="id" width ="50px" dataSort={true} searchable={true} >{'id'}</TableHeaderColumn>
								<TableHeaderColumn dataField="profileId" width ="50px" dataSort={true} searchable={true} >{'profileId'}</TableHeaderColumn>
								<TableHeaderColumn dataField="charityId" width ="50px" dataSort={true} searchable={false}>{'charityId'}</TableHeaderColumn>
								<TableHeaderColumn dataField="donated" dataFormat={priceFormatter} width ="50px" dataSort={true} searchable={true}>{'donated'}</TableHeaderColumn>
								<TableHeaderColumn dataField="datetime" width ="250px" dataSort={true} searchable={false}>{'datetime'}</TableHeaderColumn>
							</BootstrapTable>
						)}
					</div>
				 </div>
				 </div>
			);
	}
}
function select(state) {
  return {
    Admin: state.Admin,
    Datas: state.Datas
  };
}
module.exports=connect(select)(DonationTabs);
