import React, { Component} from 'react';
import { connect } from 'react-redux';
import {catch_advertisers,add_vertisers,update_advertisers,del_advertisers} from '../../Actions/Admin';
import AdminHeader from '../../AdminHeader';
import _ from 'lodash';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
class Advertisers extends Component {
	constructor(props) {
		super(props);
		this.afterSaveCell = this.afterSaveCell.bind(this);
		this.afterInsertRow = this.afterInsertRow.bind(this);
		this.afterdeleteRow = this.afterdeleteRow.bind(this);
		this.handleRowSelect = this.handleRowSelect.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.admintoken){
			this.props.history.push('./login')
        }else{
			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_advertisers(attr));
		}
	}
	renderCustomClearSearch = (onClick) => {
		return (
		  <button
			className='btn btn-success'
			onClick={ onClick }>
			Empty
		  </button>
		);
	}
	afterSaveCell(row, cellName, cellValue) {
	  // do your stuff...
		console.log(row)
		console.log(cellName)
		console.log(cellValue)
		let attr={
			id:row.id,
			name:cellValue,
			token:localStorage.admintoken
		}
		this.props.dispatch(update_advertisers(attr));
	}
	handleRowSelect(row, isSelected, e) {
		console.log(row)
		//this.props.history.push('./info/'+row.id);
	}
	afterInsertRow(row){
		console.log(row)
		let attr={
			id:row.id,
			name:row.name,
			token:localStorage.admintoken
		}
		this.props.dispatch(add_vertisers(attr));

	}
	afterdeleteRow(row){
		let attr={
			id:row[0],
			token:localStorage.admintoken
		}
		this.props.dispatch(del_advertisers(attr));

	
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Admin.add){
			let attr={
				token:localStorage.admintoken
			}
			this.props.dispatch(catch_advertisers(attr));
		}
	}

	render() {
		const {vertisers} = this.props.Admin;
		const dispatch=this.props.dispatch;
		let rows=[] 
		if(!_.isEmpty(vertisers)){
			var datas = _.map(vertisers, function(n, key) {
				let data={}
				_.forEach(n, function(value, key) {
					data[key]=value;
				});
				rows.push(data)
			}.bind(this))
		}
		const options = {
            noDataText:"no datas",
			defaultSortName: 'id',
			defaultSortOrder: 'asc',
			afterInsertRow: this.afterInsertRow,
			afterDeleteRow: this.afterdeleteRow,
      		clearSearch: true,
      		clearSearchBtn: this.renderCustomClearSearch,
            sizePerPageList: [ 10, 50, 100, 500 ],
            sizePerPage: 10, 
			paginationShowsTotal: (start, to, total) =>(<span style={{color:'black'}}>從第 { start } 筆到第 { to } 筆資料, 共有 { total } 筆資料</span>),
      		//searchPanel: (props) => (<MySearchPanel { ...props }/>)
    	};
		const cellEditProp = {
			mode: 'click',
			//iblurToSave: true
			afterSaveCell: this.afterSaveCell
		};
		 const selectRow = {
			mode: 'checkbox',  // multi select
			clickToSelect: true,
			bgColor: 'pink',
			onSelect: this.handleRowSelect
		  };
		return (
				<div className="col-sm-12 col-lg-12">
					<AdminHeader dispatch={dispatch} history={this.props.history}/>

					<form className="form" id="editform" onSubmit={this.onSubmit} >
						<div className="row" style={{height:100}}>
							<div className="col-sm-3">
								<h1>Vertiser List</h1>
							</div>
						</div>
                    	<div className="container-fluid">
                            <div className='margin-bottom-10'>
                                <BootstrapTable ref="retailstores" 
												data={rows} 
												striped={true} 
												hover={true} 
												height={500} 
												keyField="id" 
												options={options} 
                                                selectRow={selectRow}
												cellEdit={ cellEditProp }
												insertRow={true}
												deleteRow
												search pagination  multiColumnSearch>
                                    <TableHeaderColumn dataField="id" autoValue={true} width ="40px" dataSort={true} visible={false}searchable={false} editable={false}>{'id'}</TableHeaderColumn>
									<TableHeaderColumn dataField="name" width ="300px" dataSort={true} searchable={true} editable={true}>{'name'}</TableHeaderColumn>
                                </BootstrapTable>
                            </div>
                   		 </div>
					</form>
				</div>
			);
	}
}

function select(state) {
  return {
    Admin: state.Admin
  };
}
module.exports=connect(select)(Advertisers);
