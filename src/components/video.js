import React, { Component } from 'react';
import ad from '../image/Black.jpg';
import Scroll  from 'react-scroll';

var Link       = Scroll.Link;
var Element    = Scroll.Element;
var Events     = Scroll.Events;
var scroll     = Scroll.animateScroll;
var scrollSpy  = Scroll.scrollSpy;

class Video extends Component {
	constructor(props) {
		super(props);
		this.show = this.show.bind(this);
		this.onClick = this.onClick.bind(this);
	}
	show() {
		if(this.props.id==='true'){
			return(
				<div className="col-sm-2">
				</div>
			);
		}

	}
	componentDidMount() {
		Events.scrollEvent.register('begin', function(to, element) {
		  console.log("begin", arguments);
		});

		Events.scrollEvent.register('end', function(to, element) {
		  console.log("end", arguments);
		});

		scrollSpy.update();
	}
  onClick(evt) {
    //evt.preventDefault();
    this.props.onClick(this.props.id);
  }
  render() {
		var cover=(
			<img src={ad} className='cover'/>
		);

    return(
		<ul>
 			<Element name={this.props.name} className="element">
			{this.show()}
			<li onClick={this.onClick} id={this.props.id}>
				<img src={this.props.src} className={this.props.name}/>
				{this.props.close?(
				<img src={this.props.playedsrc} className={this.props.play}/>
				):("")}
				{this.props.cover?(	
					cover):(
					""
				)}
			</li>
        	</Element>
		</ul>
	);
	}
}
Video.propTypes={
  src: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.object,
  ]).isRequired,
  playedsrc: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.object,
  ]).isRequired,
  id:React.PropTypes.string,
  name:React.PropTypes.string,
  play:React.PropTypes.string,
  cover:React.PropTypes.bool,
  close:React.PropTypes.bool,
  onClick: React.PropTypes.func,
}
export default Video;
