import React, { Component } from 'react';

class Iframe extends Component {
  render() {
    return(
		<div className="col-sm-4">
			<h2>{this.props.btnText}</h2>
			<div className="embed-responsive embed-responsive-16by9">
				<iframe className="embed-responsive-item" src={'http://www.youtube.com/embed/'+this.props.code} frameBorder="0" allowFullScreen></iframe>
			</div>
		</div>
	);
	}
}
Iframe.propTypes={
  code: React.PropTypes.string.isRequired,
  btnText: React.PropTypes.string
}
export default Iframe;
