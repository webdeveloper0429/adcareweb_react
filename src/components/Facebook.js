import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import {facebook_login} from '../Actions/AppActions';

export default class Facebook extends React.Component {
 	constructor(props) {
        super(props);
		this.state ={access_token:""}
		this.componentClicked = this.componentClicked.bind(this);
        this.responseFacebook = this.responseFacebook.bind(this);
	}
 	componentClicked(){
        let attr={
            token:this.state.access_token
        }
        this.props.dispatch(facebook_login(attr));
    }
    responseFacebook(response){
        let attr={
            token:response.accessToken
        }
        console.log(response)
        this.props.dispatch(facebook_login(attr));
        this.setState({access_token: response.accessToken});
    }
  render() {
    return(
		<FacebookLogin
			appId="244337159360428"
			fields="name,email,picture"
			//onClick={this.componentClicked}
			callback={this.responseFacebook}
			cssClass="my-facebook-button"
			size="small"
			textButton=""
		/>
	);
	}
}
