import React, { Component} from 'react';
import { changeForm } from '../Actions/AppActions';
import LoadingButton from './LoadingButton';
import Input from '../components/Input';
import ErrorMessage from './ErrorMessage';
const assign = Object.assign || require('object.assign');

class SignupForm extends Component {
	constructor(props) {
		super(props);
		this.state={loading:false,type:'email',submit:false,password:''}
		this.onchange = this.onchange.bind(this);
		this.inputButton = this.inputButton.bind(this);
		this.Timeout = this.Timeout.bind(this);
		this.componentDidMount = this.componentDidMount.bind(this);
	}
	componentDidMount() {
		this.setState({loading:true,type:'email',submit:false})
		this.Timeout();
	}
	Timeout(){
 		setTimeout(() => {
			this.setState({loading:false})
      	}, 1000);
	}
	inputButton() {
		if(this.state.type==='email'){
			return(
				<div className="row">
					<Input classname={this.state.type} type={'text'} value={this.props.data.email} placeholder={'Enter Your Email'} onChange={this._changeEmail.bind(this)}/>
				</div>
			)
		}else if(this.state.type==='password'){
			return(
				<div className="row">
					<Input classname={this.state.type} type={'password'} value={this.state.password} placeholder={'Enter Your Password'} onChange={this._changePassword.bind(this)}/>
				</div>
			)
		}else{
			return(
				<div className="row">
					<Input classname={this.state.type} type={'text'} value={this.props.data.nickname} placeholder={'How To Greet You?'} onChange={this._changeNickname.bind(this)}/>
				</div>
			)
		
		}

	}
  render() {
	  const button=(
		<div className="row">
			<div className="col-sm-10">
			</div>
			<div className="col-sm-2">
			  {this.props.loading||this.state.loading ? (
				<LoadingButton />
			  ) : (
				<button icon="cross" className="btn" type='submit' onClick={this.onchange}>{this.props.btnText}>&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
			  )}
			</div>
        </div>
			)
    return(
      <form className="form" onSubmit={this._onSubmit.bind(this)}>
	    <ErrorMessage />
		{this.inputButton()}
		&nbsp;&nbsp;&nbsp;
		{button}
      </form>
    );
  }
  onchange(evt){
	this.setState({loading:true})
	this.Timeout();
	if(this.props.data.email && !this.state.password){
		this.setState({type:'password',submit:false,loading:false})
	}else if(this.state.password && this.props.data.email){
		this.setState({type:'nickname',submit:true,loading:false})
	}
  
  }
  _changeNickname(evt) {
    var newState = this._mergeWithCurrentState({
      nickname: evt.target.value
    });

    this._emitChange(newState);
  }
  _changeEmail(evt) {
    var newState = this._mergeWithCurrentState({
      email: evt.target.value
    });

    this._emitChange(newState);
  }
  _changePassword(evt) {
	this.setState({password: evt.target.value})
  }
  _changeCode(evt) {
    var newState = this._mergeWithCurrentState({
      code: evt.target.value
    });

    this._emitChange(newState);
  }
  _mergeWithCurrentState(change) {
    return assign(this.props.data, change);
  }
  _emitChange(newState) {
    this.props.dispatch(changeForm(newState));
  }
  _onSubmit(evt) {
    evt.preventDefault();
	if(this.props.data.email && this.state.password && this.props.data.nickname){
		this.setState({type:'email'})
    	this.props.onSubmit(this.props.data.email,this.state.password,this.props.data.nickname);
	}
	else{
		return false;
	}
  }
}

SignupForm.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
  btnText: React.PropTypes.string.isRequired,
  loading:React.PropTypes.bool,
  data: React.PropTypes.object.isRequired
}

export default SignupForm;
