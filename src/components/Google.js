import React from 'react';
import { GoogleLogin } from 'react-google-login-component';
import {google_login} from '../Actions/AppActions';
 
class Google extends React.Component{
 
  constructor (props, context) {
    super(props, context);
  }
 
  responseGoogle (googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    console.log({accessToken: id_token});
        let attr={
            token:id_token
        }
	this.props.dispatch(google_login(attr))
    //anything else you want to do(save to localStorage)... 
  }
 
  render () {
    return (
        <GoogleLogin socialId="564225550436-efq7k6o1pkcs8srtvo51ode7nv6vpm9b.apps.googleusercontent.com"
                     class="google-login"
                     scope="profile email"
                     responseHandler={this.responseGoogle.bind(this)}
                     buttonText=""/>
    );
  }
 
}
 
export default Google;
