import styled from 'styled-components';

const H2 = styled.h2`
  font-size: 30px;
  color: #FFFFFF;
`;

export default H2;
