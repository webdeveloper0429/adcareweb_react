import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

let ErrorMessage = (props) => {
	console.log(props)
	return (
		props.errorMessage ?
			<div className="error-wrapper">
				<p className="error">{props.errorMessage}</p>
			</div>
			:<div></div>
	);
};

ErrorMessage.propTypes = {
	errorMessage: PropTypes.string
};

function mapStateToProps(state) {
  return {
    errorMessage: state.UserData.errorMessage
  };
}

ErrorMessage = connect(mapStateToProps)(ErrorMessage);

export default ErrorMessage;
