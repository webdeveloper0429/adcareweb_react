import React, { Component } from 'react';
import _ from 'lodash';

class LoveForm extends Component {
	constructor(props) {
		super(props);
		this.state={color:'player'}
	}
  	_onClick() {
		if(document.getElementById(this.props.id).className=='black'){
			this.setState({color:'player'})
		}else{
			this.setState({color:'black'})
		}
    	this.props.onClick(this.props.id);
  	}
  	render() {
		if(this.props.type){
			this.state.color='player';
		}
		return(
			<li className={this.props.classname} style={{height:50}}>
				<input  type="button"  
						name={this.props.name} 
						id={this.props.id} 
						value={this.props.value} 
						onClick={this._onClick.bind(this)} 
						className={this.state.color}/>
			</li>
		);
	}
}
LoveForm.propTypes={
  	id:React.PropTypes.string.isRequired,
  	name:React.PropTypes.string,
  	type:React.PropTypes.string,
  	value:React.PropTypes.string,
  	classname:React.PropTypes.string,
  	onClick: React.PropTypes.func.isRequired,
}
export default LoveForm;
