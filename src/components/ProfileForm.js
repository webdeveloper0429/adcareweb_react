import React, { Component } from 'react';
import H4 from '../components/H4';

class ProfileForm extends Component {
	onClick(){
		this.props.history.push('./edit')
	}
	onChange(){
		this.props.history.push('./tribe')
	}
  render() {
		return(
      		<form className="form" >
				<div className="row">
					<div className="row" style={{height:100}}>
						<div className="col-sm-9">
						</div>
						<div className="col-sm-3">
							<h1>{this.props.name},</h1>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
						</div>
						<div className="col-sm-3" id="profile">
							<input  type='button' onClick={this.onClick.bind(this)} value={'Edit'}  className="profile_tab"/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-2">
							<h1 className='pro'>{'Gender'}</h1>
						</div>
						<div className="col-sm-3">
							<h1 className='pro'>{'Age'}</h1>
						</div>
						<div className="col-sm-6">
      						<img className="pro_img" src={this.props.src} />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-2">
							<h1 className='profile'>{this.props.gender}</h1>
						</div>
						<div className="col-sm-3">
							<h1 className='profile'>{this.props.age}</h1>
						</div>
						<div className="col-sm-6">
						</div>
					</div>
					<div className="row">
						<div className="col-sm-2">
						</div>
						<div className="col-sm-3">
							<h1 className='pro'>{'years old'}</h1>
						</div>
						<div className="col-sm-6">
						</div>
					</div>
					<div className="row">
						<div className="col-sm-3">
							<h1 className='pro'>{'Email'}</h1>
						</div>
						<div className="col-sm-3">
						</div>
					</div>
					<div className="row" >
						<div className="col-sm-3">
							<h1 className='profile'>{this.props.email}</h1>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-9">
						</div>
						<div className="col-sm-3" id="profile">
							<input type="button" onClick={this.onChange.bind(this)} value={'My Tribe,'} className="tribe_tab"/>
						</div>
					</div>
				</div>
			</form>
		);
	}
}
 ProfileForm.propTypes={
  	id:React.PropTypes.string,
  	email:React.PropTypes.string,
  	name:React.PropTypes.string,
  	gender:React.PropTypes.string,
  	age:React.PropTypes.number,
	src: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.object,
	]).isRequired,
  	onSubmit: React.PropTypes.func
}
export default ProfileForm;
