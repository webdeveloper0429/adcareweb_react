import React, { Component} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import Form from '../Form';
import { login} from '../../Actions/Datas';
import {iframe} from 'react-bootstrap';
import  Iframe from '../Iframe';
import  Video from '../video';
import Backstretch from '../../components/Backstretch';
import Dg from '../../components/Dg';
import ad from '../../image/black1.jpg';
import image from '../../image/Ad.jpg';
import play from '../../image/play.png';
import played from '../../image/played.svg';
import H2 from '../../components/H2';
import styles from '../../Dg.css';

class  CheckPage extends Component {
	constructor(props) {
		super(props);
		this.state = {videoid:'',close:true, eorned:'',donated:'',
					  ET:'',DT:'',btnText:'Check it Out',play: false,
					  open:false,Title2:'',Title:'DG Family',value:'Doke & Gabbana',
					  et:'0.30',dt:'0.40'}
		this.onClick = this.onClick.bind(this);
	}
	onClick() {
		this.setState({Title:'Thank you for watching',Title2:'our 10 advertisements,',
					   close:false,value:'',play:true,eorned:'Total Eorned',
					   donated:'Total Donated to OCEANA',ET:'$3.0',DT:'$4.0'})
	}
	componentDidMount() {
		if(!localStorage.token){
			this.props.history.push('/')
		}else{
			this.setState({videoid:this.props.match.params.id})
		}
	}
	componentWillReceiveProps(nextProps) {

	}

	render() {
		const {domainName} = this.props.Datas
		var adId=this.state.videoid;
		var img=`${domainName}/adcare/webadmin/image/${adId}`;
		var videos={};
		videos[adId]=img;
		//var play = {'0':image};
		var closed=this.state.close;
	  	var datas = _.map(videos, function(n, key) {
			return <Video cover={true} 
						  key={key.toString()} 
						  close={closed} 
						  id={key.toString()} 
						  src={n} 
						  playedsrc={played} 
						  play={'played_img'} 
						  name={'ad_img_one'}
					/>
		});
		return(
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Font"}}>
				<Backstretch src={ad} history={this.props.history} className="homeimage"/>
				<div className="container" onClick={this.onClick}>
					<div className="row" style={{height:25}}>
						<Dg 
							Title={this.state.Title} 
							Title2={this.state.Title2} 
							value={this.state.value} 
							style={'dg_title_one'} 
							btnText={this.state.btnText}
						   	open={this.state.play} 
							eorned={this.state.eorned}
							donated={this.state.donated}
							ET={this.state.ET}
							DT={this.state.DT}
							/>
					</div>
					<div className="row" >
						<div className="col-sm-4">
						</div>
						<div className="col-sm-4">
							{datas}
						</div>
					</div>
				</div>
			</div>

		)
	
	}
}
function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(CheckPage);
