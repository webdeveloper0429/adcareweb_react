import React, { Component} from 'react';
import { connect } from 'react-redux';
import Form from '../Form';
import { login} from '../../Actions/AppActions';
import SignUpPage from '../../components/pages/SignUpPage';
import ErrorMessage from '../ErrorMessage';
import { verifycode} from '../../Actions/AppActions';
import LoadingButton from '../LoadingButton';
import Input from '../Input';
import _ from 'lodash';

class LoginPage extends Component {
	constructor(props) {
		super(props);
		this.state={loading:false,open:false,code:'',email:'',userid:'',error:''}
		this._login = this._login.bind(this);
		this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
		this.Timeout = this.Timeout.bind(this);
	}
	Timeout(){
 		setTimeout(() => {
			this.setState({loading:false})
      	}, 1000);
	}
	_login(email, password) {
		this.setState({loading:true})
		this.props.dispatch(login(email,password));
		this.Timeout();
	}
	componentWillReceiveProps (nextProps) {
		if(nextProps.UserData.errorMessages==='needVerify'){
			this.setState({loading:false,open:false,error:nextProps.UserData.errorMessages})
		}else{
			if(nextProps.UserData.userData){
				 if(!nextProps.UserData.userData.gender) {
					this.props.history.push('./gender',nextProps.UserData.userData)	
				 }else if(!nextProps.UserData.userData.age){
					this.props.history.push('./old',nextProps.UserData.userData);	
				 }else if(_.isEmpty(nextProps.UserData.userData.loveAdCategory)){
					this.props.history.push('./love',nextProps.UserData.userData);
				 }else if(_.isEmpty(nextProps.UserData.userData.hateAdCategory)){
					this.props.history.push('./love',nextProps.UserData.userData);	
				 }else if(!nextProps.UserData.userData.prefCharity){
					this.props.history.push('./Charity',nextProps.UserData.userData);	
				}
			}else{
				if(nextProps.UserData.token){
					this.props.onchange();	
				}else{
					if(nextProps.UserData.userid ){
						this.setState({open:true})
					}
				}
			}
		}
	}
	_verifySubmit(evt){
    	evt.preventDefault();
		let attr = {
		  email: this.props.UserData.email,
		  code: this.state.code,
		};
		this.props.dispatch(verifycode(attr));
	}
	_changeCode(evt){
		this.setState({code:evt.target.value})
	}
	get verify(){
		return (
		  <form className="form" onSubmit={this._verifySubmit.bind(this)}>
			<ErrorMessage />
			<div className="row">
				<Input classname={'code'} type={'text'} value={this.state.code} placeholder={'Enter Your Code'} onChange={this._changeCode.bind(this)} />
			</div>
			<div className="row">
				<div className="col-sm-10">
				</div>
				<div className="col-sm-2">
				  {this.state.loading ? (
					<LoadingButton />
				  ) : (
					<button icon="cross" className="btn" type='submit'>{'Enter'}>&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
				  )}
				</div>
			</div>
		  </form>
		)

	}

	render() {
		const dispatch = this.props.dispatch;
		const {email,password} = this.props.UserData;
		let attr = {
			  email: email || '',
			  password: password || ''
		}
		console.log(attr)
		if(!this.state.open && !this.state.error){
		return (
				<div className="form-page__wrapper">
					<div className="form-page__form-wrapper">
					<Form data={attr} 
						  dispatch={dispatch} 
						  location={location} 
						  history={this.props.history}
						  onSubmit={this._login} 
						  loading={this.state.loading}
						  btnText={"Enter"} />
					</div>
				</div>
			);
		}else if(!this.state.open && this.state.error){
			return (
					<div className="form-page__wrapper">
						<div className="form-page__form-wrapper">
						{this.verify}
						</div>
					</div>
				);

		}else{
			return (
					<div className="form-page__wrapper">
						<div className="form-page__form-wrapper">
							<SignUpPage open={this.state.open}/>
						</div>
					</div>
				);

		}
  }

}
function select(state) {
  return {
    UserData: state.UserData
  };
}
module.exports=connect(select)(LoginPage);
