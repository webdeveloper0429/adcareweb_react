import React, { Component } from 'react';
import {connect} from 'react-redux';
import Backstretch from '../Backstretch';
import styles from '../../Page.css';
import H2 from '../../components/H2';
import {set_age} from '../../Actions/AppActions';
import old from '../../image/old.jpg';
import love from '../../image/Love.jpg';

class Old extends Component {
	constructor(props) {
		super(props);
		this.state = { old: '25'}
		this.onchange = this.onchange.bind(this);
		this.handleOnChange = this.handleOnChange.bind(this);
	}
	onchange(){
		let attr={
			Img:love,
			age:this.state.old,
			token:localStorage.token
		}
		this.props.dispatch(set_age(attr));
	}
	handleOnChange (event) {
		this.setState({old: event.target.value });
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.UserData.age){
			let attr={
				email:history.state.state.email,
				nickname:history.state.state.nickname,
				gender:history.state.state.gender,
				age:nextProps.UserData.age

			}
			this.props.history.push('./Love',attr)
		}
	}
	render() {
		const height=110;
		return (
				<div className="col-sm-12 col-lg-12">
					<Backstretch src={old} history={this.props.history} className="homeimage"/>
					<div className="container">
						<div className="row" style={{height:height}}>
						</div>
						<div className="row" style={{height:10}}>
							<div className="col-sm-5">
							</div>
							<div className="col-sm-4">
								<input type="text" style={{fontFamily: "Blod"}} name="age" value={this.state.old} onChange={this.handleOnChange} className="age"/>
							</div>
						</div>
						<div className="row" style={{height:260}}>
							<div className="col-sm-5">
							</div>
							<div className="col-sm-4">
								<input type="button" style={{fontFamily:"Font"}} name="age" value="Years Old" onClick={this.onchange} className="age"/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
							</div>
							<div className="col-sm-4">
								<H2>#2</H2>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
							</div>
							<div className="col-sm-4">
								<H2>How Old</H2>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-1">
							</div>
							<div className="col-sm-4">
								<H2>Are You?</H2>
							</div>
						</div>
					</div>
				</div>
			);
  		}
}
function select(state) {
  return {
    UserData: state.UserData
  };
}
module.exports=connect(select)(Old);
