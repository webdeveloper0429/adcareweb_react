import React, { Component} from 'react';
import { connect } from 'react-redux';
import {catch_profile,catch_category,update_profile,get_photo} from '../../Actions/Datas';
import Backstretch from '../Backstretch';
import H2 from '../../components/H2';
import H4 from '../../components/H4';
import img from '../../image/profile_img.jpg';
import whilte from '../../image/w.jpg';
import ProfileForm from '../ProfileForm';
import EditForm from '../EditForm';
import jwt_decode from 'jwt-decode';
import _ from 'lodash';
import {FormGroup,ControlLabel,FormControl,FieldGroup,Col} from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class ProfileEditPage extends Component {
	constructor(props) {
		super(props);
		if(_.isEmpty(props.Datas.loveDatas)){
			let attr={
				token:localStorage.token
			}
			this.props.dispatch(catch_category(attr));
		}
		this.onCancel = this.onCancel.bind(this);
		this.buttonChange = this.buttonChange.bind(this);
		this.onChange = this.onChange.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.state={value:[],much:'18,934',users:'8300',open:false,
					nickname:'',age:0,email:'',gender:'',hate:[],
					love:[],loveary:[],hateary:[],profileId:'',src:img}
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			var decoded=jwt_decode(localStorage.token)
			let attr={
				profileId:decoded.id,
				token:localStorage.token
			}
			this.setState({profileId:decoded.id});
			this.props.dispatch(catch_profile(attr));
			this.props.dispatch(get_photo(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.photo){
			console.log(nextProps.Datas.photo)
			this.setState({src:nextProps.Datas.photo})
		}
		if(!nextProps.Datas.profileDatas.edit && !this.state.open){
			var loveary=[];
			var hateary=[];
			var love_div = _.map(nextProps.Datas.profileDatas.rows.loveAdCategory, function(n, key) {
				loveary.push(n.id)
			});
			var hate_div = _.map(nextProps.Datas.profileDatas.rows.hateAdCategory, function(n, key) {
				hateary.push(n.id)
			});
			this.setState({nickname:nextProps.Datas.profileDatas.rows.nickname||null,
						   age:nextProps.Datas.profileDatas.rows.age||null,
						   email:nextProps.Datas.profileDatas.rows.email||null,
						   gender:nextProps.Datas.profileDatas.rows.gender ||null,
						   loveary:loveary ||[],
						   hateary:hateary ||[]
			})
		}else if(nextProps.Datas.profileDatas.edit && this.state.open){
			this.props.history.push('./profile')
		}
		
	}
	onChange(gender,age,email,password,loveid,hateid){
		let attr={
			token:localStorage.token,
			gender:gender,
			age:age,
			email:email,
			love:loveid,
			hate:hateid
		}
		console.log(attr)
		this.props.dispatch(update_profile(attr));
		this.setState({open:true})

	}
	handleChange(type,value,id) {
		if(type==='love'){
	  		this.setState({loveary:id,value:value})
		}else{
	  		this.setState({hateary:id,value:value})
		}
	}
	buttonChange(type,value){
		 switch (type) {
			 case 'age':
				this.setState({age:value})
				return;
			 case 'email':
				this.setState({email:value})
				return;
			 case 'password':
				this.setState({password:value})
				return;
			 case 'gender':
				this.setState({gender:value})
				return;
			 default:
				return;
		}
	}
	onCancel(){
		let attr={
			token:localStorage.token
		}
		this.props.dispatch(catch_profile(attr));
		this.setState({open:false})
		this.props.history.push('./profile')
	}
	render() {
		const dispatch = this.props.dispatch;
		const height=110;
		const { loveDatas,profileDatas} = this.props.Datas;
		var options = [];
		options=loveDatas;
		if(profileDatas.rows){
			var love_div = _.map(profileDatas.rows.loveAdCategory, function(n, key) {
				return(
						<div className="row" key={key.toString()}>
							<div className="col-sm-1">
								<h1 className='pro'>{n.name}</h1>
							</div>
						</div>

				);
			});
			var hate_div = _.map(profileDatas.rows.hateAdCategory, function(n, key) {
				return(
						<div className="row" key={key.toString()}>
							<div className="col-sm-1" >
								<h1 className='pro'>{n.name}</h1>
							</div>
						</div>

				);
			});
			var profile=(
				<div>
					<EditForm dispatch={dispatch}
							  history={this.props.history}
							  src={this.state.src} 
							  name={this.state.nickname} 
							  gender={this.state.gender}
							  age={this.state.age}
							  email={this.state.email}
							  onSubmit={this.onChange}
 							  value={this.state.value}
 							  password={this.state.password}
							  options={options}
							  selected={this.state.gender}
							  onChange={this.handleChange}
							  onbuttonChange={this.buttonChange}
							  loveid={this.state.loveary}
							  hateid={this.state.hateary}
							  onClick={this.onCancel}
					/>
				</div>
			);
		}
    return (
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Blod"}}>
				<Backstretch src={whilte} history={this.props.history} className="homeimage"/>
				{profile}
			</div>
		);
	}
}

function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(ProfileEditPage);
