import React, { Component} from 'react';
import { connect } from 'react-redux';
import {get_amount} from '../../Actions/Datas';
import Backstretch from '../Backstretch';
import H2 from '../../components/H2';
import H4 from '../../components/H4';
import img from '../../image/Charity.jpg';
import oceana from '../../image/oceana.png';
import jwt_decode from 'jwt-decode';
import _ from 'lodash';
import {FormattedNumber,IntlMixin,ReactIntl} from 'react-intl';

class TribePage extends Component {
	constructor(props) {
		super(props);
		this.state={ e_much:'',d_much:'',click_ln:'85',receive_ln:'70'}
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			var decoded = jwt_decode(localStorage.token);
			console.log(decoded);

			let attr={
				profileId:decoded.id,
				token:localStorage.token
			}
			this.props.dispatch(get_amount(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.amount){
			console.log(nextProps.Datas.amount.totalDonations)
			this.setState({d_much:nextProps.Datas.amount.totalDonations,
						   e_much:nextProps.Datas.amount.totalEarnings})

		}
		
	}
	render() {
    	return (
			<div className="col-sm-12 col-lg-12">
				<Backstretch src={img} history={this.props.history} className="homeimage"/>
				<div className="container">
					<div className="row" style={{height:320}}>
					</div>
					<div className="row" >
						<div className="col-sm-2" id="tribe">
							<img src={oceana}/>
						</div>
					</div>
					<div className="row" >
						<div className="col-sm-2">
						</div>
						<div className="col-sm-10">
							<H4>{'Donations From Referrals'}</H4>
						</div>
					</div>
					<div className="row" className='ranking'>
						<div className="col-sm-2">
						</div>
						<div className="col-sm-10">
							<FormattedNumber value={1000}  style="currency" currency="USD"  value={this.state.d_much}/>
							<span id='usd'>USD</span>
						</div>
					</div>
					<div className="row" >
						<div className="col-sm-2">
						</div>
						<div className="col-sm-10">
							<H4>{'Earnings From Referrals'}</H4>
						</div>
					</div>
					<div className="row" className='ranking'>
						<div className="col-sm-2">
						</div>
						<div className="col-sm-6">
							<FormattedNumber value={1000}  style="currency" currency="USD"  value={this.state.e_much}/>
							<span id='usd'>USD</span>
						</div>
						<div className="col-sm-2">
							<H2>{this.state.click_ln} <span id='friend'>Friends</span></H2>
							<H4>{'Clicked Lnvitation'}</H4>
						</div>
						<div className="col-sm-2">
							<H2>{this.state.receive_ln} <span id='friend'>Friends</span> </H2>
							<H4>{'Received Lnvitation'}</H4>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(TribePage);
