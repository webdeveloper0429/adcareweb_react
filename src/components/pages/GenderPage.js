import React, { Component } from 'react';
import {connect} from 'react-redux';
import Backstretch from '../Backstretch';
import gender from '../../image/gender.jpg';
import H3 from '../../components/H3';
import H2 from '../../components/H2';
import {set_gender} from '../../Actions/AppActions';
import old from '../../image/old.jpg';

class GenderPage extends Component {
	constructor(props) {
		super(props);
		this.onchange = this.onchange.bind(this);
	}
	onchange(event){
		console.log(this.props.history)
		if(event.target.value){
			let attr={
				Img:old,
				gender:event.target.value,
				token:localStorage.token
			}
			this.props.dispatch(set_gender(attr));
		}
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
		}

	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.UserData.gender){
			let attr={
				email:history.state.state.email,
				password:history.state.state.password,
				nickname:history.state.state.nickname,
				gender:nextProps.UserData.gender
			}
			this.props.history.push('./Old',attr)
		}
	}
	render() {
		const height=110;
    return (
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Font"}}>
				<Backstretch src={gender} history={this.props.history} className="homeimage"/>
				<div className="container">
					<div className="row" style={{height:height}}>
					</div>
				 	<div className="row" style={{height:90}}>
					 	<div className="col-sm-5">
					 	</div>
					  	<div className="col-sm-4">
							<H3>{'Answer01'}</H3>
							<input style={{fontFamily: "BlodHeavy"}} type="button" name="gender" value="Male" onClick={this.onchange} className="gender"/>
					 	</div>
					</div>
				 	<div className="row" style={{height:260}}>
					 	<div className="col-sm-4">
					 	</div>
					  	<div className="col-sm-4">
							<H3>{'Answer02'}</H3>
							<input style={{fontFamily: "BlodHeavy"}} type="button"  name="gender" value="Female" onClick={this.onchange} className="gender"/>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>#1</H2>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>I Identity My</H2>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>Gender As...</H2>
					 	</div>
					</div>
				</div>
			</div>
		);
  }
}
function select(state) {
  return {
    UserData: state.UserData
  };
}
module.exports=connect(select)(GenderPage);
