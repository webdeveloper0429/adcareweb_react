import React, { Component } from 'react';
import {connect} from 'react-redux';
import Backstretch from '../Backstretch';
import H2 from '../../components/H2';
import {set_pref_charity,catch_charity,send_code} from '../../Actions/Datas';
import charity from '../../image/Charity.jpg';
import home from '../../image/Home.jpg';
import CharityForm from '../../components/CharityForm';
import LoadingButton from '../LoadingButton';
import _ from 'lodash';

class CharityPage extends Component {
	constructor(props) {
		super(props);
		this.state={tab:'#4',button:'Like The Most',code:false}
		this.onClick = this.onClick.bind(this);
		this.Timeout = this.Timeout.bind(this);
	}
	Timeout(){
 		setTimeout(() => {
			this.setState({loading:false})
      	}, 1000);
	}
	onClick(key,id,value){
		this.setState({loading:true})
		let attr={
			Img:home,
			charity:value,
			token:localStorage.token,
			charityId:id,
			nickname:history.state.state.nickname,
			email:history.state.state.email
		}
		this.props.dispatch(set_pref_charity(attr));
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			let attr={
				token:localStorage.token,
				serviceType:'charity'
			}
			this.props.dispatch(catch_charity(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		let attr={
			token:localStorage.token,
			email:history.state.state.email,
			nickname:history.state.state.nickname,
		}
		if(nextProps.Datas.charity && !this.state.code){
			this.setState({code:true})
			this.props.dispatch(send_code(attr));
		}else if(nextProps.Datas.code){
			this.setState({loading:false})
			this.props.history.push('./',attr)
		}	
		
	}
	render() {
		const {CharityDatas} = this.props.Datas;
		const height=300;
		let classname='';
		var datas = _.map(CharityDatas, function(n, key) {
			return (
				<CharityForm key={key.toString()} 
							 id={(n.id).toString()} 
							 name={n.name} 
							 value={n.name} 
							 classname={'charity'+key}
							 description={n.description} 
							 onClick={this.onClick.bind(this, key)}
				/>
			);
		}.bind(this))
    return (
			<div className="col-sm-12 col-lg-12" id='charity' style={{fontFamily: "Font"}}>
					{this.state.loading?(
						<LoadingButton />
					):('')}
				<Backstretch src={charity} history={this.props.history} className="homeimage"/>
				<div className="container">
					<div className="row" style={{height:height}}>
					</div>
					{datas}
				 	<div className="row">
					 	<div className="col-sm-12">
							<H2>{this.state.tab}</H2>
							<H2>Which Charity</H2>
							<H2>Would You</H2>
							<H2>{this.state.button}?</H2>
					 	</div>
					</div>
				</div>
			</div>
		);
  }
}
function select(state) {
  	return {
    	Datas: state.Datas
  	};
}
module.exports=connect(select)(CharityPage);
