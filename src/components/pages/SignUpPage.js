import React, { Component} from 'react';
import { connect } from 'react-redux';
import SignupForm from '../SignupForm';
import {register,verifycode} from '../../Actions/AppActions';
import ErrorMessage from '../ErrorMessage';
import LoadingButton from '../LoadingButton';
import Input from '../Input';
import img from '../../image/logonin.jpg';

class SignUpPage extends Component {
	constructor(props) {
		super(props);
		this.state={loading:false,open:false,code:'',email:'',nickname:''}
		this._signup= this._signup.bind(this);
		this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
		this.Timeout = this.Timeout.bind(this);
	}
	Timeout(){
 		setTimeout(() => {
			this.setState({loading:false})
      	}, 1000);
	}
	_signup(email, password,nickname) {
		this.setState({loading:true})
		this.props.dispatch(register(email,password,nickname));
		this.Timeout();
	}
	_changeCode(evt){
		this.setState({code:evt.target.value})
	}
	_verifySubmit(evt){
    	evt.preventDefault();
		let attr = {
		  email: history.state.state.email,
		  code: this.state.code,
		};
		this.props.dispatch(verifycode(attr));
	}
	componentDidMount() {
 		if(localStorage.userid){
			this.setState({open:true,nickname:history.state.state.nickname})
		}
	}
	componentWillReceiveProps(nextProps) {
		 if(nextProps.UserData.charity){
				this.setState({open:true})
		 }else{
			if(nextProps.UserData.loggedIn){
				if(nextProps.UserData.token){
					this.props.history.push('/');
				}
				else{
					this.props.history.push('./Gender',{loading:false,
														id:nextProps.UserData.id,
														loginType:nextProps.UserData.loginType,
														email:nextProps.UserData.email,
														password:nextProps.UserData.password,
														nickname:nextProps.UserData.nickname})
				}
			}else{
				this.props.history.push('/');
			
			}
		 }
	}
	get verify(){
		return (
		  <form className="form" onSubmit={this._verifySubmit.bind(this)}>
			<ErrorMessage />
			<div className="row">
				<Input classname={'code'} type={'text'} value={this.state.code} placeholder={'Enter Your Code'} onChange={this._changeCode.bind(this)} />
			</div>
			<div className="row">
				<div className="col-sm-10">
				</div>
				<div className="col-sm-2">
				  {this.state.loading ? (
					<LoadingButton />
				  ) : (
					<button icon="cross" className="btn" type='submit'>{'Enter'}>&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
				  )}
				</div>
			</div>
		  </form>
		)

	}

	render() {
		const dispatch = this.props.dispatch;
		const {email,nickname,password,currentlySending} = this.props.UserData;
		let attr={
			  email: email || '',
			  nickname: nickname || '',
			  password: password || ''
		}
		if(this.state.open && this.state.nickname){
			return (
					<div className="form-page__wrapper">
						<div className="form-page__form-wrapper">
						{this.verify}
						</div>
					</div>
				);

		}else{
			return (
					<div className="form-page__wrapper">
						<div className="form-page__form-wrapper">
						<SignupForm data={attr}  
									dispatch={dispatch} 
									history={this.props.history} 
									onSubmit={this._signup} 
									btnText={"Enter"}
						  			loading={this.state.loading}
								   	data={attr}	
						/>
						</div>
					</div>
				);
		}
  }
}
function select(state) {
  return {
	UserData: state.UserData
  };
}
module.exports=connect(select)(SignUpPage);
