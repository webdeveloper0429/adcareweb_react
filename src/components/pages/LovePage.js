import React, { Component } from 'react';
import {connect} from 'react-redux';
import Backstretch from '../Backstretch';
import H2 from '../../components/H2';
import {set_love_ad_category,set_hate_ad_category,catch_category} from '../../Actions/Datas';
import love from '../../image/Love.jpg';
import _ from 'lodash';
import LoveForm from '../../components/LoveForm';
import LoadingButton from '../LoadingButton';
import Loading from 'react-loading';
import ErrorMessage from '../ErrorMessage';

class LovePage extends Component {
	constructor(props) {
		super(props);
		this.state={tab:'#3',button:'Love',loading: false,value:[],type:false}
		this.onClick = this.onClick.bind(this);
		this._onClick = this._onClick.bind(this);
		this.Timeout = this.Timeout.bind(this);
	}
	Timeout(){
 		setTimeout(() => {
			this.setState({loading:false})
      	}, 1000);
	}
	onClick(id){
		this.setState({type:false})
		//this.state.value.push(id)
		if(!_.includes(this.state.value,id)){
			this.state.value.push(id)
		}else{
			var index=_.findIndex(this.state.value,function(x) { 
				return x == id; 
			});
			this.state.value.splice(index,1)
		}
		if(_.isEmpty(this.state.value)){
			this.state.value.push(id)
		}
		console.log(this.state.value)
	}
	_onClick(){
		this.setState({loading:true})
		let attr={}
		if(this.state.button==='Hate'){
			this.setState({loading:false})
			attr={
				Img:love,
				ids:this.state.value,
				token:localStorage.token,
			}
			this.props.dispatch(set_hate_ad_category(attr));
		}else{
			this.setState({loading:false})
			if(_.isEmpty(this.state.value)){
				return false;
			}else{
				attr={
					Img:love,
					ids:this.state.value,
					token:localStorage.token,
				}
				this.props.dispatch(set_love_ad_category(attr));
			}
		
		}
		
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			this.setState({loading:true})
			let attr={
				token:localStorage.token
			}
			this.props.dispatch(catch_category(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.love){
			this.setState({button:'Hate',value:[],loading:false,type:true})
		}else if(nextProps.Datas.hate){
			let attr={
				email:history.state.state.email,
				password:history.state.state.password,
				nickname:history.state.state.nickname,
				gender:history.state.state.gender,
				age:history.state.state.age,
				hate:nextProps.Datas.hate,
				love:this.props.Datas.love
			}
			this.props.history.push('./Charity',attr)
		
		}
	}
	render() {
		const {loveDatas,currentlySending} = this.props.Datas;
		const height=110;
		if(loveDatas){
			this.state.loading=false;
			var classname='';
			var datas = _.map(loveDatas, function(n, key) {
				if(key%2=='1'){
					classname='player1';
				}else{
					classname='player2';
				}
				return(
				<LoveForm key={key.toString()} 
							 id={(n.id).toString()} 
							 name={n.name} 
							 value={n.name} 
							 classname={classname}
							 type={this.state.type}
							 onClick={this.onClick.bind(this, key)}
				/>
				);
			}.bind(this))
		}
    return (
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "myFirstFont"}}>
				<Backstretch src={love} history={this.props.history} className="homeimage"/>
				<div className="container">
					<ErrorMessage />
					<div className="row" style={{height:height}}>
					</div>
					<ul>
						{datas}
					</ul>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>{this.state.tab}</H2>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-8">
							<H2>What Do You {this.state.button}?</H2>
					 	</div>
					 	<div className="col-sm-2">
						  {this.state.loading ? (
								<LoadingButton />
						  	) : (
								<input  type="button"   value={'Confirm,'} onClick={this._onClick} className="love"/>
				  			)}
					 	</div>
					</div>
				</div>
			</div>
		);
  }
}
function select(state) {
  return {
    UserData: state.UserData,
    Datas: state.Datas
  };
}
module.exports=connect(select)(LovePage);
