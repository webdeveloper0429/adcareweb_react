import React, { Component } from 'react';
import {connect} from 'react-redux';
import Backstretch from '../Backstretch';
import wallet from '../../image/Black.jpg';
import CharityForm from '../../components/CharityForm';
import _ from 'lodash';
//import d3 from 'react-d3'; 
import H2 from '../../components/H2';
import H4 from '../../components/H4';
//import {LineTooltip} from 'react-d3-tooltip';
//import d3 from "d3";
//var LineTooltip = require('react-d3-tooltip').LineTooltip;
import {get_donation,get_amount,get_spending} from '../../Actions/Datas';
import rd3 from 'react-d3-library';
//const LineChart = rd3.LineChart;
import style from '../../LineChart.css';
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, ReferenceLine,
  ReferenceDot, Tooltip, CartesianGrid, Legend, Brush, ErrorBar, AreaChart, Area } from 'recharts';
import {Grid,Row,Col} from 'react-bootstrap';
import 'recharts/umd/Recharts.min.js';

let dt = new Date();
let month = new Array(12);
let months = "January,February,March,April,May,June,July,August,September,October,November,December".split(",");
let year=dt.getFullYear()
let now_month=(dt.getMonth())+1
var days = new Date(year,now_month,0).getDate();
let rows=[];
for(let i=1; i<=days; i++){
	rows.push({day:i})
}
rows.push({day:months[dt.getMonth()]})

export function CustomTooltip(props) {
  if (props.active) {
    const { payload } = props;
    return (
      <div className='custom-tooltip'>
        <H2>${payload[0].value}<span>{'usd'}</span></H2>
      </div>
    );
  }

  return null;
}

class WalletPage extends Component {
	constructor(props) {
		super(props);
		  	this.state={donated:0,d3:'',e_much:'',d_much:'',rows:rows,opacity: 1,anotherState: false,style_s:{color:'white'},style:{color:'gray'}}
			this._onClick = this._onClick.bind(this);
			this.onClick = this.onClick.bind(this);

	}
	componentDidMount() {
		let attr={
			token:localStorage.token
		}
		//this.props.dispatch(get_donation(attr));
		this.props.dispatch(get_amount(attr));
		this.setState({style_s:{color:'white'},style:{color:'gray'}});
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.donation){
			let rows=[];
			let values={};
			console.log(nextProps.Datas.donation)
			let datas = _.map(nextProps.Datas.donation, function(n, key) {
				if(n.walletRecord.datetime){
					let d = new Date(n.walletRecord.datetime);
					let dd=d.toString().split(" ")
					console.log(dd)
					let donated=Number(n.earningRecord.donated)
					if(_.isUndefined(values[dd[2]])){
						values[dd[2]]=0;
					}
					values[dd[2]]+= donated;
				}
			});
			let results = _.map(values, function(n, key) {
				rows.push({'day':key,'value':n})
			});
			let new_rows=_.sortBy(rows, ['day', 'value']);
			new_rows.push({day:months[dt.getMonth()]})
			var data=new_rows.concat(this.state.rows)
			console.log(data)

			this.setState({rows:new_rows})
		}
		if(nextProps.Datas.amount){
			this.setState({d_much:nextProps.Datas.amount.totalDonations,
						   e_much:nextProps.Datas.amount.totalEarnings})

		}
	}
	_onClick(){
		this.props.history.push('./cash');
	}
	onClick(e){
		this.setState({rows:rows})

		const type=e.target.getAttribute('data-i')
		let attr={
			token:localStorage.token
		}
		if(type=='Spending'){
			this.setState({style_s:{color:'white'},style:{color:'gray'}})
			//this.props.dispatch(get_spending(attr));
		}else{
			this.setState({style_s:{color:'gray'},style:{color:'white'}})
			this.props.dispatch(get_donation(attr));
		}
	
	}
  	handleLegendMouseEnter() {
    	this.setState({
      	opacity: 0.5,
    	});
  	}

 	handleLegendMouseLeave() {
    	this.setState({
      		opacity: 1,
    	});
  	}
	render () {
    	const {rows,opacity} = this.state;
		console.log(rows)
		return (
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Font"}}>
				<Backstretch src={wallet} history={this.props.history} className="homeimage"/>
					<Grid>
					    <Row className="show-grid">
						  <Col xs={12} md={12}>
							<LineChart width={950} height={300} data={this.state.rows}
								  margin={{top: 5, right: 5, bottom: 5, left: 5 }}>
							  <XAxis dataKey="day"/>
							  <YAxis dataKey="value"/>
							  <Tooltip content={<CustomTooltip />} cursor={false}/>
							  <Line connectNulls={true}  dataKey='value' stroke='#FFFFFF' fill='#FFFFFF' />
							</LineChart>
						  </Col>
						</Row>
					    <Row className="show-grid">
						  <Col xs={6} md={4}>
						  </Col>
						  <Col xs={6} md={4}>
            				<button className='wallet_btn' data-i="Spending" type='button' style={this.state.style_s} onClick={this.onClick}>{'Spending History'}</button>
						  </Col>
						  <Col xs={6} md={4}>
            				<button className='wallet_btn' data-i="Donation" type='button' style={this.state.style} onClick={this.onClick}>{'Donation History'}</button>
						  </Col>
						</Row>
					    <Row className="show-grid">
						  <Col xs={12} md={12}>
							<H4>{'Total Earnings'}</H4>
						  </Col>
						</Row>
					    <Row className="show-grid">
						  <Col xs={12} md={12}>
							<H2>${this.state.e_much}<span>USD</span></H2>
						  </Col>
						</Row>
					    <Row className="show-grid">
						  <Col xs={6} md={4}>
							<H4 id='wallet'>{'Total Donations'}</H4>
						  </Col>
						  <Col xs={6} md={4}>
							<H4>{'How to use?'}</H4>
						  </Col>
						  <Col xs={6} md={4}>
							<H4>{'Feel meaninful?'}</H4>
						  </Col>
						</Row>
					    <Row className="show-grid">
						  <Col xs={6} md={4}>
							<H2>${this.state.d_much}<span>USD</span></H2>
						  </Col>
						  <Col xs={6} md={4}>
							<input  type="button" value={'Cash Out,'} onClick={this._onClick} className="wallet"/>
						  </Col>
						  <Col xs={6} md={4}>
							<input  type="button"  value={'Impact Report,'} onClick={this._onClick} className="wallet"/>
						  </Col>
						</Row>
					</Grid>
			</div>
		);
	}
}
function select(state) {
  	return {
    	Datas: state.Datas
  	};
}
module.exports=connect(select)(WalletPage);
