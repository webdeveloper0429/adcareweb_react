import React, { Component} from 'react';
import { connect } from 'react-redux';
import { login} from '../../Actions/AppActions';
import Input from '../Input';
import Backstretch from '../Backstretch';
import _ from 'lodash';
import ad from '../../image/Black.jpg';
import img1 from '../../image/oceana.svg';
import img2 from '../../image/green.svg';
import pay1 from '../../image/pay1.svg';
import pay2 from '../../image/pay2.svg';
import H2 from '../../components/H2';


class CashPage extends Component {
	constructor(props) {
		super(props);
		this.state={mush:'',title:''}
		this.onSubmit = this.onSubmit.bind(this);
		this.changeMuch = this.changeMuch.bind(this);
		this.show = this.show.bind(this);
	}
	onSubmit(){
		console.log('123')
	}
	changeMuch(){
	
	}
	show(e){
		const type=e.target.getAttribute('data-i')
		this.setState({title:type})
	}
	render() {
		const height=250;
		if(this.state.title==='donation'){
			var button=(
				<div>
					<div className="col-sm-4">
					</div>
					<div className="col-sm-8" id="img1">
						<img  src={img1}  />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<img  src={img2}  />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<img  src={img1}  />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<img  src={img2}  />
					</div>
				</div>
				)
		}else if(this.state.title==='pay'){
			var button=(
				<div>
					<div className="col-sm-4">
					</div>
					<div className="col-sm-8" id="img1">
						<img  src={pay1}  />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<img  src={pay2}  />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<img  src={pay1}  />
						&nbsp;&nbsp;&nbsp;&nbsp;
						<img  src={pay2}  />
					</div>
				</div>
				)
		}else{
			var button=(
					<Input classname='input' type={'text'} value={this.state.much} placeholder={'How Much?'}onChange={this.changeMuch}/>
				)
		}
		return (
		<div className="col-sm-12 col-lg-12" style={{fontFamily: "Font"}}>
			<Backstretch src={ad} history={this.props.history} className="homeimage"/>
			<div className="container">
				<form className="form" onSubmit={this.onSubmit}>
					<div className="row" style={{height:height}}>
					</div>
					<div className="row">
						<div className="col-sm-8">
						</div>
						<div className="col-sm-4">
							<a href="javascript:void (0)" data-i="cash"  onClick={this.show} >{'Cash Out'}</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="javascript:void (0)" data-i="pay" onClick={this.show}>{'Pay Service'}</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="javascript:void (0)" data-i="donation" onClick={this.show}>{'Donation'}</a>
						</div>
					</div>
					<div className="row" style={{height:150}}> 
						<div className="col-sm-2">
						</div>
						<div className="col-sm-10">
							{button}
						</div>
					</div>
				</form>
				<div className="row"> 
					<div className="col-sm-2">
					</div>
					<div className="col-sm-3" style={{width:250}}>
					<H2>{'What Do You Want To Do?'}</H2>
					</div>
					<div className="col-sm-9">
					</div>
				</div>
			</div>
		</div>
		)
	}
}
function select(state) {
  return {
    data: state
  };
}
module.exports=connect(select)(CashPage);
