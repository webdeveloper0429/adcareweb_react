import React, { Component} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import  Video from '../video';
import Backstretch from '../../components/Backstretch';
import Dg from '../../components/Dg';
import ad from '../../image/Black.jpg';
import image from '../../image/Ad.jpg';
import play from '../../image/play.png';
import {catch_ad} from '../../Actions/AppActions';
class  CheckPage extends Component {
	constructor(props) {
		super(props);
		this.state = { videoid:'',play: true,open:false,Title:'DG Family',value:'Doke & Gabbana',et:'0.30',dt:'0.40'}
	}
	_onClick(id) {
		let attr={
			token:localStorage.token,
			id:id
		}
		this.props.history.push('/Video/'+id);
		//this.props.dispatch(catch_ad(attr));
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			this.setState({videoid:this.props.match.params.id})
		}
	}
	render() {
		const {domainName} = this.props.Datas;
		var adId=this.state.videoid;
		var img=`${domainName}/adcare/webadmin/image/${adId}`;
		var videos={};
		videos[adId]=img;
		console.log(videos);
	  	var datas = _.map(videos, function(n, key) {
			return <Video close={true} 
 					key={key.toString()} 
					playedsrc={play} 
					id={adId} 
					src={n} 
					play={'play_img_one'} 
					name={'ad_img_one'}
					onClick={this._onClick.bind(this,adId)}
					/>
		}.bind(this));
		const header=(
				<h1 className='head'>{'Wanna see ?'}</h1>
		)
		return(
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Font"}}>
				<Backstretch src={ad} history={this.props.history} className="homeimage"/>
				{header}
				<div className="container">
					<div className="row" style={{height:25}}>
						<Dg Title={this.state.Title} 
							value={this.state.value} 
							style={'dg_title_one'} 
							ET={'$'+this.state.et} 
							DT={'$'+this.state.dt} 
							eorned={'Earned'} 
							donated={'Donated to OCEANA'} 
							open={true} 
							id={this.state.videoid}
							/>
					</div>
					<div className="row">
						<div className="col-sm-4">
						</div>
						<div className="col-sm-4">
							{datas}
						</div>
					</div>
				</div>
			</div>

		)
	
	}
}
function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(CheckPage);
