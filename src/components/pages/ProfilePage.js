import React, { Component} from 'react';
import { connect } from 'react-redux';
import {catch_profile,catch_category,update_profile,get_photo} from '../../Actions/Datas';
import Backstretch from '../Backstretch';
import img from '../../image/profile_img.jpg';
import whilte from '../../image/w.jpg';
import ProfileForm from '../ProfileForm';
import jwt_decode from 'jwt-decode';
import _ from 'lodash';
class ProfilePage extends Component {
	constructor(props) {
		super(props);
		if(_.isEmpty(props.Datas.loveDatas)){
			let attr={
				token:localStorage.token
			}
			this.props.dispatch(catch_category(attr));
		}
		this.onCancel = this.onCancel.bind(this);
		this.buttonChange = this.buttonChange.bind(this);
		this.onChange = this.onChange.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.state={value:[],much:'18,934',users:'8300',open:false,
					nickname:'',age:'',email:'',gender:'',hate:[],
					love:[],loveary:[],hateary:[],profileId:'',src:img}
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			var decoded=jwt_decode(localStorage.token)
			let attr={
				profileId:decoded.id,
				token:localStorage.token
			}
			this.setState({profileId:decoded.id});
			this.props.dispatch(catch_profile(attr));
			this.props.dispatch(get_photo(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.photo){
			console.log(nextProps.Datas.photo)
				var photo=nextProps.Datas.photo
			this.setState({src:photo})
		}
		if(!nextProps.Datas.profileDatas.edit && !this.state.open){
			var loveary=[];
			var hateary=[];
			 _.map(nextProps.Datas.profileDatas.rows.loveAdCategory, function(n, key) {
				loveary.push(n.id)
			});
			_.map(nextProps.Datas.profileDatas.rows.hateAdCategory, function(n, key) {
				hateary.push(n.id)
			});
			this.setState({nickname:nextProps.Datas.profileDatas.rows.nickname,
						   age:nextProps.Datas.profileDatas.rows.age,
						   email:nextProps.Datas.profileDatas.rows.email,
						   gender:nextProps.Datas.profileDatas.rows.gender,
						   loveary:loveary,
						   hateary:hateary
			})
		}else if(nextProps.Datas.profileDatas.edit && this.state.open){
			this.setState({open:false})
		}
		
	}
	onChange(gender,age,email,password,loveid,hateid){
		let attr={
			token:localStorage.token,
			gender:gender,
			age:age,
			email:email,
			love:loveid,
			hate:hateid
		}
		this.props.dispatch(update_profile(attr));

	}
	handleChange(type,value,id) {
		if(type==='love'){
	  		this.setState({loveary:id,value:value})
		}else{
	  		this.setState({hateary:id,value:value})
		}
	}
	buttonChange(type,value){
		 switch (type) {
			 case 'age':
				this.setState({age:value})
				return;
			 case 'email':
				this.setState({email:value})
				return;
			 case 'password':
				this.setState({password:value})
				return;
			 case 'gender':
				this.setState({gender:value})
				return;
			 default:
				return;
		}
	}
	onCancel(){
		let attr={
			token:localStorage.token
		}
		this.props.dispatch(catch_profile(attr));
		this.setState({open:false})
		this.props.history.push('./profile')
	}
	render() {
		const dispatch = this.props.dispatch;
		const { loveDatas,profileDatas} = this.props.Datas;
		if(profileDatas.rows){
			var love_div = _.map(profileDatas.rows.loveAdCategory, function(n, key) {
				return(
						<div className="row" key={key.toString()}>
							<div className="col-sm-1">
								<h1 className='pro'>{n.name}</h1>
							</div>
						</div>

				);
			});
			var hate_div = _.map(profileDatas.rows.hateAdCategory, function(n, key) {
				return(
						<div className="row" key={key.toString()}>
							<div className="col-sm-1" >
								<h1 className='pro'>{n.name}</h1>
							</div>
						</div>

				);
			});
				var profile=(
				<div className="container">
					<div className="pro_tab">
						<div className="row">
							<div className="col-sm-2">
								<h1 className='profile'>{'Love,'}</h1>
							</div>
							<div className="col-sm-3">
							</div>
						</div>
						{love_div}
						<div className="row">
							<div className="col-sm-2">
								<h1 className='profile'>{'Hate,'}</h1>
							</div>
							<div className="col-sm-3">
							</div>
						</div>
						{hate_div}
					</div>
					<ProfileForm dispatch={dispatch}
							  	 history={this.props.history}
								 name={profileDatas.rows.nickname} 
								 gender={profileDatas.rows.gender}
								 age={profileDatas.rows.age}
								 email={profileDatas.rows.email}
								 src={this.state.src}
					/>	
				</div>
				)
		}
    return (
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Blod"}}>
				<Backstretch src={whilte} history={this.props.history} className="homeimage"/>
				{profile}
			</div>
		);
	}
}

function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(ProfilePage);
