import { DefaultPlayer as Video } from 'react-html5video';
import { Player, LoadingSpinner,ControlBar} from 'video-react';
import 'react-html5video/dist/styles.css';
import styles from '../../Video.css';
import {connect} from 'react-redux';
import React,{Component} from 'react';
import {catch_ad,add_wallet} from '../../Actions/Datas';
import _ from 'lodash';
import Scroll  from 'react-scroll';
import ReactDOM from 'react-dom';
import ErrorMessage from '../ErrorMessage';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import 'video-react/dist/video-react.css';

class VideoPage extends Component {
	constructor(props) {
		super(props);
		this.state={videoId:0,videos:[],player:'',marginBottom:0,id:0,value:1,direction: 1}
		this.onEnd=this.onEnd.bind(this);
		
		// to detect onwheel
		this.wheeling = false;
		this.wheeldelay = false;
	}
	handleButtonClick = (inc) => {
		let id = this.state.id + inc;
		id = id > 0 ? id : this.state.value - 1;
		id = id <= this.state.value ? id : 1;
		const direction = inc;
		this.setState({id:id, direction:direction});
		this.props.history.push('./'+id)
	}


	renderItem = (item) => {
		const classes = `slide`;
		console.log(item)
		if(!_.isEmpty(item)){
			var data=(
			<Player
				playsInline
				muted={true}
				ref={'play_'+item.id}
				source src={item.url}
				onEnded={this.onEnd.bind(this,item.id)}
				>
				<source src={item.url} />
				<ControlBar autoHide={false} />
			</Player>
			)
		}
		return (
			<div key={item.id} className={classes}>
				{data}
			</div>
		);
	}
	// detect wheel stop
	wheelstop = (yindex) => {
		if (yindex > -15 && yindex < 15) {
			this.wheeling = false;
			console.log(yindex,"stopped");
			return true;
		} 
		return false;
	}

	// detect wheel start
	wheelstart = (yindex) => {
		if (this.wheeldelay) return false;
		if (yindex > 30 || yindex < -30) {
			this.wheeldelay = true;
			setTimeout(( () => {this.wheeldelay = false}) , 500 );
			this.wheeling = true;
			console.log(yindex,"start");
			return true;
		}
		return false;
	}

	w = (event) => {
		//console.log("w",event.deltaX,event.deltaY,event.deltaZ,event.deltaMode);

		if (this.wheeling) {
			this.wheelstop(event.deltaY)
		} else {
			if (this.wheelstart(event.deltaY)) {
				if (event.deltaY > 0) {
					console.log('start scroll down');
					this.handleButtonClick(1);
				} else if (event.deltaY <0) {
					console.log('start scroll up');
					this.handleButtonClick(-1);
				}
			}
		}
	}
	onEnd(id){
		let attr={
			token:localStorage.token,
			id:id
		}
		this.props.dispatch(add_wallet(attr));
		//console.log(this.refs.player.getState())
	}
	paneDidMount(node){
		console.log(node)
    	if (node) {
      		//node.addEventListener('scroll',  console.log('scroll!'));
    	}
  	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('./')
        }else{
			this.setState({
				id: Number(this.props.match.params.id)
			});
			let attr={
				token:localStorage.token,
			}
			this.props.dispatch(catch_ad(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.AdDatas &&!nextProps.Datas.add){
			this.setState({
				value: (nextProps.Datas.AdDatas).length
			});
		}
		if(nextProps.Datas.add && nextProps.Datas.id){
			this.props.history.push('../played/'+nextProps.Datas.id)

		}
		
	}

	render(){
		const {AdDatas} = this.props.Datas;
		const classes = `slide-container`;
		var data={}
		var datas = _.map(AdDatas, function(n, key) {
			if(n.id==this.state.id){
				data=n;
			}
		}.bind(this))
		console.log(data)
		return (
			<div ref='test' onWheel={this.w}>
				<CSSTransitionGroup transitionName="slide" className={classes} transitionEnterTimeout={500} transitionLeaveTimeout={300}>
					{this.renderItem(data)}
				</CSSTransitionGroup>
			</div>
		)
	}
}
function select(state) {
  return {
    Datas: state.Datas
  };
}
module.exports=connect(select)(VideoPage);
