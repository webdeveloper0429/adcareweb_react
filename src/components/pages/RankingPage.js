import React, { Component} from 'react';
import { connect } from 'react-redux';
import {get_amount} from '../../Actions/Datas';
import Backstretch from '../Backstretch';
import H2 from '../../components/H2';
import H4 from '../../components/H4';
import img from '../../image/ranking.jpg';
import {FormattedNumber,IntlMixin,ReactIntl} from 'react-intl';
import {Grid,Row,Col} from 'react-bootstrap';


class RankingPage extends Component {
	constructor(props) {
		super(props);
		this.state={much:'',users:'8300'}
		this.submit=this.submit.bind(this)
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			let attr={
				token:localStorage.token
			}
			this.props.dispatch(get_amount(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.Datas.amount){
			this.setState({much:nextProps.Datas.amount.totalDonations})

		}
	}
	submit(){
		this.props.history.push('./country')
	}
	render() {
    return (
			<div className="col-sm-12 col-lg-12" style={{fontFamily: "Font"}}>
				<Backstretch src={img} history={this.props.history} className="homeimage"/>
				<div className="container">
					<div className="row" style={{height:400}}>
					</div>
					<Grid>
					    <Row className="show-grid">
						  <Col sm={6} md={4}>
								<button icon="cross" className="Ranking_btn" onClick={this.submit}>>&nbsp;&nbsp;
									<i className="icon-chevron-right"/>
								</button>
						  </Col>
						</Row>
					    <Row className="show-grid">
						      <Col sm={6} md={3}>
								<H4>{'All donations on'}</H4>
							  </Col>
						</Row>
					    <Row className="show-grid">
						      <Col id='one'sm={6} md={3}>
								<h2>{'oneoneday,'}</h2>
							  </Col>
						</Row>
					    <Row className="show-grid">
						      <Col id='ranking'sm={6} md={3}>
								<FormattedNumber value={1000}  style="currency" currency="USD"  value={this.state.much}/>
							  </Col>
						      <Col sm={6} md={3}>
								<span id="usd">USD</span>
							  </Col>
						</Row>
					    <Row className="show-grid">
						      <Col sm={6} md={3}>
								<H4>{this.state.users}<span>{'users have donationed'}</span></H4>
							  </Col>
						</Row>
					</Grid>
				</div>
			</div>
		);
	}
}

// Which props do we want to inject, given the global state?
function select(state) {
  return {
    Datas: state.Datas
  };
}

// Wrap the component to inject dispatch and state into it
module.exports=connect(select)(RankingPage);
