import React, { Component} from 'react';
import { connect } from 'react-redux';
import {get_country,logout} from '../../Actions/Datas';
import Backstretch from '../Backstretch';
import H2 from '../../components/H2';
import H4 from '../../components/H4';
import AppHeader from '../../AppHeader.js';
import kingdom from '../../image/kingdom.jpg';
import japan from '../../image/japan.jpg';
import usa from '../../image/usa.jpg';
import hongkong from '../../image/hongkong.jpg';
import france from '../../image/france.jpg';
import black_logo from '../../image/logo_black.svg';
import {FormattedNumber,IntlMixin,ReactIntl} from 'react-intl';
import _ from 'lodash';

class CountryPage extends Component {
	constructor(props) {
		super(props);
		this.onClick = this.onClick.bind(this);
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			let attr={
				token:localStorage.token
			}
			this.props.dispatch(get_country(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
	}
	onClick() {
		this.props.history.push('/')
	}
	logout(){
		this.props.dispatch(logout());
		console.log(this.props.history)
		this.props.history.push('/');
	}

	render() {
		const {country} = this.props.Datas;
		var datas = _.map(country, function(n, key) {
			return(
				<div key={key.toString()} className={'top'+key}>
					<h1>#{key+1} {n.name}</h1>
					<FormattedNumber value={1000}  style="currency" currency="USD"  value={n.amount}/>
					<span id='country'>USD</span>
				</div>
				
				);
		});
		return (
			<div style={{fontFamily: "Font"}}>
				<Backstretch src={""} history={this.props.history} className="homeimage"/>
				<div className='top'>
					<h1>{'Top 5 countries'}</h1>
				</div>
				<div className='top6'>
					{datas}
				</div>
				<button className='logo' type="button" onClick={this.onClick}>
					<img src={black_logo} />
				</button>
			</div>
		);
	}
}

// Which props do we want to inject, given the global state?
function select(state) {
  return {
    Datas: state.Datas
  };
}

// Wrap the component to inject dispatch and state into it
module.exports=connect(select)(CountryPage);
