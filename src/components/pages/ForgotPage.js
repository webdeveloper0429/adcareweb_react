import React, { Component} from 'react';
import { connect } from 'react-redux';
import { forgot ,VerifyJudgment,forgotreset} from '../../Actions/AppActions';
import ForgotForm from '../ForgotForm';
import VerifyForm from '../VerifyForm';

class ForgotPage extends Component {
	constructor(props) {
		super(props);
		this.state={open:false}
		this._onforgot= this._onforgot.bind(this);
		this._onVerify= this._onVerify.bind(this);
	}
	_onforgot(email){
		if(email){
			let attr = {
			  email: email
			}
			this.props.dispatch(forgot(attr));
		}else{
			this.props.dispatch(VerifyJudgment('email is empty'));
			return false;	
		}
	
	}
	_onVerify(email,password,code){
		this.props.dispatch(forgotreset(email,password,code));
	}
	componentWillReceiveProps (nextProps) {
		if(nextProps.UserData.open){
			this.setState({open:true})
		}
	}
	render(){
		const dispatch = this.props.dispatch;
		const { email,password,currentlySending} = this.props.UserData;
		let attr={
			  email:email ||'',
			  password:password || '',
			  open:false,
		}
		return (
			<div className="form-page__wrapper">
				<div className="form-page__form-wrapper">
				{this.state.open?(
					<VerifyForm data={attr}  
								dispatch={dispatch} 
								location={location} 
								history={this.props.history} 
								onSubmit={this._onVerify} 
								btnText={"Enter"} 
								currentlySending={currentlySending}
					/>
				):(
					<ForgotForm data={attr}  
								dispatch={dispatch} 
								location={location}
							   	history={this.props.history} 
								onSubmit={this._onforgot} 
								btnText={"Enter"} 
								currentlySending={currentlySending}
					/>
				)}
				</div>
			</div>
		)
	
	
	}
}
function select(state) {
  return {
    UserData: state.UserData
  };
}
module.exports=connect(select)(ForgotPage);
