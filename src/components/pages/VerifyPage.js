import React, { Component} from 'react';
import { connect } from 'react-redux';
import {send_code,verifycode} from '../../Actions/AppActions';

import ErrorMessage from '../ErrorMessage';
import LoadingButton from '../LoadingButton';
import Input from '../Input';
class VerifyPage extends Component {
	constructor(props) {
		super(props);
		this.state={code:''}
	}
	_changeCode(evt){
		this.setState({code:evt.target.value})
	}
	componentDidMount() {

		var attr={
			email:history.state.state.email
		}
		this.props.dispatch(send_code(attr));
	}
	_verifySubmit(evt){
    	evt.preventDefault();
		let attr = {
		  email: this.props.data.formState.email,
		  code: this.state.code,
		};
		this.props.dispatch(verifycode(attr));
	}
	get verify(){
		return (
		  <form className="form" onSubmit={this._verifySubmit.bind(this)}>
			<ErrorMessage />
			<div className="row">
				<Input classname={'code'} type={'text'} value={this.state.code} placeholder={'Enter Your Code'} onChange={this._changeCode.bind(this)} />
			</div>
			<div className="row">
				<div className="col-sm-10">
				</div>
				<div className="col-sm-2">
				  {this.props.currentlySending ? (
					<LoadingButton />
				  ) : (
					<button icon="cross" className="btn" type='submit'>{'Enter'}>&nbsp;&nbsp;<i className="icon-chevron-right"/></button>
				  )}
				</div>
			</div>
		  </form>
		)

	}
	render() {
		return (
				<div className="form-page__wrapper">
					<div className="form-page__form-wrapper">
					{this.verify}
					</div>
				</div>
			);

  	}
}
function select(state) {
  return {
    data: state
  };
}
module.exports=connect(select)(VerifyPage);

