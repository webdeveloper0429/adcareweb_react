import React,{Component} from 'react';
import {Popover,OverlayTrigger,Image} from 'react-bootstrap';
import H2 from '../../components/H2';
import {connect} from 'react-redux';
import SignUpPage from '../../components/pages/SignUpPage';
import styles from '../../Menu.css';
import {clearForm} from '../../Actions/AppActions';
import LoginButton from '../../components/pages/LoginPage';
import ForgotPage from '../../components/pages/ForgotPage';
import Backstretch from '../../components/Backstretch';
import Facebook from '../../components/Facebook';
import Google from '../../components/Google';
import LoadingButton from '../LoadingButton';

class HomePage extends Component {
	constructor(props) {
		super(props);
		this.state = { appTitle: "Sign in",show: false,title:"sign in",
						open:false,Title:'Get Started,',value:'with your account',
						access_token:'',loading:false}
		this.show = this.show.bind(this);
		this.onchange = this.onchange.bind(this);
		this.change = this.change.bind(this);
		this.handleOnChange = this.handleOnChange.bind(this);
		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
		this.modalButton = this.modalButton.bind(this);
	}
	componentDidMount() {
 		if(localStorage.userid){
			this.setState({title:'Register',open:true,loading:false})
		}else{
			if(localStorage.token && localStorage.token!=='undefined'){
				this.setState({loading:false,Title:'Thank You',value:'For Your Register',open:false,show: true})
				//this.props.history.push('./about')
			}
		}
	}
	componentWillReceiveProps (nextProps) {
 		if(!localStorage.token && !this.state.open){
			this.setState({loading:false,open: false,Title:'Get Started,',value:'with your account',show: false});
		}else if(localStorage.token && this.props.UserData.loggedIn){
			this.setState({loading:false,open: false,Title:'Thank You,',value:'For Your Register',show: true});
		}

	}
	show(e){
		this.props.dispatch(clearForm('true'));
		const type=e.target.getAttribute('data-i')
		if(type==='sign in'){
			this.setState({show: true,title:type})
		}
		else if(type==='logout'){
			this.props.history.push('/')
			this.setState({appTitle:'Sign in',title:""})
		}else{
			this.setState({title:type,show: true})

		}
	}
	onchange(){
		if(this.state.Title==='Get Started,'){
			this.setState({open: true});
		}
	}
	close(){
		this.setState({show: false})
	}
	change(){
		this.setState({Title:'Thank You',value:'For Your Register',open:false})
	}
	handleOnChange (event) {
		this.setState({ [event.target.id]: event.target.value }, null);
	}
	handleEmailChange(e) {
		this.setState({email: e.target.value});
	}
	handlePasswordChange(e) {
		this.setState({password: e.target.value});
	}
	modalButton() {
		if(this.state.title==='Register') {
			return (
				<SignUpPage  onchange={this.change} history={this.props.history} />
			);
		}else if(this.state.title==='sign in'){
			return (
				<LoginButton  onchange={this.change} history={this.props.history}/>
			);
		}
		else{
			return(
				<ForgotPage />
			);
		}
      }

	handleGoogleLogin() {
		console.log("google login");
		//auth2.grantOfflineAccess().then(signInCallback);
		//this.props.dispatch(google_login());
	}

	render(){
		const dispatch = this.props.dispatch;
		const {loading,Img} = this.props.UserData;
		const height=480;
		const width=800;
	  	const LOGO_WIDTH = 30;
		const LOGO_HEIGHT = 30;
		const popoverClick = (
			  <Popover id="popover-trigger-click" >
				<Image className="avatar-image avatar-image--icon" 
						style={{width:LOGO_WIDTH, height:LOGO_HEIGHT}} 
						src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTImf6r0BNyL7KEmoHkpjw_ZCekjPxp-ejm_U1wLnYBxlI--7GygpOTaQ" circle />
				<hr/>
				<button data-i="logout" className="btn btn-info" type="submit" onClick={this.show} >Logout</button>
			  </Popover>
			);
		const popoverbutton=(
			<OverlayTrigger trigger="click" placement="bottom" overlay={popoverClick}>
				<Image className="avatar-image avatar-image--icon" 
						style={{width:LOGO_WIDTH, height:LOGO_HEIGHT}} 
						src="https://cdn-images-1.medium.com/fit/c/64/64/1*dmbNkD5D-u45r44go_cf0g.png" circle />
			</OverlayTrigger>
		);
		const fb_gl_button=(
			<div className="row">
				<div className="col-sm-1">
				 	<Facebook dispatch={dispatch}/>
				</div>
				<Google dispatch={dispatch}/>
			</div>
		);
		return (
			<div className="col-sm-12 col-lg-12"  style={{fontFamily: "Font"}}>
			<Backstretch loading={loading} history={this.props.history} src={Img} className="homeimage"/>
			{this.state.open?(
				<div className="container">
					<div className="row">
					  	<div className="col-sm-4" style={{height:height,width:width}}>
						</div>
					</div>
				 	<div className="row">
					  	<div className="col-sm-8">
							{this.state.appTitle ? (
								 <a href="javascript:void (0)" data-i="sign in"  onClick={this.show} >{this.state.appTitle}</a>
							  ) : (
								popoverbutton
							)}
							&nbsp;&nbsp;&nbsp;
							{this.state.appTitle ? (
							  <a href="javascript:void (0)" data-i="Register" onClick={this.show}>{'Register'}</a>
							  ) : (
								  ""
							)}
							&nbsp;&nbsp;&nbsp;
							<a href="javascript:void (0)" data-i="Forgot" onClick={this.show}>{'Forgot Password'}</a>
					 	</div>
							{fb_gl_button}
					</div>
				 	<div className="row">
					  	<div className="col-sm-10">
							{this.modalButton()}
						</div>
					  	<div className="col-sm-2">
						</div>
					</div>
				</div>
				):(
				<div className="container">
					<div className="row">
					  	<div className="col-sm-4" style={{height:height}}>
						</div>
					</div>
				 	<div className="row">
					  	<div className="col-sm-8">
						<H2 className={styles.title}>{this.state.Title}</H2>
					 	</div>
					 	<div className="col-sm-4">
					 	</div>
					</div>
				 	<div className="row">
					  	<div className="col-sm-8">
						{this.state.loading?(
							<LoadingButton />
							):(
							<input type="button" name="account" value={this.state.value} onClick={this.onchange} className="account"/>
						)}
						</div>
					</div>
					&nbsp;&nbsp;&nbsp;
						{this.state.show?(""):(
							fb_gl_button
						)}
				</div>
				)}
			</div>
		);
	}	
}
function select(state) {
  return {
    UserData: state.UserData
  };
}
module.exports=connect(select)(HomePage);
