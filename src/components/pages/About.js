import React, { Component } from 'react';
import {connect} from 'react-redux';
import Backstretch from '../Backstretch';
import gender from '../../image/gender.jpg';
import styles from '../../Page.css';
import H3 from '../../components/H3';
import H2 from '../../components/H2';
import {changeForm} from '../../Actions/AppActions';
import old from '../../image/old.jpg';
import OldPage from '../../components/pages/OldPage.js';

class About extends Component {
	constructor(props) {
		super(props);
		this.onchange = this.onchange.bind(this);
	}
	onchange(event){
		console.log(this.props.history)
		if(event.target.value){
			let attr={
				Img:old,
				gender:event.target.value
			}
			this.props.dispatch(changeForm(attr));
		}
	}
	componentWillReceiveProps(nextProps) {
		if(nextProps.data.formState.gender){
			this.props.history.push('./Old',{gender:nextProps.data.formState.gender})
		}
	}
	render() {
		const { formState} = this.props.data;
		const height=110;
    return (
			<div className="col-sm-12 col-lg-12">
				<Backstretch src={gender} className="homeimage"/>
				<div className="container">
					<div className="row" style={{height:height}}>
					</div>
				 	<div className="row" style={{height:90}}>
					 	<div className="col-sm-5">
					 	</div>
					  	<div className="col-sm-4">
							<H3>Answer01</H3>
							<input type="button" name="gender" value="Male" onClick={this.onchange} className="gender"/>
					 	</div>
					</div>
				 	<div className="row" style={{height:260}}>
					 	<div className="col-sm-4">
					 	</div>
					  	<div className="col-sm-4">
							<H3>Answer02</H3>
							<input type="button" name="gender" value="Female" onClick={this.onchange} className="gender"/>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>#1</H2>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>I Identity My</H2>
					 	</div>
					</div>
				 	<div className="row">
					 	<div className="col-sm-1">
					 	</div>
					 	<div className="col-sm-4">
							<H2>Gender As...</H2>
					 	</div>
					</div>
				</div>
			</div>
		);
  }
}
function select(state) {
  return {
    data: state
  };
}
module.exports=connect(select)(About);
