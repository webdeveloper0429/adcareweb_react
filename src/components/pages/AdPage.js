import React, { Component} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import  Video from '../video';
import Backstretch from '../../components/Backstretch';
import Dg from '../../components/Dg';
import ad from '../../image/black1.jpg';
//import image from '../../image/Ad.jpg';
import play from '../../image/play.png';
import styles from '../../Menu.css';
import {catch_ad} from '../../Actions/Datas';

class  AdPage extends Component {
	constructor(props) {
		super(props);
		this.state = { play: true,open:false,Title:'DG Family',value:'Doke & Gabbana'}
	}
	componentDidMount() {
 		if(!localStorage.token){
			this.props.history.push('/')
        }else{
			let attr={
				token:localStorage.token
			}
			this.props.dispatch(catch_ad(attr));
		}
	}
	_onClick(id) {
		this.props.history.push('/Check/'+id)
	}
	handleSetActive(to) {
    	console.log(to);
  	}

	render() {
		const {AdDatas,domainName} = this.props.Datas;
		var rows={}
		_.map(AdDatas, function(n, key) {
			var img=`${domainName}/adcare/webadmin/image/${n.id}`;
			rows[n.id]=img
		})
		var name='',name1='';
	  	var datas = _.map(rows, function(n, key) {
			if(key%2=='1'){
				name='first';
				name1='first1';
			}else{
				name='second';
				name1='second1';
			}
			return <Video close={true} 
					key={key.toString()}  
					id={n.id} 
					src={n} playedsrc={play} 
					name={'ad_img '+name} 
					play={'play_img '+name1}
					onClick={this._onClick.bind(this, key)}
					/>
		}.bind(this));
		return(
			<div className="col-sm-12 col-lg-12">
				<Backstretch  src={ad} history={this.props.history} className="homeimage"/>
				<div className="container">
					<div className="row" style={{height:25}}>
						<Dg title={styles.title} 
							Title={this.state.Title} 
							value={this.state.value} 
							btnText={'watch now'} 
							style={'dg_title'}/>
					</div>
					<div className="row" id="video">
						<div className="col-sm-5">
						</div>
						<div className="col-sm-4">
							{datas}
						</div>
					</div>
				</div>
			</div>

		)
	
	}
}
function select(state) {
  return {
	Datas: state.Datas
  };
}
module.exports=connect(select)(AdPage);
