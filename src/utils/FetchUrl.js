export default async function FetchUrl(type,url,attr,callback){
	var Headers={};
	if (type==='admin' ||type==='set'||type==='catch'|| type==='send_code'){
		Headers={
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'token':attr.token
    	}
	}else{
		Headers={
			'Accept': 'application/json',
			'Content-Type': 'application/json',
    	}
	}
	var datas = {
		method: "POST",
		headers: Headers,
		body: JSON.stringify(attr)
  	};
	fetch(url, datas).then(response =>{
		if (response.status >= 200 && response.status < 300) {
			if(type==='photo'){
				return response.blob()
			
			}else{
				return response.json()
			}
		} else {
			var error = new Error(response.statusText)
			error.response = response
			throw error
		}
		}).then(function (textValue) {
			const response = textValue;
			console.log(response);
			if(type==='login'){
				if(response.status==='needVerify'){
					callback(false,response.status);
				}else if(response.status==='missingStep'){
					localStorage.setItem('token', response.token);
					callback(false,response.status, response.userData);
				}else{
					if(response.status=== 'successful'){
						localStorage.setItem('token', response.token);
						callback(true);
					}else{
						callback(false, response.errMsg);
					}
				}
			}else if(type==='set'|| type==='send_code'){
				if(response.success){
					callback(true);
				}else{
					callback(false, response.errMsg);
				}
			}else if(type=='admin'){
				if(response.success){
					if(response.token){
						localStorage.setItem('admintoken', response.token);
						callback(true);
					}else{
						callback(true,response.data);
					}
				}else{
					callback(false, response.errMsg);
				}
			
			
			}else if(type==='catch'){
				if(response.success){
					callback(true,response.data);
				}else{
					callback(false, response.errMsg);
				}
			
			
			}else if(type==='photo'){
				var objectURL = URL.createObjectURL(response);
				callback(true,objectURL);
			}else{
				if(response.status==='successful'){
        			localStorage.token = response.token;
					callback(true);
				}else{
					callback(false, response.errMsg);
				}
			
			}
			
		}).catch(function(error) {
			console.log(error);
     		callback(false, error);
		});
}



