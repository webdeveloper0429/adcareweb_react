import bcrypt from 'bcryptjs';
import FetchUrl from './FetchUrl';
import {web} from '../Constants/Globals';
/**
 * Authentication lib
 * @type {Object}
 */
var domainName=web.domainName;
var auth = {
  /**
   * Logs a user in
   * @param  {string}   email The email of the user
   * @param  {string}   password The password of the user
   * @param  {Function} callback Called after a user was logged in on the remote server
   */
  login(attr, callback) {
    if (this.loggedIn()) {
      callback(true);
      return;
    }
	const url = `${domainName}/adcare/onboarding/login`;
	console.log(url)
	var type='login';
	FetchUrl(type,url,attr,callback)
  },
  /**
   * Logs the current user out
   */
  logout(callback) {
	localStorage.removeItem('token');
	localStorage.removeItem('userid');
	console.log(localStorage)
	callback(true);
  },
  /**
   * Checks if anybody is logged in
   * @return {boolean} True if there is a logged in user, false if there isn't
   */
  loggedIn() {
    return !!localStorage.token;
  },
  /**
   * Registers a user in the system
   * @param  {string}   email The email of the user
   * @param  {string}   password The password of the user
   * @param  {Function} callback Called after a user was registered on the remote server
   */
  register(attr, callback) {
	const url = `${domainName}/adcare/onboarding/signup`;
	var type='register';
	FetchUrl(type,url,attr,callback)
  },

  /*
   * set_gender
  */
  set_gender(attr, callback) {
	const url = `${domainName}/adcare/onboarding/set_gender`;
	var type='set';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * set_age
  */
  set_age(attr, callback) {
	const url = `${domainName}/adcare/onboarding/set_age`;
	var type='set';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * set_love_ad_category
  */
  set_love_ad_category(attr, callback) {
	const url = `${domainName}/adcare/onboarding/set_love_ad_category`;
	var type='set';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * set_hate_ad_category
  */
  set_hate_ad_category(attr, callback) {
	const url = `${domainName}/adcare/onboarding/set_hate_ad_category`;
	var type='set';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * category love hate
  */
  catch_category(attr, callback) {
	const url = `${domainName}/adcare/ad/category_list`;
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * charity
  */
  catch_charity(attr, callback) {
	const url = `${domainName}/adcare/wallet/service_list`;
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * ad
  */
  catch_ad(attr, callback) {
	//const url = `${domainName}/adcare/ad/detail/${attr.id}`;
	const url = `${domainName}/adcare/ad/list`;
	console.log(url)
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * catch_detail
  */
  catch_detail(attr, callback) {
	const url = `${domainName}/adcare/ad/detail/${attr.id}`;
	console.log(url)
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * add_wallet
  */
  add_wallet(attr, callback) {
	const url = `${domainName}/adcare/ad/add_record/${attr.id}`;
	console.log(url)
	var type='set';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * get_donation
  */
  get_donation(attr, callback) {
	const url = `${domainName}/adcare/wallet/donated_record`;
	console.log(url)
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * get_spending
  */
  get_spending(attr, callback) {
	const url = `${domainName}/adcare/wallet/payout_record`;
	console.log(url)
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * get_amount
  */
  get_amount(attr, callback) {
	const url = `${domainName}/adcare/wallet/amount`;
	console.log(url)
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * get_country
  */
  get_country(attr, callback) {
	const url = `${domainName}/adcare/ranking/country`;
	console.log(url)
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * profile
  */
  catch_profile(attr, callback) {
	const url = `${domainName}/adcare/profile/info`;
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * update profile
  */
  update_profile(attr, callback) {
	const url = `${domainName}/adcare/profile/update`;
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * get_photo
  */
  get_photo(attr, callback) {
	const url = `${domainName}/adcare/profile/thumbnail/${attr.profileId}`;
	console.log(url)
	var type='photo';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * set_pref_charity
  */
  set_pref_charity(attr, callback) {
	const url = `${domainName}/adcare/onboarding/set_pref_charity`;
	var type='set';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * send_code
  */
  send_code(attr, callback) {
	const url = `${domainName}/adcare/onboarding/send_code`;
	var type='send_code';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * verifycode
  */
  verifycode(attr, callback) {
	const url = `${domainName}/adcare/onboarding/verify`;
	var type='verifycode';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_users
  */
  catch_users(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_users`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_charities
  */
  catch_charities(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_charities`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_country
  */
  catch_country(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_country`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_add_country
  */
  add_country(attr, callback) {
	const url = `${domainName}/adcare/webadmin/add_country`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_adlist
  */
  catch_adlist(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_adlist`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_addetail
  */
  catch_addetail(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_addetail`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_adCategory
  */
  catch_adCategory(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_adCategory`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_user
  */
  catch_user(attr, callback) {
	const url = `${domainName}/adcare/webadmin/info`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_donation
  */
  catch_donation(attr, callback) {
	const url = `${domainName}/adcare/webadmin/donated_record`;
	console.log(url)
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_del_donation
  */
  del_donation(attr, callback) {
	const url = `${domainName}/adcare/webadmin/del_donation`;
	console.log(url)
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_user_ad
  */
  catch_userad(attr, callback) {
	const url = `${domainName}/adcare/webadmin/walletRecord`;
	console.log(url)
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_add_ad
  */
  admin_add_ad(attr, callback) {
	const url = `${domainName}/adcare/webadmin/add_ad`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_add_charity
  */
  add_charity(attr, callback) {
	const url = `${domainName}/adcare/webadmin/add_charity`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_update_charity
  */
  update_charity(attr, callback) {
	const url = `${domainName}/adcare/webadmin/update_charity`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_del_charity
  */
  del_charity(attr, callback) {
	const url = `${domainName}/adcare/webadmin/del_charity`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_login
  */
  admin_login(attr, callback) {
	const url = `${domainName}/adcare/webadmin/admin_login`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_del_category
  */
  del_category(attr, callback) {
	const url = `${domainName}/adcare/webadmin/del_category`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_add_category
  */
  add_category(attr, callback) {
	const url = `${domainName}/adcare/webadmin/add_category`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_update_category
  */
  update_category(attr, callback) {
	const url = `${domainName}/adcare/webadmin/update_category`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * update users
  */
  update_user(attr, callback) {
	const url = `${domainName}/adcare/webadmin/update`;
	var type='catch';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_catch_advertisers
  */
  catch_advertisers(attr, callback) {
	const url = `${domainName}/adcare/webadmin/catch_advertisers`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_add_vertisers
  */
  add_vertisers(attr, callback) {
	const url = `${domainName}/adcare/webadmin/add_vertisers`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_del_advertisers
  */
  del_advertisers(attr, callback) {
	const url = `${domainName}/adcare/webadmin/del_advertisers`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_update_advertisers
  */
  update_advertisers(attr, callback) {
	const url = `${domainName}/adcare/webadmin/update_advertisers`;
	var type='admin';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * admin_logout
  */
  admin_logout(callback) {
	localStorage.removeItem('admintoken');
	console.log(localStorage)
	callback(true);
  },

  facebook_login(attr,callback) {
	const url = `${domainName}/adcare/onboarding/facebook_login`;
	var type='fb';
	FetchUrl(type,url,attr,callback)
  },
  google_login(attr,callback) {
	const url = `${domainName}/adcare/onboarding/google_login`;
	var type='fb';
	FetchUrl(type,url,attr,callback)
  },
  /*
   * forgot password
  */
  forgot(attr, callback) {
	const url = `${domainName}/adcare/onboarding/forgot_submit`;
	var type='forgot';
	FetchUrl(type,url,attr,callback)
  },
  forgotreset(attr, callback) {
	const url = `${domainName}/adcare/onboarding/forgot_reset`;
	var type='forgotreset';
	FetchUrl(type,url,attr,callback)
  }


}

module.exports = auth;
